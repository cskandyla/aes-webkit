#include "cloud/curlcloud.h"


int main(int argc, char *argv[])
{
  CURL *curl;
  curl_global_init(CURL_GLOBAL_ALL);
  curl = curl_easy_init();
  printf("\nLOGING IN!\n");
  session_info *current_session=cloud_login(curl,"tchatzi@arx.net","1234",3);
  printf("Login Successfull!\nSession Info:\n");
  printf("username:%s\n",current_session->user_email);
  printf("password: %s\n",current_session->password);
  printf("session_id: %s\n",current_session->session_id);
  printf("user key:  %s\n",current_session->user_key);
  printf("\nBROWSING\n");
   cloud_browse(curl,current_session->session_id);
  printf("\nGETTING A FILE\n");
  // cloud_get_file(curl,current_session->session_id,"skandylas@arx.net/Web/documents/decryptedfile",current_session->user_key);
  //cloud_logout(curl,current_session->session_id);
  //cloud_upload_file(curl,"skandylas@arx.net/test/temp/","lightning-bolt.jpg",current_session->session_id,"the_iv","deaddata","text",current_session->user_key,2);
  //cloud_mkdir(curl,"da_dir","skandylas@arx.net/documents",current_session->session_id);
  //cloud_upload_folder(curl,"skandylas@arx.net/sync","/home/longbow/Code/Work/aes-webkit/folder1",current_session->session_id,current_session->user_key);
  if(curl)
    {
      curl_easy_cleanup(curl);
    }
  curl_global_cleanup();

}
