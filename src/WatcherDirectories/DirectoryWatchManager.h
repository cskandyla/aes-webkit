#include <unordered_map>
#include <vector>
#include <string>
#include "cJSON.h"
using std::string;
using std::vector;
using std::unordered_map;

#ifndef __DIRECTORYWATCHMANAGER__
#define __DIRECTORYWATCHMANAGER__



char *strndup (const char *s, size_t n);
class DirectoryWatchManager
{
private:
    unordered_map<string,vector<string>> usertodirectorymap;

public:
    void Load(string file);
    void Save(string file);
    void Update(string user,vector<string> folders);
    vector<string> DirectoriesForUser(string user);
};
#endif // __DIRECTORYWATCHMANAGER__
