#include "DirectoryWatchManager.h"

#include "cJSON.h"
#include <cstring>
#include <cstdio>
#include <cstdlib>


//Utility matching function for json strings

void DirectoryWatchManager::Load(string file)
{
    //JSON FILE
    FILE *fp;
    long lSize;
    char *buffer;

    fp = fopen ( "Directories.json" , "r" );
    if( !fp ) perror(file.c_str());

    fseek( fp , 0L , SEEK_END);
    lSize = ftell( fp );
    rewind( fp );

    /* allocate memory for entire content */
    buffer = (char*)calloc( 1, lSize+1 );
    if( !buffer ) fclose(fp),fputs("memory alloc fails",stderr),exit(1);

    /* copy the file into the buffer */
    if( 1!=fread( buffer , lSize, 1 , fp) )
    {
        fclose(fp);
        free(buffer);
        fputs("entire read fails",stderr);
        return;
    }

    fclose(fp);



    cJSON *json;
    json=cJSON_Parse(buffer);
    if(json)
        if(json->child!=NULL)
        {
            printf("%d\n",cJSON_GetArraySize(json->child));
            for(int i=0; i<cJSON_GetArraySize(json->child); i++)
            {
                cJSON *json_data=cJSON_GetArrayItem(json->child,i);
                vector<string> folders;
                string username;
                while(json_data->child!=NULL)
                {
                    if(strcmp(json_data->child->string,"user")==0)
                    {
                        //printf("user:%s\n",json_data->child->valuestring);
                        username=json_data->child->valuestring;
                    }
                    else if(strcmp(json_data->child->string,"folder")==0)
                    {
                        //printf("folder:%s\n",json_data->child->valuestring);
                        folders.push_back(json_data->child->valuestring);
                    }
                    usertodirectorymap[username]=folders;
                    json_data->child=json_data->child->next;
                }
            }

        }
    free(buffer);

}
void DirectoryWatchManager::Save(string file)
{

    FILE *ob=fopen(file.c_str(),"w");
    /*
    for ( auto it = usertodirectorymap.begin(); it != usertodirectorymap.end(); ++it )
    {
        fprintf(ob,"{\n");
        fprintf(ob,"\"user\":\"%s\"\n",it->first.c_str());
        if(it->second.size()>0)
            fprintf(ob,"\n{\n");
        for(int i=0; i<it->second.size(); i++)
        {
            if(i!=it->second.size()-1)
                fprintf(ob,"\"folder\":\"%s\",\n",it->second[i].c_str());
            else
                fprintf(ob,"\"folder\":\"%s\"\n}\n\n",it->second[i].c_str());
        }
        fprintf(ob,"\n}\n\n");
    }*/
    fprintf(ob,"{\n\"users\":\n[");

    for ( auto it = usertodirectorymap.begin(); it != usertodirectorymap.end(); ++it )
    {
        fprintf(ob,"\n{\n");
        fprintf(ob,"\"user\":\"%s\",\n",it->first.c_str());
        for(int i=0; i<it->second.size(); i++)
        {

            fprintf(ob,"\"folder\":\"%s\",\n",it->second[i].c_str());
            /* else
                 fprintf(ob,"\"folder\":\"%s\"\n}\n\n",it->second[i].c_str());*/

        }
        fprintf(ob,"\n},\n");
    }

    fprintf(ob,"\n]\n}");
    fclose(ob);
}

void DirectoryWatchManager::Update(string user, vector<string> folders)
{
    usertodirectorymap[user]=folders;
}

vector<string> DirectoryWatchManager::DirectoriesForUser(string user)
{

    return usertodirectorymap[user];
}

