#include "wx/taskbar.h"
#include "wx/fswatcher.h"
#include <curl/curl.h>
#include <vector>
#include <string>
#include <deque>
#include <set>
#include "../cloud/curlcloud.h"
#include "FolderUploadThread.h"
#include "FileSyncThread.h"
#include "DownloadStatusbar.h"
#include "../WatcherDirectories/DirectoryWatchManager.h"
using std::string;
using std::wstring;
using std::vector;
using std::deque;
using std::set;
#ifndef __MYTASKBARICON__
#define __MYTASKBARICON__

/*
if(thread_done)
{
    delete thread;
    thread=NULL;
    //Create a new one
    t=new thread(files,&thread_done);
}
else if(!thread_done)
{
    //Do nothing
    files were added to the list anyways :P
}
if(folder_thread_done && file_thread_done)
{
    //Delete this thread create a new one
}
else if(!folder_thread_done || !file_thread_done)
{

    }
*/


enum THREAD_STATUS
{
    RUNNING=0,DONE
};



class MyTaskBarIcon : public wxTaskBarIcon
{
private:
    wxFrame *parent;
    wxFileSystemWatcher *m_watcher;
    CURL *curl;
    vector<string> dirsToWatch;
    session_info* current_session;
    //folder upload thread;
    FolderUploadThread *folderUploadThread;
    deque<wstring> directories;
    bool folder_thread_done;
    //file sync thread
    FileSyncThread     *fileSyncThread;
    deque<wstring> files;
    set<wstring> folderstosync;
    bool file_thread_done;
    wxTimer m_timer,refresh_timer;
    DownloadStatusBar *downloadprogress;
    DirectoryWatchManager *dwm;



public:
#if defined(__WXOSX__) && wxOSX_USE_COCOA
    MyTaskBarIcon(wxTaskBarIconType iconType = wxTBI_DEFAULT_TYPE)
        :   wxTaskBarIcon(iconType);
#else
    MyTaskBarIcon();

#endif

    void Init(wxFrame *parent,CURL *curl,session_info *current_session);
    void OnLeftButtonDClick(wxCommandEvent&);
    void OnEnableSync(wxCommandEvent&);
    void OnMenuExit(wxCommandEvent&);
    void OnMenuCheckmark(wxCommandEvent&);
    void OnMenuUICheckmark(wxUpdateUIEvent&);
    void OnMenuSub(wxCommandEvent&);
    void SetLanguage(wxCommandEvent&);
    void OnMenuFolderSync(wxCommandEvent&);
    void OnShow(wxCommandEvent&);
    void OnLogOut(wxCommandEvent&);
    void OnFileSystemEvent(wxFileSystemWatcherEvent &event);
    wstring LastFileCreated(const char *cloud_path,wstring directory);
    vector<string> LastNFilesCreated(char *cloud_path,string directory,unsigned int n);
    void addToWatcher(wstring directory);
    virtual wxMenu *CreatePopupMenu();
    void OnTimer(wxTimerEvent& event);
    void RefreshTimer(wxTimerEvent& event);

    wxDECLARE_EVENT_TABLE();
};

//TICKBOX(ENABLE SYNCING),SYNC FOLDERS,EXIT
enum
{

    PU_ENABLE_SYNC = 10001,
    PU_SYNC_FOLDER,
    PU_SHOW,
    PU_EXIT,
    PU_LOGOUT,
    PU_CHECKMARK,
    PU_LANGUAGE,
    PU_LANGUAGE_FINNISH,
    PU_LANGUAGE_ENGLISH,
    PU_TIMER_ID,
    PU_REFRESH_TIMER_ID
};



#endif
