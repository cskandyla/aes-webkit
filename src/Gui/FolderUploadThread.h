
// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#if !wxUSE_THREADS
#error "This requires thread support!"
#endif // wxUSE_THREADS

#include "wx/thread.h"
#include <curl/curl.h>
#include <deque>
#include <string>
#include "../cloud/curlcloud.h"
#include "DownloadStatusbar.h"
using std::deque;
using std::string;
using std::wstring;


#ifndef __FOLDERUPLOADTHREAD__
#define __FOLDERUPLOADTHREAD__


class FolderUploadThread : public wxThread
{
public:
    FolderUploadThread(deque<wstring> &folder_list,CURL *curl,session_info *current_session,bool *thread_done,DownloadStatusBar *bar);
    ~FolderUploadThread();
protected:
    virtual wxThread::ExitCode Entry();
    deque<wstring> &folder_list;
    CURL *curl;
    session_info *current_session;
    bool *thread_done;
    int max_size;
    DownloadStatusBar *bar;
};
#endif // __FOLDERUPLOADTHREAD__
