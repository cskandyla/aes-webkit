#include "MyWebFrame.h"
#include "../CEF/webview_chromium.h"


#ifndef wxHAS_IMAGES_IN_RESOURCES
#include "sample.xpm"
#endif

WebFrame::WebFrame(const wxString& url) :
    wxFrame(NULL, wxID_ANY, "wxWebView Sample",wxDefaultPosition,wxDefaultSize/*,wxDEFAULT_FRAME_STYLE & ~(/*wxRESIZE_BORDER | wxMAXIMIZE_BOX  | wxMINIMIZE_BOX)*/)
{
    // set the frame icon
    loggedin=false;
    wxIcon *daticon=new wxIcon("Pilvilinna-icon_large.png");
    SetIcon(*daticon);
    SetTitle("wxWebView Sample");
    current_session=NULL;

    wxBoxSizer* topsizer = new wxBoxSizer(wxVERTICAL);

    // Create the toolbar
    m_toolbar = CreateToolBar(wxTB_TEXT);
    m_toolbar->SetToolBitmapSize(wxSize(32, 32));

    wxBitmap back = wxArtProvider::GetBitmap(wxART_GO_BACK , wxART_TOOLBAR);
    wxBitmap forward = wxArtProvider::GetBitmap(wxART_GO_FORWARD , wxART_TOOLBAR);
#ifdef __WXGTK__
    wxBitmap stop = wxArtProvider::GetBitmap("gtk-stop", wxART_TOOLBAR);
#else
    wxBitmap stop = wxBitmap(stop_xpm);
#endif
#ifdef __WXGTK__
    wxBitmap refresh = wxArtProvider::GetBitmap("gtk-refresh", wxART_TOOLBAR);
#else
    wxBitmap refresh = wxBitmap(refresh_xpm);
#endif

    m_toolbar_back = m_toolbar->AddTool(wxID_ANY, wxT("Back"), back);
    m_toolbar_forward = m_toolbar->AddTool(wxID_ANY, wxT("Forward"), forward);
    m_toolbar_stop = m_toolbar->AddTool(wxID_ANY, wxT("Stop"), stop);
    m_toolbar_reload = m_toolbar->AddTool(wxID_ANY, wxT("Reload"),  refresh);
    m_url = new wxTextCtrl(m_toolbar, wxID_ANY, wxT(""),  wxDefaultPosition, wxSize(400, -1), wxTE_PROCESS_ENTER );
    m_toolbar->AddControl(m_url, wxT("URL"));
    //m_toolbar_tools = m_toolbar->AddTool(wxID_ANY, _("Menu"), wxBitmap(sample_xpm));

    m_toolbar->Realize();

    // Set find values.
    m_findFlags = wxWEBVIEW_FIND_DEFAULT;
    m_findText = wxEmptyString;
    m_findCount = 0;

    // Create panel for find toolbar.
    wxPanel* panel = new wxPanel(this);
    topsizer->Add(panel, wxSizerFlags().Expand());

    // Create sizer for panel.
    wxBoxSizer* panel_sizer = new wxBoxSizer(wxVERTICAL);
    panel->SetSizer(panel_sizer);

    // Create the find toolbar.
    m_find_toolbar = new wxToolBar(panel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTB_HORIZONTAL|wxTB_TEXT|wxTB_HORZ_LAYOUT);
    m_find_toolbar->Hide();
    panel_sizer->Add(m_find_toolbar, wxSizerFlags().Expand());

    // Create find control.
    m_find_ctrl = new wxTextCtrl(m_find_toolbar, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(140,-1), wxTE_PROCESS_ENTER);


    //Find options menu
    wxMenu* findmenu = new wxMenu;
    m_find_toolbar_wrap = findmenu->AppendCheckItem(wxID_ANY,"Wrap");
    m_find_toolbar_matchcase = findmenu->AppendCheckItem(wxID_ANY,"Match Case");
    m_find_toolbar_wholeword = findmenu->AppendCheckItem(wxID_ANY,"Entire Word");
    m_find_toolbar_highlight = findmenu->AppendCheckItem(wxID_ANY,"Highlight");
    // Add find toolbar tools.
    m_find_toolbar->SetToolSeparation(7);
    m_find_toolbar_done = m_find_toolbar->AddTool(wxID_ANY, "Close", wxArtProvider::GetBitmap(wxART_CROSS_MARK));
    m_find_toolbar->AddSeparator();
    m_find_toolbar->AddControl(m_find_ctrl, "Find");
    m_find_toolbar->AddSeparator();
    m_find_toolbar_next = m_find_toolbar->AddTool(wxID_ANY, "Next", wxArtProvider::GetBitmap(wxART_GO_DOWN, wxART_TOOLBAR, wxSize(16,16)));
    m_find_toolbar_previous = m_find_toolbar->AddTool(wxID_ANY, "Previous", wxArtProvider::GetBitmap(wxART_GO_UP, wxART_TOOLBAR, wxSize(16,16)));
    m_find_toolbar->AddSeparator();
    m_find_toolbar_options = m_find_toolbar->AddTool(wxID_ANY, "Options", wxArtProvider::GetBitmap(wxART_PLUS, wxART_TOOLBAR, wxSize(16,16)), "", wxITEM_DROPDOWN);
    m_find_toolbar_options->SetDropdownMenu(findmenu);
    m_find_toolbar->Realize();

    // Create the info panel
    m_info = new wxInfoBar(this);
    topsizer->Add(m_info, wxSizerFlags().Expand());
    wxWebView::RegisterFactory(wxWebViewBackendChromium, wxSharedPtr<wxWebViewFactory>
                               (new wxWebViewFactoryChromium));
    m_browser = wxWebView::New(this, wxID_ANY, "https://demo.whitecloud.mobi",
                               wxDefaultPosition, wxDefaultSize, wxWebViewBackendChromium);
    // Create the webview
    //m_browser = wxWebView::New(this, wxID_ANY, url);
    topsizer->Add(m_browser, wxSizerFlags().Expand().Proportion(1));

    //We register the wxfs:// protocol for testing purposes
    m_browser->RegisterHandler(wxSharedPtr<wxWebViewHandler>(new wxWebViewArchiveHandler("wxfs")));
    //And the memory: file system
    m_browser->RegisterHandler(wxSharedPtr<wxWebViewHandler>(new wxWebViewFSHandler("memory")));

    SetSizer(topsizer);

    //Set a more sensible size for web browsing
    SetSize(wxSize(800, 600));

    // Create a log window
    new wxLogWindow(this, wxT("Logging"), true, false);

    // Create the Tools menu
    m_tools_menu = new wxMenu();
    wxMenuItem* print = m_tools_menu->Append(wxID_ANY , wxT("Print"));
    wxMenuItem* viewSource = m_tools_menu->Append(wxID_ANY , wxT("View Source"));
    wxMenuItem* viewText = m_tools_menu->Append(wxID_ANY, wxT("View Text"));
    m_tools_menu->AppendSeparator();
    m_tools_layout = m_tools_menu->AppendCheckItem(wxID_ANY, wxT("Use Layout Zoom"));
    m_tools_tiny = m_tools_menu->AppendCheckItem(wxID_ANY, wxT("Tiny"));
    m_tools_small = m_tools_menu->AppendCheckItem(wxID_ANY, wxT("Small"));
    m_tools_medium = m_tools_menu->AppendCheckItem(wxID_ANY, wxT("Medium"));
    m_tools_large = m_tools_menu->AppendCheckItem(wxID_ANY, wxT("Large"));
    m_tools_largest = m_tools_menu->AppendCheckItem(wxID_ANY, wxT("Largest"));
    m_tools_menu->AppendSeparator();
    m_tools_handle_navigation = m_tools_menu->AppendCheckItem(wxID_ANY, wxT("Handle Navigation"));
    m_tools_handle_new_window = m_tools_menu->AppendCheckItem(wxID_ANY, wxT("Handle New Windows"));
    m_tools_menu->AppendSeparator();

    //Find
    m_find = m_tools_menu->Append(wxID_ANY, wxT("Find"));
    m_tools_menu->AppendSeparator();

    //History menu
    m_tools_history_menu = new wxMenu();
    wxMenuItem* clearhist =  m_tools_history_menu->Append(wxID_ANY, wxT("Clear History"));
    m_tools_enable_history = m_tools_history_menu->AppendCheckItem(wxID_ANY, wxT("Enable History"));
    m_tools_history_menu->AppendSeparator();

    m_tools_menu->AppendSubMenu(m_tools_history_menu, "History");

    //Create an editing menu
    wxMenu* editmenu = new wxMenu();
    m_edit_cut = editmenu->Append(wxID_ANY, wxT("Cut"));
    m_edit_copy = editmenu->Append(wxID_ANY, wxT("Copy"));
    m_edit_paste = editmenu->Append(wxID_ANY, wxT("Paste"));
    editmenu->AppendSeparator();
    m_edit_undo = editmenu->Append(wxID_ANY, wxT("Undo"));
    m_edit_redo = editmenu->Append(wxID_ANY, wxT("Redo"));
    editmenu->AppendSeparator();
    m_edit_mode = editmenu->AppendCheckItem(wxID_ANY, wxT("Edit Mode"));

    m_tools_menu->AppendSeparator();
    m_tools_menu->AppendSubMenu(editmenu, "Edit");

    wxMenu* scroll_menu = new wxMenu;
    m_scroll_line_up = scroll_menu->Append(wxID_ANY, "Line &up");
    m_scroll_line_down = scroll_menu->Append(wxID_ANY, "Line &down");
    m_scroll_page_up = scroll_menu->Append(wxID_ANY, "Page u&p");
    m_scroll_page_down = scroll_menu->Append(wxID_ANY, "Page d&own");
    m_tools_menu->AppendSubMenu(scroll_menu, "Scroll");

    wxMenuItem* script =  m_tools_menu->Append(wxID_ANY, wxT("Run Script"));

    //Selection menu
    wxMenu* selection = new wxMenu();
    m_selection_clear = selection->Append(wxID_ANY, wxT("Clear Selection"));
    m_selection_delete = selection->Append(wxID_ANY, wxT("Delete Selection"));
    wxMenuItem* selectall = selection->Append(wxID_ANY, wxT("Select All"));

    editmenu->AppendSubMenu(selection, "Selection");

    wxMenuItem* loadscheme =  m_tools_menu->Append(wxID_ANY, wxT("Custom Scheme Example"));
    wxMenuItem* usememoryfs =  m_tools_menu->Append(wxID_ANY, wxT("Memory File System Example"));

    m_context_menu = m_tools_menu->AppendCheckItem(wxID_ANY, wxT("Enable Context Menu"));

    //By default we want to handle navigation and new windows
    m_tools_handle_navigation->Check();
    m_tools_handle_new_window->Check();
    m_tools_enable_history->Check();
    if(!m_browser->CanSetZoomType(wxWEBVIEW_ZOOM_TYPE_LAYOUT))
        m_tools_layout->Enable(false);
    m_toolbar->Hide();



    Connect(m_url->GetId(), wxEVT_TEXT_ENTER,
            wxCommandEventHandler(WebFrame::OnUrl), NULL, this );



    // Connect the webview events
    Connect(m_browser->GetId(), wxEVT_WEBVIEW_NAVIGATING,
            wxWebViewEventHandler(WebFrame::OnNavigationRequest), NULL, this);
    Connect(m_browser->GetId(), wxEVT_WEBVIEW_NAVIGATED,
            wxWebViewEventHandler(WebFrame::OnNavigationComplete), NULL, this);
    Connect(m_browser->GetId(), wxEVT_WEBVIEW_LOADED,
            wxWebViewEventHandler(WebFrame::OnDocumentLoaded), NULL, this);
    Connect(m_browser->GetId(), wxEVT_WEBVIEW_ERROR,
            wxWebViewEventHandler(WebFrame::OnError), NULL, this);
    Connect(m_browser->GetId(), wxEVT_WEBVIEW_NEWWINDOW,
            wxWebViewEventHandler(WebFrame::OnNewWindow), NULL, this);
    Connect(m_browser->GetId(), wxEVT_WEBVIEW_TITLE_CHANGED,
            wxWebViewEventHandler(WebFrame::OnTitleChanged), NULL, this);

    Connect(wxID_ANY,wxEVT_ICONIZE,wxIconizeEventHandler(WebFrame::OnIconizeHide),NULL,this);

    Connect(wxID_ANY,wxEVT_CLOSE_WINDOW,wxCloseEventHandler(WebFrame::OnExitHide),NULL,this);


    //Connect the idle events
    Connect(wxID_ANY, wxEVT_IDLE, wxIdleEventHandler(WebFrame::OnIdle), NULL, this);



}

void WebFrame::OnIconizeHide(wxIconizeEvent& event)
{
    this->Hide();
}

void WebFrame::OnExitHide(wxCloseEvent& event)
{
    this->Hide();
}


WebFrame::~WebFrame()
{
    delete m_tools_menu;
}

/**
  * Method that retrieves the current state from the web control and updates the GUI
  * the reflect this current state.
  */

void WebFrame::UpdateState()
{
    m_toolbar->EnableTool( m_toolbar_back->GetId(), m_browser->CanGoBack() );
    m_toolbar->EnableTool( m_toolbar_forward->GetId(), m_browser->CanGoForward() );

    if (m_browser->IsBusy())
    {
        m_toolbar->EnableTool( m_toolbar_stop->GetId(), true );
    }
    else
    {
        m_toolbar->EnableTool( m_toolbar_stop->GetId(), false );
    }

    SetTitle( m_browser->GetCurrentTitle() );
    m_url->SetValue( m_browser->GetCurrentURL() );



}

void WebFrame::OnIdle(wxIdleEvent& WXUNUSED(evt))
{
    if(m_browser->IsBusy())
    {
        wxSetCursor(wxCURSOR_ARROWWAIT);
        m_toolbar->EnableTool(m_toolbar_stop->GetId(), true);
    }
    else
    {
        wxSetCursor(wxNullCursor);
        m_toolbar->EnableTool(m_toolbar_stop->GetId(), false);
    }
}

/**
  * Callback invoked when user entered an URL and pressed enter
  */
void WebFrame::OnUrl(wxCommandEvent& WXUNUSED(evt))
{
    m_browser->LoadURL( m_url->GetValue() );
    m_browser->SetFocus();
    UpdateState();
}

/**
    * Callback invoked when user pressed the "back" button
    */
void WebFrame::OnBack(wxCommandEvent& WXUNUSED(evt))
{
    m_browser->GoBack();
    UpdateState();
}

/**
  * Callback invoked when user pressed the "forward" button
  */
void WebFrame::OnForward(wxCommandEvent& WXUNUSED(evt))
{
    m_browser->GoForward();
    UpdateState();
}

/**
  * Callback invoked when user pressed the "stop" button
  */
void WebFrame::OnStop(wxCommandEvent& WXUNUSED(evt))
{
    m_browser->Stop();
    UpdateState();
}

/**
  * Callback invoked when user pressed the "reload" button
  */
void WebFrame::OnReload(wxCommandEvent& WXUNUSED(evt))
{
    m_browser->Reload();
    UpdateState();
}

void WebFrame::OnClearHistory(wxCommandEvent& WXUNUSED(evt))
{
    m_browser->ClearHistory();
    UpdateState();
}

void WebFrame::OnEnableHistory(wxCommandEvent& WXUNUSED(evt))
{
    m_browser->EnableHistory(m_tools_enable_history->IsChecked());
    UpdateState();
}

void WebFrame::OnCut(wxCommandEvent& WXUNUSED(evt))
{
    m_browser->Cut();
}

void WebFrame::OnCopy(wxCommandEvent& WXUNUSED(evt))
{
    m_browser->Copy();
}

void WebFrame::OnPaste(wxCommandEvent& WXUNUSED(evt))
{
    m_browser->Paste();
}

void WebFrame::OnUndo(wxCommandEvent& WXUNUSED(evt))
{
    m_browser->Undo();
}

void WebFrame::OnRedo(wxCommandEvent& WXUNUSED(evt))
{
    m_browser->Redo();
}

void WebFrame::OnMode(wxCommandEvent& WXUNUSED(evt))
{
    m_browser->SetEditable(m_edit_mode->IsChecked());
}

void WebFrame::OnLoadScheme(wxCommandEvent& WXUNUSED(evt))
{
    wxFileName helpfile("../help/doc.zip");
    helpfile.MakeAbsolute();
    wxString path = helpfile.GetFullPath();
    //Under MSW we need to flip the slashes
    path.Replace("\\", "/");
    path = "wxfs:///" + path + ";protocol=zip/doc.htm";
    m_browser->LoadURL(path);
}

void WebFrame::OnUseMemoryFS(wxCommandEvent& WXUNUSED(evt))
{
    m_browser->LoadURL("memory:page1.htm");
}

void WebFrame::OnEnableContextMenu(wxCommandEvent& evt)
{
    m_browser->EnableContextMenu(evt.IsChecked());
}

void WebFrame::OnFind(wxCommandEvent& WXUNUSED(evt))
{
    wxString value = m_browser->GetSelectedText();
    if(value.Len() > 150)
    {
        value.Truncate(150);
    }
    m_find_ctrl->SetValue(value);
    if(!m_find_toolbar->IsShown())
    {
        m_find_toolbar->Show(true);
        SendSizeEvent();
    }
    m_find_ctrl->SelectAll();
}

void WebFrame::OnFindDone(wxCommandEvent& WXUNUSED(evt))
{
    m_browser->Find("");
    m_find_toolbar->Show(false);
    SendSizeEvent();
}

void WebFrame::OnFindText(wxCommandEvent& evt)
{
    int flags = 0;

    if(m_find_toolbar_wrap->IsChecked())
        flags |= wxWEBVIEW_FIND_WRAP;
    if(m_find_toolbar_wholeword->IsChecked())
        flags |= wxWEBVIEW_FIND_ENTIRE_WORD;
    if(m_find_toolbar_matchcase->IsChecked())
        flags |= wxWEBVIEW_FIND_MATCH_CASE;
    if(m_find_toolbar_highlight->IsChecked())
        flags |= wxWEBVIEW_FIND_HIGHLIGHT_RESULT;

    if(m_find_toolbar_previous->GetId() == evt.GetId())
        flags |= wxWEBVIEW_FIND_BACKWARDS;

    wxString find_text = m_find_ctrl->GetValue();
    long count = m_browser->Find(find_text, flags);

    if(m_findText != find_text)
    {
        m_findCount = count;
        m_findText = find_text;
    }

    if(count != wxNOT_FOUND || find_text.IsEmpty())
    {
        m_find_ctrl->SetBackgroundColour(*wxWHITE);
    }
    else
    {
        m_find_ctrl->SetBackgroundColour(wxColour(255, 101, 101));
    }

    m_find_ctrl->Refresh();

    //Log the result, note that count is zero indexed.
    if(count != m_findCount)
    {
        count++;
    }
    //wxLogMessage("Searching for:%s  current match:%i/%i", m_findText.c_str(), count, m_findCount);
}

/**
  * Callback invoked when there is a request to load a new page (for instance
  * when the user clicks a link)
  */
void WebFrame::OnNavigationRequest(wxWebViewEvent& evt)
{
    if(m_info->IsShown())
    {
        m_info->Dismiss();
    }

//    wxLogMessage("%s", "Navigation request to '" + evt.GetURL() + "' (target='" +
//                evt.GetTarget() + "')");


    wxASSERT(m_browser->IsBusy());

    //If we don't want to handle navigation then veto the event and navigation
    //will not take place, we also need to stop the loading animation
    if(!m_tools_handle_navigation->IsChecked())
    {
        evt.Veto();
        m_toolbar->EnableTool( m_toolbar_stop->GetId(), false );
    }
    else
    {
        UpdateState();
    }
}

/**
  * Callback invoked when a navigation request was accepted
  */
void WebFrame::OnNavigationComplete(wxWebViewEvent& evt)
{
//    wxLogMessage("%s", "Navigation complete; url='" + evt.GetURL() + "'");
    UpdateState();
}

/**
  * Callback invoked when a page is finished loading
  */
void WebFrame::OnDocumentLoaded(wxWebViewEvent& evt)
{
    //Only notify if the document is the main frame, not a subframe

    if(evt.GetURL() == m_browser->GetCurrentURL() && !loggedin)
    {



        if(current_session!=NULL  &&  !loggedin)
        {
            char uglyhack[4096];
            //very ugly hack designed for quick demo should find a better way to do this ;)
            sprintf(uglyhack,"document.getElementById(\"username\").value=\"%s\";document.getElementById(\"password\").value=\"%s\";document.getElementById(\"login\").submit();",current_session->user_email,current_session->password);
            //GetBrowser()->RunScript(uglyhack);
            //GetBrowser()->RunScript("");
            //frame->SetLoggedIn(true);
        }
//        wxLogMessage("%s", "Document loaded; url='" + evt.GetURL() + "'");
        loggedin=true;

    }
    UpdateState();
}

/**
  * On new window, we veto to stop extra windows appearing
  */
void WebFrame::OnNewWindow(wxWebViewEvent& evt)
{
//    wxLogMessage("%s", "New window; url='" + evt.GetURL() + "'");

    //If we handle new window events then just load them in this window as we
    //are a single window browser
    if(m_tools_handle_new_window->IsChecked())
    {

        m_browser->LoadURL(evt.GetURL());
    }

    UpdateState();
}

void WebFrame::OnTitleChanged(wxWebViewEvent& evt)
{
    SetTitle(evt.GetString());
//    wxLogMessage("%s", "Title changed; title='" + evt.GetString() + "'");
}

/**
  * Invoked when user selects the "View Source" menu item
  */


/**
 * Invoked when user selects the "View Text" menu item
 */
void WebFrame::OnViewTextRequest(wxCommandEvent& WXUNUSED(evt))
{
    wxDialog textViewDialog(this, wxID_ANY, "Page Text",
                            wxDefaultPosition, wxSize(700,500),
                            wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER);
    wxStyledTextCtrl* text = new wxStyledTextCtrl(&textViewDialog, wxID_ANY);
    text->SetText(m_browser->GetPageText());
    wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
    sizer->Add(text, 1, wxEXPAND);
    SetSizer(sizer);
    textViewDialog.ShowModal();
}

/**
  * Invoked when user selects the "Menu" item
  */
void WebFrame::OnToolsClicked(wxCommandEvent& WXUNUSED(evt))
{
    if(m_browser->GetCurrentURL() == "")
        return;

    m_tools_tiny->Check(false);
    m_tools_small->Check(false);
    m_tools_medium->Check(false);
    m_tools_large->Check(false);
    m_tools_largest->Check(false);

    wxWebViewZoom zoom = m_browser->GetZoom();
    switch (zoom)
    {
    case wxWEBVIEW_ZOOM_TINY:
        m_tools_tiny->Check();
        break;
    case wxWEBVIEW_ZOOM_SMALL:
        m_tools_small->Check();
        break;
    case wxWEBVIEW_ZOOM_MEDIUM:
        m_tools_medium->Check();
        break;
    case wxWEBVIEW_ZOOM_LARGE:
        m_tools_large->Check();
        break;
    case wxWEBVIEW_ZOOM_LARGEST:
        m_tools_largest->Check();
        break;
    }

    m_edit_cut->Enable(m_browser->CanCut());
    m_edit_copy->Enable(m_browser->CanCopy());
    m_edit_paste->Enable(m_browser->CanPaste());

    m_edit_undo->Enable(m_browser->CanUndo());
    m_edit_redo->Enable(m_browser->CanRedo());

    m_selection_clear->Enable(m_browser->HasSelection());
    m_selection_delete->Enable(m_browser->HasSelection());

    m_context_menu->Check(m_browser->IsContextMenuEnabled());

    //Firstly we clear the existing menu items, then we add the current ones
    wxMenuHistoryMap::const_iterator it;
    for( it = m_histMenuItems.begin(); it != m_histMenuItems.end(); ++it )
    {
        m_tools_history_menu->Destroy(it->first);
    }
    m_histMenuItems.clear();

    wxVector<wxSharedPtr<wxWebViewHistoryItem> > back = m_browser->GetBackwardHistory();
    wxVector<wxSharedPtr<wxWebViewHistoryItem> > forward = m_browser->GetForwardHistory();

    wxMenuItem* item;

    unsigned int i;
    for(i = 0; i < back.size(); i++)
    {
        item = m_tools_history_menu->AppendRadioItem(wxID_ANY, back[i]->GetTitle());
        m_histMenuItems[item->GetId()] = back[i];
        Connect(item->GetId(), wxEVT_MENU,
                wxCommandEventHandler(WebFrame::OnHistory), NULL, this );
    }

    wxString title = m_browser->GetCurrentTitle();
    if ( title.empty() )
        title = "(untitled)";
    item = m_tools_history_menu->AppendRadioItem(wxID_ANY, title);
    item->Check();

    //No need to connect the current item
    m_histMenuItems[item->GetId()] = wxSharedPtr<wxWebViewHistoryItem>(new wxWebViewHistoryItem(m_browser->GetCurrentURL(), m_browser->GetCurrentTitle()));

    for(i = 0; i < forward.size(); i++)
    {
        item = m_tools_history_menu->AppendRadioItem(wxID_ANY, forward[i]->GetTitle());
        m_histMenuItems[item->GetId()] = forward[i];
        Connect(item->GetId(), wxEVT_TOOL,
                wxCommandEventHandler(WebFrame::OnHistory), NULL, this );
    }

    wxPoint position = ScreenToClient( wxGetMousePosition() );
    PopupMenu(m_tools_menu, position.x, position.y);
}

/**
  * Invoked when user selects the zoom size in the menu
  */
void WebFrame::OnSetZoom(wxCommandEvent& evt)
{
    if (evt.GetId() == m_tools_tiny->GetId())
    {
        m_browser->SetZoom(wxWEBVIEW_ZOOM_TINY);
    }
    else if (evt.GetId() == m_tools_small->GetId())
    {
        m_browser->SetZoom(wxWEBVIEW_ZOOM_SMALL);
    }
    else if (evt.GetId() == m_tools_medium->GetId())
    {
        m_browser->SetZoom(wxWEBVIEW_ZOOM_MEDIUM);
    }
    else if (evt.GetId() == m_tools_large->GetId())
    {
        m_browser->SetZoom(wxWEBVIEW_ZOOM_LARGE);
    }
    else if (evt.GetId() == m_tools_largest->GetId())
    {
        m_browser->SetZoom(wxWEBVIEW_ZOOM_LARGEST);
    }
    else
    {
        wxFAIL;
    }
}

void WebFrame::OnZoomLayout(wxCommandEvent& WXUNUSED(evt))
{
    if(m_tools_layout->IsChecked())
        m_browser->SetZoomType(wxWEBVIEW_ZOOM_TYPE_LAYOUT);
    else
        m_browser->SetZoomType(wxWEBVIEW_ZOOM_TYPE_TEXT);
}

void WebFrame::OnHistory(wxCommandEvent& evt)
{
    m_browser->LoadHistoryItem(m_histMenuItems[evt.GetId()]);
}

void WebFrame::OnRunScript(wxCommandEvent& WXUNUSED(evt))
{
    wxTextEntryDialog dialog(this, "Enter JavaScript to run.", wxGetTextFromUserPromptStr, "", wxOK|wxCANCEL|wxCENTRE|wxTE_MULTILINE);
    if(dialog.ShowModal() == wxID_OK)
    {
        m_browser->RunScript(dialog.GetValue());
    }
}

void WebFrame::OnClearSelection(wxCommandEvent& WXUNUSED(evt))
{
    m_browser->ClearSelection();
}

void WebFrame::OnDeleteSelection(wxCommandEvent& WXUNUSED(evt))
{
    m_browser->DeleteSelection();
}

void WebFrame::OnSelectAll(wxCommandEvent& WXUNUSED(evt))
{
    m_browser->SelectAll();
}

/**
  * Callback invoked when a loading error occurs
  */
void WebFrame::OnError(wxWebViewEvent& evt)
{
#define WX_ERROR_CASE(type) \
    case type: \
        category = #type; \
        break;

    wxString category;
    switch (evt.GetInt())
    {
        WX_ERROR_CASE(wxWEBVIEW_NAV_ERR_CONNECTION);
        WX_ERROR_CASE(wxWEBVIEW_NAV_ERR_CERTIFICATE);
        WX_ERROR_CASE(wxWEBVIEW_NAV_ERR_AUTH);
        WX_ERROR_CASE(wxWEBVIEW_NAV_ERR_SECURITY);
        WX_ERROR_CASE(wxWEBVIEW_NAV_ERR_NOT_FOUND);
        WX_ERROR_CASE(wxWEBVIEW_NAV_ERR_REQUEST);
        WX_ERROR_CASE(wxWEBVIEW_NAV_ERR_USER_CANCELLED);
        WX_ERROR_CASE(wxWEBVIEW_NAV_ERR_OTHER);
    }

//    wxLogMessage("%s", "Error; url='" + evt.GetURL() + "', error='" + category + " (" + evt.GetString() + ")'");

    //Show the info bar with an error
    m_info->ShowMessage(wxT("An error occurred loading ") + evt.GetURL() + "\n" +
                        "'" + category + "'", wxICON_ERROR);

    UpdateState();
}

/**
  * Invoked when user selects "Print" from the menu
  */
void WebFrame::OnPrint(wxCommandEvent& WXUNUSED(evt))
{
    m_browser->Print();
}



