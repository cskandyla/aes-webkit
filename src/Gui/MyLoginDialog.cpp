#include "MyLoginDialog.h"
#include <wx/hyperlink.h>

BEGIN_EVENT_TABLE(MyLoginDialog,wxWindow)
    EVT_PAINT(MyLoginDialog::OnPaint)
    EVT_CLOSE(MyLoginDialog::OnClose)
END_EVENT_TABLE()


MyLoginDialog::MyLoginDialog(const wxString& title,CURL *curl, session_info **current_session,int width=1024,int height=1024) :wxDialog(NULL, -1, title,wxDefaultPosition,wxSize(width,height))
{

    this->curl=curl;
    this->current_session=current_session;
    Centre();
    Connect(ID_LOGIN_BUTTON, wxEVT_COMMAND_BUTTON_CLICKED,
            wxCommandEventHandler(MyLoginDialog::OnLogin));

    int sys_width=wxSystemSettings::GetMetric (wxSYS_SCREEN_X);
    int sys_height=wxSystemSettings::GetMetric (wxSYS_SCREEN_Y);
    //wxMessageBox(wxString::Format(wxT("%i"),sys_width)+wxString(" ")+wxString::Format(wxT("%i"),sys_height));

    if(sys_width > 1024 && sys_height > 1024)
    {
        wxFont font(26, wxDEFAULT, wxNORMAL,wxNORMAL);
        this->SetFont(font);
        wxStaticText *i_am_pilvilina = new wxStaticText(this,wxID_STATIC,_("Elisa Pilvilina Plus"),wxPoint(240,210));
        i_am_pilvilina->SetBackgroundColour(wxColor(255,255,255));
//Create  a bigger/prettier font :D
        wxFont font2(12, wxDEFAULT, wxNORMAL,wxNORMAL);
        this->SetFont(font2);
        user_email_entry=new wxTextCtrl( this, NULL,wxString(""),wxPoint(200,510),wxSize(300,wxDefaultSize.GetHeight()));
        user_email_entry->SetHint(_("User Email"));

        password_entry=new wxTextCtrl(this,wxID_ANY,wxString(""),wxPoint(200,540),wxSize(300,wxDefaultSize.GetHeight()),wxTE_PASSWORD);
        password_entry->SetHint(_("Password"));
        wxBitmapButton *login_button=new wxBitmapButton(this,ID_LOGIN_BUTTON,wxBitmap("arrow_right-fa-64.png",wxBITMAP_TYPE_PNG),wxPoint(300+16,580),wxDefaultSize,wxBORDER_NONE);
        login_button->SetBitmapHover(wxBitmap("arrow_right-fa-64-blue.png",wxBITMAP_TYPE_PNG));

        wxFont font3(30, wxDEFAULT, wxNORMAL,wxNORMAL);
        this->SetFont(font);
        wxStaticText *not_a_user = new wxStaticText(this,wxID_STATIC,_("Not a User ?"),wxPoint(180,710));
        not_a_user->SetBackgroundColour(wxColor(255,255,255));

        wxHyperlinkCtrl *some_link=new wxHyperlinkCtrl(this, wxID_ANY, _("Sign Up Now!"), "https://kauppa.saunalahti.fi/#!/palvelut/pilvilinna",wxPoint(380,710));
        some_link->SetBackgroundColour(wxColor(255,255,255));
        Centre();


    }
    else
    {
        wxFont font(13, wxDEFAULT, wxNORMAL,wxNORMAL);
        this->SetFont(font);
        wxStaticText *i_am_pilvilina = new wxStaticText(this,wxID_STATIC,_("Elisa Pilvilina Plus"),wxPoint(120,105));
        i_am_pilvilina->SetBackgroundColour(wxColor(255,255,255));
//Create  a bigger/prettier font :D
        wxFont font2(10, wxDEFAULT, wxNORMAL,wxNORMAL);
        this->SetFont(font2);
        user_email_entry=new wxTextCtrl( this, NULL,wxString(""),wxPoint(100,255),wxSize(150,wxDefaultSize.GetHeight()));
        user_email_entry->SetHint(_("User Email"));

        password_entry=new wxTextCtrl(this,wxID_ANY,wxString(""),wxPoint(100,280),wxSize(150,wxDefaultSize.GetHeight()),wxTE_PASSWORD);
        password_entry->SetHint(_("Password"));
        wxBitmapButton *login_button=new wxBitmapButton(this,ID_LOGIN_BUTTON,wxBitmap("arrow_right-fa-32.png",wxBITMAP_TYPE_PNG),wxPoint(150+8,310),wxDefaultSize,wxBORDER_NONE);
        login_button->SetBitmapHover(wxBitmap("arrow_right-fa-32-blue.png",wxBITMAP_TYPE_PNG));

        wxFont font3(15, wxDEFAULT, wxNORMAL,wxNORMAL);
        this->SetFont(font);
        wxStaticText *not_a_user = new wxStaticText(this,wxID_STATIC,_("Not a User ?"),wxPoint(90,360));
        not_a_user->SetBackgroundColour(wxColor(255,255,255));
        wxHyperlinkCtrl *some_link=new wxHyperlinkCtrl(this, wxID_ANY, _("Sign Up Now!"), "https://kauppa.saunalahti.fi/#!/palvelut/pilvilinna",wxPoint(190,360));
        some_link->SetBackgroundColour(wxColor(255,255,255));
        Centre();

    }


}

void MyLoginDialog::OnPaint(wxPaintEvent& event)
{



//Create a temporary (stack-allocated) wxPaintDC object
    //Draw Background image
//NEVER INITIALIZE MEMBERS HERE
 int sys_width=wxSystemSettings::GetMetric (wxSYS_SCREEN_X);
    int sys_height=wxSystemSettings::GetMetric (wxSYS_SCREEN_Y);
    //wxMessageBox(wxString::Format(wxT("%i"),sys_width)+wxString(" ")+wxString::Format(wxT("%i"),sys_height));

    if(sys_width > 1024 && sys_height > 1024)
    {
        wxPaintDC paintDC(this);
        paintDC.DrawBitmap(wxBitmap(wxImage("Pilvilinna.png")),0,0);
    }
    else
    {

        wxPaintDC paintDC(this);
        paintDC.DrawBitmap(wxBitmap(wxImage("Pilvilinna_Small.png")),0,0);

    }
}


void MyLoginDialog::OnLogin(wxCommandEvent & WXUNUSED(event))
{
    //wxMessageBox(user_email_entry->GetValue());
    //wxMessageBox(password_entry->GetValue());
    //GET STUFF FROM TEXTFIELDS;
    //    wxMessageBox("LOGIN EVENT?");

    //*current_session=cloud_login(curl,(char*)user_email_entry->GetValue().ToStdString().c_str(),(char*)password_entry->GetValue().ToStdString().c_str(),3);

    std::string user_email=user_email_entry->GetValue().ToStdString();
    std::string user_password=password_entry->GetValue().ToStdString();
//    wxMessageBox(wxString(user_email)+wxString(":")+wxString(user_password));
    *current_session=cloud_login(curl,(char*)user_email.c_str(),(char*)user_password.c_str(),3);


    //PASS ACTIVE SESSION AROUND
    //??pass a session object via pointer should work ??
    if(*current_session!=NULL)
    {
        EndModal(wxID_OK);
        Destroy();
        return;
    }
    else
    {
        wxMessageBox("Wrong Username/Password Retry!");

    }

}


void MyLoginDialog::OnClose(wxCloseEvent& event)
{
    EndModal(wxID_CANCEL);
    Destroy();
    exit(0);
}
