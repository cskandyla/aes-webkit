
#if !wxUSE_WEBVIEW_WEBKIT && !wxUSE_WEBVIEW_IE
#error "A wxWebView backend is required by this sample"
#endif

#include "wx/artprov.h"
#include "wx/cmdline.h"
#include "wx/notifmsg.h"
#include "wx/settings.h"
#include "wx/webview.h"
#include "wx/webviewarchivehandler.h"
#include "wx/webviewfshandler.h"
#include "wx/infobar.h"
#include "wx/filesys.h"
#include "wx/fs_arc.h"
#include "wx/fs_mem.h"
#include "wx/taskbar.h"
#include <wx/filepicker.h>

#if wxUSE_STC
#include "wx/stc/stc.h"
#else
#error "wxStyledTextControl is needed by this sample"
#endif






#ifndef wxHAS_IMAGES_IN_RESOURCES
#include "sample.xpm"
#endif
#include "MyTaskBarIcon.h"
#include "MyLoginDialog.h"
#include "MyWebFrame.h"
#include "simple.h"




#ifndef __MYWEBAPP__
#define __MYWEBAPP__

enum LOGINSTATE {INITIAL,LOGGED_IN,LOGGED_OUT};

void initLanguageSupport(long language);

class WebApp : public wxApp
{
public:
    WebApp() :
        m_url("https://demo.whitecloud.mobi")
    {
    }

    virtual bool OnInit();

#if wxUSE_CMDLINE_PARSER
    virtual void OnInitCmdLine(wxCmdLineParser& parser)
    {
        wxApp::OnInitCmdLine(parser);

        parser.AddParam("URL to open",
                        wxCMD_LINE_VAL_STRING,
                        wxCMD_LINE_PARAM_OPTIONAL);
    }

    virtual bool OnCmdLineParsed(wxCmdLineParser& parser)
    {
        if ( !wxApp::OnCmdLineParsed(parser) )
            return false;

        if ( parser.GetParamCount() )
            m_url = parser.GetParam(0);

        return true;
    }
#endif // wxUSE_CMDLINE_PARSER

private:
    wxString m_url;
    MyTaskBarIcon  *m_taskBarIcon;
    CURL *curl;
    session_info *current_session;

};

#endif
