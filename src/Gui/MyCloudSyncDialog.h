#include <wx/wx.h>
#include <wx/listctrl.h>
#include <wx/listbook.h>
#include <queue>
#include <string>
using std::queue;
using std::wstring;
#include "../WatcherDirectories/DirectoryWatchManager.h"
#include "folder.xpm"
#ifndef __MYCLOUDSYNCDLG__
#define __MYCLOUDSYNCDLG__

enum
{
    ID_LISTBOOK = 10,
    ID_SAVE_BUTTON,
    ID_ADD_BUTTON,
    ID_DELETE_BUTTON
};

class MyCloudSyncDialog:public wxDialog
{
private:
    wxListCtrl* listCtrl;
    queue<wstring> *newlyadded;
    wxImageList* list;
    DirectoryWatchManager *dwm;
    string username;

    int itemcount;
public:
    MyCloudSyncDialog(const wxString& title,DirectoryWatchManager *dwm,std::string username);
    void OnDeleteFolderToSync(wxCommandEvent & WXUNUSED(event));
    void OnSave(wxCommandEvent & WXUNUSED(event));
    void OnAddFolderToSync(wxCommandEvent & WXUNUSED(event));
    wxListCtrl* getList();
    queue<wstring>* GetNewlyAdded();
};
#endif
