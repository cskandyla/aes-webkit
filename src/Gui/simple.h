#ifndef _SIMPLE_H_
#define _SIMPLE_H_

#include <wx/app.h>
#include <wx/frame.h>
#include <string>

using std::string;

class SimpleFrame : public wxFrame
{
public:

    SimpleFrame();
    void OnExitHide(wxCloseEvent &evt);
    void OnMove(wxMoveEvent &evt);
    void NoResize(wxSizeEvent &evt);
    void OnIconizeHide(wxIconizeEvent &evt);
    void OnMaximize(wxMaximizeEvent &evt);
    void Login(string username,string password);
    void Wait();
    void Logout();
    void DummyShow();
    void Refresh();


    //StatusBar Stuff


private:
    wxWebView *m_webview;

};



#endif
