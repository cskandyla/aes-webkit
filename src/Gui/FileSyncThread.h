#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#if !wxUSE_THREADS
#error "This sample requires thread support!"
#endif // wxUSE_THREADS

#include "wx/thread.h"
#include <curl/curl.h>
#include <deque>
#include <string>
#include "../cloud/curlcloud.h"
#include "DownloadStatusbar.h"
using std::deque;
using std::string;
using std::wstring;

#ifndef __FOLDERSYNCTHREAD__
#define __FOLDERSYNCTHREAD__


char* wchar_to_utf8(wchar_t *in);

class FileSyncThread : public wxThread
{
public:
    FileSyncThread(deque<wstring> &file_list,CURL *curl,session_info *current_session,bool *thread_done,    DownloadStatusBar *bar);
    bool Running();
    ~FileSyncThread();
protected:
    virtual wxThread::ExitCode Entry();
    deque<wstring> &file_list;
    CURL *curl;
    session_info *current_session;
    bool* thread_done;
    bool running;
    int max_size;
    DownloadStatusBar *bar;
};
#endif // __FOLDERSYNCTHREAD__
