/////////////////////////////////////////////////////////////////////////////
// Name:        webview.cpp
// Purpose:     wxWebView sample
// Author:      Marianne Gagnon
// Copyright:   (c) 2010 Marianne Gagnon, Steven Lamerton
// Licence:     wxWindows licence
/////////////////////////////////////////////////////////////////////////////

// ----------------------------------------------------------------------------
// headers
// ----------------------------------------------------------------------------

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include "wxWebApp.h"
#include "resources.h"
#include "windows.h"
#include <Exdisp.h>
#include <fstream>
#include "../CEF/webview_chromium.h"
#include "../WatcherDirectories/DirectoryWatchManager.h"


wxIMPLEMENT_APP(WebApp);

// ============================================================================
// implementation
// ============================================================================

wxLocale* locale;
long language;

void initLanguageSupport(long language)
{
    //language =  wxLANGUAGE_DEFAULT;

    // fake functions, use proper implementation
    //if( userWantsAnotherLanguageThanDefault() )

    // load language if possible, fall back to english otherwise
    //wxMessageBox(wxT("Available:")+wxString::Format(wxT("%li"),wxLocale::IsAvailable(language)));
    if(wxLocale::IsAvailable(language))
    {
        locale = new wxLocale( language );

#ifdef __WXGTK__
        // add locale search paths
        locale->AddCatalogLookupPathPrefix(wxT("/usr"));
        locale->AddCatalogLookupPathPrefix(wxT("/usr/local"));
        wxStandardPaths* paths = (wxStandardPaths*) &wxStandardPaths::Get();
        wxString prefix = paths->GetInstallPrefix();
        locale->AddCatalogLookupPathPrefix( prefix );
#endif
        locale->AddCatalogLookupPathPrefix(".");
        locale->AddCatalog("Debug");


        //wxMessageBox("Greek Added");

        if(! locale->IsOk() )
        {
            wxMessageBox("selected language is wrong");
            std::cerr << "selected language is wrong" << std::endl;
            delete locale;
            locale = new wxLocale( wxLANGUAGE_ENGLISH );
            language = wxLANGUAGE_ENGLISH;
        }
    }
    else
    {
        wxMessageBox("The selected language is not supported by your system.");
        std::cout << "The selected language is not supported by your system."
                  << "Try installing support for this language." << std::endl;
        locale = new wxLocale( wxLANGUAGE_ENGLISH );
        language = wxLANGUAGE_ENGLISH;
    }

}

bool WebApp::OnInit()
{
    int code=0;
    wxInitAllImageHandlers();
    if(!wxWebViewChromium::StartUp(code))
    {
        exit(code);
    }

    if ( !wxApp::OnInit() )
        return false;
    curl=curl_easy_init();

    //Test Greek
    initLanguageSupport(wxLANGUAGE_FINNISH);

    MyLoginDialog *login_dlg;

    //Sleep(1);

    int sys_width=wxSystemSettings::GetMetric (wxSYS_SCREEN_X);
    int sys_height=wxSystemSettings::GetMetric (wxSYS_SCREEN_Y);
    if(sys_width > 1024 && sys_height > 1024)
    login_dlg=new MyLoginDialog(wxString("Login"),curl,&current_session,1024,1024);
    else
    login_dlg=new MyLoginDialog(wxString("Login"),curl,&current_session,512,512);


    SimpleFrame *frame = new SimpleFrame();
    bool keep_alive=false;
    std::ifstream in;
    in.open("keep-alive",std::ios::in);
    if(in.is_open())
    {
        keep_alive=true;
    }

    if(!keep_alive)
    {
        if(login_dlg->ShowModal()==wxID_OK)
        {

        }
    }
    else
    {
        string username,password;
        getline(in,username);
        getline(in,password);
        in.close();

        current_session=cloud_login(curl,(char*)username.c_str(),(char*)password.c_str(),3);
    }
    //ENSURE WE HAVE A VALID SESSION

    if(current_session!=NULL)
    {



       m_taskBarIcon = new MyTaskBarIcon();
            if ( !m_taskBarIcon->SetIcon(wxICON(pilvi),_("Elisa Pilvilinna  Plus Syncronization Service!")) )
            {
                wxLogError(wxT("Could not set icon."));
            }
            frame->SetIcon(wxICON(pilvi));
            m_taskBarIcon->Init(frame,curl,current_session);
            frame->Login(current_session->user_email,current_session->password);
        frame->ShowFullScreen(true,wxFULLSCREEN_NOMENUBAR);
    }

    //SHOULD NEVER GET HERE
    return true;
}
