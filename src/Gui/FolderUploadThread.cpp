#include "FolderUploadThread.h"
#include <algorithm>
FolderUploadThread::FolderUploadThread(deque<wstring> &folder_list,CURL *curl,session_info *current_session,bool *thread_done,DownloadStatusBar *bar)
    : wxThread(wxTHREAD_DETACHED),folder_list(folder_list),curl(curl),current_session(current_session),thread_done(thread_done),bar(bar)
{
    //Session must be a valid session
    *thread_done=false;

}


wxThread::ExitCode FolderUploadThread::Entry()
{
    if(curl)
    {
        bar->Show();
        max_size=folder_list.size();
        bar->Update("Now","Starting:","Folder Upload",0,max_size);
        wchar_t cName[MAX_COMPUTERNAME_LENGTH + 1];
        unsigned long len=MAX_COMPUTERNAME_LENGTH + 1;
        GetComputerName(cName,&len);
        char name[4096];
        wcstombs(name,cName,len);
//        wxMessageBox(wxString(current_session->user_email));
        string cloud_path=current_session->user_email;
        std::replace( cloud_path.begin(), cloud_path.end(), '\\', '/');
        cloud_path+="/sync/";
        cloud_path+=name;
//        wxMessageBox(wxString(cloud_path));
        while(folder_list.size()>0)
        {
            wstring directory=folder_list.front();
            std::replace( directory.begin(), directory.end(), '\\', '/');
            //wxMessageBox(wxString("Uploading: ")+wxString(cloud_path)+":"+wxString(directory));
            //cloud_upload_unicode_folder(curl, (wchar_t*)wxString(cloud_path).wc_str(), (wchar_t*)wxString(directory).wc_str(), current_session->session_id, current_session->user_key);


            dyn_array array;
            dyn_array_alloc(&array,sizeof(filenames));
            cloud_unicode_folder_to_filenames(&array,curl,(wchar_t*)wxString(cloud_path).wc_str(),(wchar_t*)wxString(directory).wc_str(),current_session->session_id,current_session->user_key);

            for(int i=0; i<array.size; i++)
            {
                filenames *fname=(filenames*)dyn_array_get(&array,i);
                //printf("%s , %s , %s \n",fname->cloud_path,fname->filename,fname->convertedname);

                int percent=i*100/array.size;

                wxString filename=wxString(fname->filename).AfterLast('/');
                wxString foldername=wxString(fname->filename).BeforeLast('/').AfterLast('/');
                bar->Update(foldername,filename,"Folder Upload",i,array.size);
                cloud_upload_file_named(curl,fname->cloud_path,fname->filename,fname->convertedname,current_session->session_id,"DAIV","DEADDATA","unknown",current_session->user_key,1);

            }




            //wxMessageBox(wxString(cloud_path)+wxString(directory)+wxString(" Uploaded!"));
            folder_list.pop_front();
        }
        *thread_done=true;
        bar->Update("FINALY","DONE!","Folder Upload",100,100);
        bar->Show(false);
    }
    else
        wxMessageBox("No/Invalid Curl Session");
    return (wxThread::ExitCode)0; // success
}

FolderUploadThread::~FolderUploadThread()
{

    *thread_done=true;
}
