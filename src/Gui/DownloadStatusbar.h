#ifndef __DOWNLOADPROGRESSBAR__
#define __DOWNLOADPROGRESSBAR__

#include <wx/wx.h>
#include <wx/statusbr.h>
#include <wx/gauge.h>
#include <wx/frame.h>



class DownloadStatusBar : public wxFrame
{
public:
    DownloadStatusBar(wxWindow *parent ,wxPoint pos);
    wxGauge* GetProgressGauge();
    void Update(wxString foldername,wxString filename,wxString mode,int num,int total);
    void OnExitHide(wxCloseEvent &evt);
private:

    double m_value;
    wxGauge* gauge;
    wxStaticText *current_file,*current_folder;
    wxStaticText *progress_mode,*real_progress;
};
#endif // __DOWNLOADPROGRESSBAR__
