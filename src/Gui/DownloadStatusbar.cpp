#include "DownloadStatusbar.h"


DownloadStatusBar::DownloadStatusBar(wxWindow *parent,wxPoint pos)
    :
    wxFrame (NULL, wxID_ANY, _("Elisa Pilvilinna Plus Syncronization"),pos,wxSize(400,175), wxCAPTION | wxSYSTEM_MENU  | wxCLOSE_BOX | wxFULL_REPAINT_ON_RESIZE),m_value(0)
{

    wxFont font(10, wxDEFAULT, wxNORMAL,wxNORMAL);
    this->SetFont(font);
    wxPanel *dapanel=new wxPanel(this,wxID_ANY);
    dapanel->SetSize(400,150);
    dapanel->SetBackgroundColour(wxColor(255,255,255));

    wxBoxSizer *topsizer = new wxBoxSizer(wxVERTICAL);

    gauge = new  wxGauge(dapanel,wxID_ANY,100);
    gauge->SetValue(75);
    current_file = new  wxStaticText (dapanel,wxID_STATIC,_("File: None") , wxDefaultPosition, wxDefaultSize, wxST_ELLIPSIZE_END);
    current_file->SetBackgroundColour(wxColor(255,255,255));
    //current_file->SetForegroundColour(wxColor(0,255,0));
    current_file->SetMinSize(wxSize(400,30));
    current_file->Wrap(100);
    current_folder = new  wxStaticText (dapanel,wxID_STATIC,_("Folder: None") , wxDefaultPosition, wxDefaultSize, wxST_ELLIPSIZE_END);
    current_folder->SetBackgroundColour(wxColor(255,255,255));
    //current_folder->SetForegroundColour(wxColor(0,0,255));
    real_progress = new  wxStaticText (dapanel,wxID_STATIC,_("No/Progress") , wxDefaultPosition, wxDefaultSize, wxST_ELLIPSIZE_END);
    real_progress->SetBackgroundColour(wxColor(255,255,255));
    //real_progress->SetForegroundColour(wxColor(255,0,0));

    wxFlexGridSizer *flex_grid=new wxFlexGridSizer(4,2,5,5);
    flex_grid->SetFlexibleDirection(wxHORIZONTAL);
    progress_mode=new wxStaticText(dapanel,wxID_STATIC,_("Progress Mode") , wxDefaultPosition, wxDefaultSize, wxST_ELLIPSIZE_END);
    progress_mode->SetBackgroundColour(wxColor(255,255,255));
    wxStaticText *folder_static=new wxStaticText(dapanel,wxID_STATIC,_("Folder:") , wxDefaultPosition, wxDefaultSize, wxST_ELLIPSIZE_END);
    folder_static->SetBackgroundColour(wxColor(255,255,255));
    wxStaticText *file_static=new wxStaticText(dapanel,wxID_STATIC,_("Current File:") , wxDefaultPosition, wxDefaultSize, wxST_ELLIPSIZE_END);
    file_static->SetBackgroundColour(wxColor(255,255,255));
    wxStaticText *files_static=new wxStaticText(dapanel,wxID_STATIC,_("files:") , wxDefaultPosition, wxDefaultSize, wxST_ELLIPSIZE_END);
    files_static->SetBackgroundColour(wxColor(255,255,255));
    flex_grid->Add(progress_mode);
    flex_grid->Add(0,0);
    flex_grid->Add(folder_static);
    flex_grid->Add(current_folder,1,wxEXPAND);
    flex_grid->Add(files_static);
    flex_grid->Add(real_progress,1,wxEXPAND);
    flex_grid->Add(file_static);
    flex_grid->Add(current_file,1,wxEXPAND);

    //flex_grid->Add(gauge,1,wxEXPAND);
    flex_grid->AddGrowableRow(3,1);
    flex_grid->AddGrowableRow(2,1);
    flex_grid->AddGrowableRow(1,1);

    topsizer->Add(flex_grid,5,wxALL | wxEXPAND);
    topsizer->Add(gauge,1,wxALL | wxEXPAND);
    dapanel->SetSizerAndFit(topsizer);
    Centre();
    this->Bind(wxEVT_CLOSE_WINDOW, &DownloadStatusBar::OnExitHide, this);


}

void DownloadStatusBar::Update(wxString foldername,wxString filename,wxString mode,int num,int total)
{
    int percentage=num*100/total;
    gauge->SetValue(percentage);
    current_file->SetLabel(filename);
    current_folder->SetLabel(foldername);
    progress_mode->SetLabel(mode);
    real_progress->SetLabel(wxString::Format(wxT("%i"),num)+"/"+wxString::Format(wxT("%i"),total));
}


wxGauge* DownloadStatusBar::GetProgressGauge()
{
    return this->gauge;
}


void DownloadStatusBar::OnExitHide(wxCloseEvent &evt)
{
    this->Hide();
}
