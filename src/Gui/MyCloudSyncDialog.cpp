#include "MyCloudSyncDialog.h"

#include <fstream>
#include <string>
using std::ifstream;
using std::ios;
using std::string;

MyCloudSyncDialog::MyCloudSyncDialog(const wxString& title,DirectoryWatchManager *dwm,string username): wxDialog(NULL, -1, title, wxDefaultPosition,wxSize(480,320))
{


    wxInitAllImageHandlers();
    //wxSetAssertHandler 	( NULL);

    wxPanel *panel = new wxPanel(this, -1);
    wxBoxSizer *topsizer = new wxBoxSizer( wxVERTICAL );


    wxFont font(10, wxDEFAULT, wxNORMAL,wxNORMAL);
    wxStaticText *InfoBox = new wxStaticText(panel,wxID_STATIC,_("Please select the folders that you would like to keep syncronized on the cloud"));
    InfoBox->SetFont(font);


    listCtrl = new wxListCtrl(panel,ID_LISTBOOK,
                              wxDefaultPosition,wxDefaultSize,wxLC_LIST);
    newlyadded=new queue<wstring>();
    list = new wxImageList(32, 32,true,0);
    listCtrl->SetImageList(list,wxIMAGE_LIST_SMALL);
    wxBoxSizer *edit_folders_sizer = new wxBoxSizer( wxHORIZONTAL );

    wxButton* add_folder_button  = new wxButton(panel,ID_ADD_BUTTON,_("Add"));
    wxButton* delete_folder__button  = new wxButton(panel,ID_DELETE_BUTTON,_("Delete"));
    edit_folders_sizer->Add(add_folder_button,1,wxLEFT);
    edit_folders_sizer->Add(delete_folder__button,1,wxRIGHT);


    wxFont fontr(10, wxDEFAULT, wxNORMAL,wxNORMAL);
    wxStaticText *DisclaimerBox = new wxStaticText(panel,wxID_STATIC,_("Note that at the moment the syncronization is one way only!"));
    DisclaimerBox->SetFont(fontr);
    DisclaimerBox->SetForegroundColour( wxColor(*wxRED));
    wxButton* save_button  = new wxButton(panel,ID_SAVE_BUTTON,_("Save"));

    topsizer->Add(InfoBox,1,wxEXPAND);
    topsizer->Add(listCtrl,4,wxEXPAND);
    topsizer->Add(edit_folders_sizer,1,wxEXPAND);
    topsizer->Add(DisclaimerBox,1,wxEXPAND);
    topsizer->Add(save_button,1,wxBOTTOM|wxRIGHT);
    panel->SetSizerAndFit(topsizer);


    //CONNECT EVENTS
    Connect(ID_ADD_BUTTON, wxEVT_COMMAND_BUTTON_CLICKED,
            wxCommandEventHandler(MyCloudSyncDialog::OnAddFolderToSync));
    Connect(ID_DELETE_BUTTON, wxEVT_COMMAND_BUTTON_CLICKED,
            wxCommandEventHandler(MyCloudSyncDialog::OnDeleteFolderToSync));
    Connect(ID_SAVE_BUTTON, wxEVT_COMMAND_BUTTON_CLICKED,
            wxCommandEventHandler(MyCloudSyncDialog::OnSave));



    string path;
    wxIcon myfolder=wxICON(folder);

    this->dwm=dwm;
    this->username=username;
    //this->dwm->Load("Directories.json");

    vector<string> directories=dwm->DirectoriesForUser(username);
    for(int i=0; i<directories.size(); i++)
    {
        //wxMessageBox(directories[i]);
        list->Add(myfolder);
        listCtrl->InsertItem(listCtrl->GetItemCount(),directories[i],listCtrl->GetItemCount());
    }
}

void MyCloudSyncDialog::OnAddFolderToSync(wxCommandEvent & WXUNUSED(event))
{
    wxDirDialog dlg(NULL, _("Choose input directory"), "",
                    wxDD_DEFAULT_STYLE | wxDD_DIR_MUST_EXIST);

    if(dlg.ShowModal()==wxID_OK)
    {
        list->Add(wxBitmap("folder32.png",wxBITMAP_TYPE_PNG));
        listCtrl->InsertItem(listCtrl->GetItemCount(),dlg.GetPath(),listCtrl->GetItemCount());
        newlyadded->push(dlg.GetPath().ToStdWstring());

    }
}

void MyCloudSyncDialog::OnDeleteFolderToSync(wxCommandEvent & WXUNUSED(event))
{

    long itemIndex = -1;


    while ((itemIndex = listCtrl->GetNextItem(itemIndex,
                        wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED)) != wxNOT_FOUND)
    {
        // Got the selected item index
        listCtrl->DeleteItem(itemIndex);
    }
}

void MyCloudSyncDialog::OnSave(wxCommandEvent & WXUNUSED(event))
{
    vector<string> directories_to_update;
    long item = -1;
    for ( ;; )
    {
        item = listCtrl->GetNextItem(item);
        if ( item == -1 )
            break;
        // this item is selected - do whatever is needed with it
        directories_to_update.push_back(listCtrl->GetItemText(item).ToStdString());
    }

    dwm->Update(username,directories_to_update);
    EndModal(wxID_OK);
    Destroy();
}

wxListCtrl* MyCloudSyncDialog::getList()
{
    return listCtrl;
}
queue<wstring>* MyCloudSyncDialog::GetNewlyAdded()
{

    return this->newlyadded;
}


