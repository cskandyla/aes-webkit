#include "FileSyncThread.h"
#include <algorithm>

/*
char* wchar_to_utf8(wchar_t *in)
{
    printf("in:%ls\n",in);
    char *out=(char*)malloc(wcslen(in)*6);

    int sizeRequired = WideCharToMultiByte( CP_UTF8, 0, in, -1,
                                        out, 6*wcslen(in),  NULL, NULL);
    printf( "Bytes required for UTF8 encoding (excluding NUL terminator): %u\n",
             sizeRequired-1);
    printf("outlen:%d %d\n ",strlen(out),sizeRequired);
    return out;
}
*/


FileSyncThread::FileSyncThread(deque<wstring> &file_list,CURL *curl,session_info *current_session,bool *thread_done,    DownloadStatusBar *bar)
    : wxThread(wxTHREAD_DETACHED),file_list(file_list),curl(curl),current_session(current_session),thread_done(thread_done),bar(bar)
{
    //Session must be a valid session

    running=false;
    thread_done=false;
//    wxMessageBox("Starting File Sync Thread!");
}


wxThread::ExitCode FileSyncThread::Entry()
{
    running=true;
    wchar_t cName[MAX_COMPUTERNAME_LENGTH + 1];
    unsigned long len=MAX_COMPUTERNAME_LENGTH + 1;
    GetComputerName(cName,&len);
    string cloud_path=current_session->user_email;
    char name[4096];
    wcstombs(name,cName,len);
    cloud_path+="/sync/";
    cloud_path+=name;
    cloud_path+="/";


//    wxMessageBox(wxString(cloud_path));
    max_size=file_list.size();
    bar->Show();
    bar->Update("Now","Starting:","File Syncronization Queue",0,max_size);
    int i=0;

    while(file_list.size()>0)
    {
        wxString file=file_list.front();
        std::replace( file.begin(), file.end(), '\\', '/');
        std::size_t found = file.find_last_of("/\\");
        wxString folder=file.substr(0,found);
        wxString new_cloud_path=cloud_path;
        new_cloud_path+="/";
        new_cloud_path+=folder;
        //wxMessageBox(wxString("Uploading: ")+wxString(cloud_path)+wxString(file));
        //wxMessageBox(wxString("Local Path:")+wxString(file));
        // wxMessageBox(wxString("Cloud Path:")+wxString(cloud_path)+wxString(folder));
        //cloud_upload_folder(curl, (char*)cloud_path.c_str(), (char*)file.c_str(), current_session->session_id, current_session->user_key);
        //ENCODE CREATE A VALID RANDOM IV **MAYBE TRY TO IDENTIFY FILE?**
        char mb_filename[4096];
        int size=wcstombs(mb_filename,wxString(file).wc_str(),4096);
        char *folder_u=wchar_to_utf8((wchar_t*)wxString(new_cloud_path).wc_str());
        char *file_u=wchar_to_utf8((wchar_t*)wxString(file).wc_str());
//        wxMessageBox(wxString("folder_U")+wxString(folder_u));
        //      wxMessageBox(wxString("file_U")+wxString(file_u));
        bar->Update(folder,file,"File Syncronization Queue",i,max_size);
        cloud_upload_file_named(curl,folder_u,mb_filename,file_u,current_session->session_id,"DA_IVZ","DAEDDATA","file",current_session->user_key,1);
        i++;
        //cloud_upload_file(curl, (char*) new_cloud_path.c_str(), (char*) file.c_str(), current_session->session_id, "daiv", "deaddata", "file", current_session->user_key,1);
        //wxMessageBox(wxString(cloud_path)+wxString(file)+wxString(" Uploaded!"));
        file_list.pop_front();
    }
    bar->Update("FINALY","DONE!","File Sync Queue",100,100);
    bar->Show(false);
    *thread_done=true;

    return (wxThread::ExitCode)0; // success
}


bool FileSyncThread::Running()
{
    return running;
}


FileSyncThread::~FileSyncThread()
{

    *thread_done=true;
}
