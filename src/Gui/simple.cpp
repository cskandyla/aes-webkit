/////////////////////////////////////////////////////////////////////////////
// Author: Steven Lamerton
// Copyright: (c) 2013 Steven Lamerton
// Licence: wxWindows licence
/////////////////////////////////////////////////////////////////////////////

#include <wx/menu.h>
#include <wx/msgdlg.h>
#include <wx/sharedptr.h>
#include <wx/webview.h>
#include <wx/button.h>
#include <wx/sizer.h>
#include <sstream>
using std::stringstream;
#include "simple.h"
#include "../CEF/webview_chromium.h"
//wxDEFAULT_FRAME_STYLE  /*& ~(wxRESIZE_BORDER  | wxMAXIMIZE_BOX   |  wxMINIMIZE_BOX)*/
SimpleFrame::SimpleFrame() : wxFrame(NULL, wxID_ANY, "Elisa Pilvilinna Plus",wxDefaultPosition,wxDefaultSize, wxCAPTION | wxSYSTEM_MENU | wxMAXIMIZE_BOX | wxCLOSE_BOX | wxFULL_REPAINT_ON_RESIZE| wxRESIZE_BORDER)
{
    wxWebView::RegisterFactory(wxWebViewBackendChromium, wxSharedPtr<wxWebViewFactory>
                               (new wxWebViewFactoryChromium));


//    wxMessageBox("Factory Registered!");

    wxBoxSizer *topsizer = new wxBoxSizer(wxVERTICAL);


    m_webview = wxWebView::New(this, wxID_ANY, "https://pilvilinna.elisa.fi/login",
                               wxDefaultPosition, wxDefaultSize, wxWebViewBackendChromium);

//    wxMessageBox("WebView Created!");




    topsizer->Add(m_webview, 1, wxEXPAND);


    this->SetSizer(topsizer);
    this->Bind(wxEVT_CLOSE_WINDOW, &SimpleFrame::OnExitHide, this);
    //this->Bind(wxEVT_SIZE, &SimpleFrame::NoResize, this);
    //this->Bind(wxEVT_MAXIMIZE,&SimpleFrame::OnMaximize,this);

    //this->Bind(wxEVT_ICONIZE,&SimpleFrame::OnIconizeHide,this);
}

void SimpleFrame::Login(string username,string password)
{




    //((wxWebViewChromium*)m_webview)->Stop();
    stringstream ss;
    ss<<"username="<<username<<"&password="<<password<<"&status=login&desktop=1";
    //wxMessageBox(ss.str());
    CefRefPtr< CefPostDataElement > postdataelem= CefPostDataElement::Create();
    const std::string& upload_data = ss.str();
    postdataelem->SetToBytes(upload_data.size()+1,upload_data.c_str());
    CefRefPtr< CefPostData > postdata=CefPostData::Create();
    postdata->AddElement(postdataelem);
    CefRefPtr< CefRequest > request=CefRequest::Create();
    request->SetURL("https://pilvilinna.elisa.fi/login");
    request->SetMethod("POST");
    request->SetPostData(postdata);


//VERY UGLY HACK USED TO ENSURE LOGIN SINCE WINDOWS AND WXWINDOWS HATE THREAD SAFETY -- NOTICE YIELDING THE <MAIN> THREAD TO AN UNKNOWN PROCESS CREATED BY the WxWebview
    ClientHandler* m_clientHandler=((wxWebViewChromium*)m_webview)->m_clientHandler;
    bool success=false;
    while(!success)
    {
        //wxMessageBox("Sleeping");
        wxYield();
        wxMilliSleep(1000);
        if(m_clientHandler->GetBrowser())
        {

            if(!m_clientHandler->GetBrowser()->IsLoading())
            {
                ((wxWebViewChromium*)m_webview)->m_clientHandler->GetBrowser()->GetMainFrame()->LoadRequest(request);
                success=true;
            }

        }
    }




    //





}

void SimpleFrame::Logout()
{
    CefRefPtr< CefRequest > request=CefRequest::Create();
    request->SetURL("https://pilvilinna.elisa.fi/logout");
    request->SetMethod("POST");
    ((wxWebViewChromium*)m_webview)->m_clientHandler->GetBrowser()->GetMainFrame()->LoadRequest(request);
    m_webview->Reload();

}

void SimpleFrame::OnExitHide(wxCloseEvent &evt)
{
    this->Hide();
}

void SimpleFrame::OnIconizeHide(wxIconizeEvent &evt)
{
    this->Hide();
}

void SimpleFrame::NoResize(wxSizeEvent& evt)
{
    // ((wxWebViewChromium*) m_webview)->OnSize(evt);
    //evt.Skip();

//((wxWebViewChromium*) m_webview)->Reload();
}


void SimpleFrame::OnMaximize(wxMaximizeEvent &evt)
{
    //m_webview->Reload(wxWEBVIEW_RELOAD_NO_CACHE);
    evt.Skip();

}

void SimpleFrame::Wait()
{
}

void SimpleFrame::DummyShow()
{
    /*while(m_webview->IsBusy())
    {

    }*/
    this->ShowFullScreen(true,wxFULLSCREEN_NOMENUBAR);
}

void SimpleFrame::Refresh()
{
    m_webview->Reload();
}
