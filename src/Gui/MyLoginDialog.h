#include <wx/wx.h>
#include "../cloud/curlcloud.h"
#include <curl/curl.h>
#ifndef __MYLOGINDLG__
#define __MYLOGINDLG__



enum
{
    ID_LOGIN_BUTTON=20001
};

//pass an unallocated session_info pointer
//Also pass an allocated  curl handle
class MyLoginDialog:public wxDialog
{
private:
    session_info **current_session;
    CURL *curl;
    wxTextCtrl *user_email_entry;
    wxTextCtrl *password_entry;




public:
    MyLoginDialog(const wxString& title,CURL *curl,session_info **current_session,int width,int height);
    void OnLogin(wxCommandEvent & WXUNUSED(event));
    void OnQuit(wxCommandEvent & WXUNUSED(event));
    void OnCancel(wxCommandEvent & WXUNUSED(event));
    void OnClose(wxCloseEvent& event);
    void OnPaint(wxPaintEvent& event);


    DECLARE_EVENT_TABLE()
};
#endif
