
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#define PATH_MAXIMUM 4096
#include <curl/curl.h>
#include "MyTaskBarIcon.h"
#include "MyCloudSyncDialog.h"
#include "../cloud/curlcloud.h"
#include <iostream>
#include <fstream>
#include <string>
#include <dirent.h>
#include <wchar.h>
#include <locale.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
using std::endl;
using std::cout;
using std::ifstream;
using std::ofstream;
using std::string;
using std::ios;


#include <cwchar>
#include <locale.h>
#include "wxWebApp.h"

#ifdef _WIN32
#include <io.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/stat.h>
#elif defined __linux__
#endif
#ifdef WIN32
#define swprintf _snwprintf
#endif
#include <algorithm>



wxBEGIN_EVENT_TABLE(MyTaskBarIcon, wxTaskBarIcon)

EVT_MENU(PU_EXIT,    MyTaskBarIcon::OnMenuExit)
EVT_MENU(PU_LOGOUT,  MyTaskBarIcon::OnLogOut)
EVT_MENU(PU_SYNC_FOLDER,MyTaskBarIcon::OnMenuFolderSync)
EVT_MENU(PU_ENABLE_SYNC,MyTaskBarIcon::OnMenuCheckmark)
EVT_MENU(PU_SHOW,MyTaskBarIcon::OnShow)
EVT_MENU(PU_LANGUAGE_ENGLISH,MyTaskBarIcon::SetLanguage)
EVT_MENU(PU_LANGUAGE_FINNISH,MyTaskBarIcon::SetLanguage)
EVT_UPDATE_UI(PU_ENABLE_SYNC,MyTaskBarIcon::OnMenuUICheckmark)
EVT_TIMER(PU_TIMER_ID, MyTaskBarIcon::OnTimer)
EVT_TIMER(PU_REFRESH_TIMER_ID, MyTaskBarIcon::RefreshTimer)

wxEND_EVENT_TABLE()

#if defined(__WXOSX__) && wxOSX_USE_COCOA
MyTaskBarIcon::(wxTaskBarIconType iconType = wxTBI_DEFAULT_TYPE)
    :   wxTaskBarIcon(iconType), m_timer(this,PU_TIMER_ID),refresh_timer(this,PU_REFRESH_TIMER_ID)
#else
MyTaskBarIcon::MyTaskBarIcon():
    m_timer(this,PU_TIMER_ID),refresh_timer(this,PU_REFRESH_TIMER_ID)

#endif
{
    folderUploadThread=NULL;
    fileSyncThread=NULL;
    folder_thread_done=true;
    file_thread_done=true;
    m_timer.Start(20*1000);
    refresh_timer.Start(20*1000*60);
    this->downloadprogress=new DownloadStatusBar(NULL,wxPoint(wxSystemSettings::GetMetric(wxSYS_SCREEN_X)-460,wxSystemSettings::GetMetric(wxSYS_SCREEN_Y)-80));

}


void MyTaskBarIcon::Init(wxFrame *parent,CURL *curl,session_info *current_session)
{
//also init curl
    this->curl = curl;
    this->parent=parent;
    this->m_watcher=new wxFileSystemWatcher();
    this->current_session=current_session;

    m_watcher->SetOwner(this);

    Connect(wxEVT_FSWATCHER,wxFileSystemWatcherEventHandler(MyTaskBarIcon::OnFileSystemEvent));
    dwm=new DirectoryWatchManager();
    dwm->Load("Directories.json");

    vector<string> my_directories=dwm->DirectoriesForUser(current_session->user_email);

    for(int i=0; i<my_directories.size(); i++)
    {

        m_watcher->Add(wxString(my_directories[i]));
    }





}

void MyTaskBarIcon::OnEnableSync(wxCommandEvent& )
{
    //wxLogMessage(_("Sync Enabled"));
}

void MyTaskBarIcon::SetLanguage(wxCommandEvent& event)
{

    if(event.GetId()==PU_LANGUAGE_FINNISH)
    {
        initLanguageSupport(wxLANGUAGE_FINNISH);
        wxMessageBox("Setting Language to Finnish!"+wxString::Format(wxT("%i"),event.GetId()));
    }
    else if(event.GetId()==PU_LANGUAGE_ENGLISH)
    {
        initLanguageSupport(wxLANGUAGE_ENGLISH);
        wxMessageBox("Setting Language to English!"+wxString::Format(wxT("%i"),event.GetId()));
    }

}

void MyTaskBarIcon::OnLogOut(wxCommandEvent&)
{

    if(file_thread_done && folder_thread_done)
    {


        wxArrayString* watched_paths=new wxArrayString();
        m_watcher->GetWatchedPaths(watched_paths);
        vector<string> folders;
        for(int i=0; i<watched_paths->GetCount(); i++)
        {
            folders.push_back(watched_paths->Item(i).ToStdString());

            //   wxMessageBox(wxString("Adding:")+wxString(path));
        }
        dwm->Update(current_session->user_email,folders);
        dwm->Save("Directories.json");
        cloud_logout(curl,current_session->session_id);
        wxMessageBox("Logged Out");
        free(current_session->user_email);
        free(current_session->password);
        free(current_session->session_id);
        free(current_session->user_key);
        free(current_session);
        current_session=NULL;
        ((SimpleFrame*)parent)->Logout();
        //((WebFrame*)parent)->InitSession(NULL);
        //((WebFrame*)parent)->GetBrowser()->LoadURL("https://demo.whitecloud.mobi/logout");
        //((WebFrame*)parent)->SetLoggedIn(false);
        //delete parent;
        remove("keep-alive");
        int sys_width=wxSystemSettings::GetMetric (wxSYS_SCREEN_X);
    int sys_height=wxSystemSettings::GetMetric (wxSYS_SCREEN_Y);
    MyLoginDialog *login_dlg;
    if(sys_width > 1024 && sys_height > 1024)
    login_dlg=new MyLoginDialog(wxString("Login"),curl,&current_session,1024,1024);
    else
    login_dlg=new MyLoginDialog(wxString("Login"),curl,&current_session,512,512);

        parent->Hide();
        if(login_dlg->ShowModal()==wxID_OK)
        {

            //wxMessageBox("Login Successfull");
            if(current_session!=NULL )
            {
                /*char uglyhack[4096];
                //very ugly hack designed for quick demo should find a better way to do this ;)
                sprintf(uglyhack,"document.getElementById(\"username\").value=\"%s\";document.getElementById(\"password\").value=\"%s\";",current_session->user_email,current_session->password);
                ((WebFrame*)parent)->GetBrowser()->RunScript(uglyhack);
                ((WebFrame*)parent)->GetBrowser()->RunScript("document.getElementById(\"login\").submit();");
                //frame->SetLoggedIn(true);*/

                ((SimpleFrame*)parent)->Login(current_session->user_email,current_session->password);
                dwm->Load("Directories.json");
                vector<string> directories=dwm->DirectoriesForUser(current_session->user_email);

                for(int i=0; i<directories.size(); i++)
                    m_watcher->Add(wxString(directories[i]));

                parent->ShowFullScreen ( true, wxFULLSCREEN_NOMENUBAR);
            }



        }
    }
}

void MyTaskBarIcon::OnMenuExit(wxCommandEvent& )
{
//destroy curl
//join thread

    if(file_thread_done && folder_thread_done)
    {

        wxArrayString* watched_paths=new wxArrayString();
        m_watcher->GetWatchedPaths(watched_paths);
        vector<string> folders;
        for(int i=0; i<watched_paths->GetCount(); i++)
        {

            folders.push_back(watched_paths->Item(i).ToStdString());

            //   wxMessageBox(wxString("Adding:")+wxString(path));
        }
        dwm->Update(current_session->user_email,folders);
        dwm->Save("Directories.json");
        //if not logged out keep username and pass for future login
        ofstream out;
        if(current_session!=NULL)
        {
            out.open("keep-alive",ios::out);
            if(!out)
                wxMessageBox("Cant Open File!");
            else
            {
                out<<current_session->user_email<<endl<<current_session->password;
                out.close();
            }


        }


        exit(0);
        if((folderUploadThread && !folder_thread_done) ||(fileSyncThread && !file_thread_done))
        {
            wxMessageBox(_("Please Wait for the upload to finish before exiting the application"));
            return;
        }
        if(curl)
        {
            cloud_logout(curl,current_session->session_id);
            curl_easy_cleanup(curl);

            if(folderUploadThread && folder_thread_done)
            {
                delete folderUploadThread;
                folderUploadThread = NULL;
                folder_thread_done=false;
            }
        }

        exit(0);
    }
}

static bool check = true;

void MyTaskBarIcon::OnMenuCheckmark(wxCommandEvent& )
{
    check = !check;
}

void MyTaskBarIcon::OnMenuUICheckmark(wxUpdateUIEvent &event)
{
    event.Check(check);
}


void MyTaskBarIcon::OnMenuFolderSync(wxCommandEvent&)
{
    // wxLogMessage("MEssage on folder sync");


    if(curl)
    {



        MyCloudSyncDialog dlg(_("Elisa Pilvilinna Plus Syncronization Manager"),dwm,current_session->user_email);
        int itemIndex=-1;
        if(dlg.ShowModal()==wxID_OK)
        {
            queue<wstring> *newlyadded=dlg.GetNewlyAdded();
            if(newlyadded->size()>0)
            {
                while (!newlyadded->empty())
                {
//FOR EACH FOLDER UPLOAD/SYNC


                    wstring dir_name=newlyadded->front();
                    newlyadded->pop();


//cloud_upload_folder(curl,cloud_dir,dir_name,current_session->session_id,current_session->user_key);
                    directories.push_back(dir_name);
                    m_watcher->Add(wxString(dir_name));
                    addToWatcher(dir_name);
                }
                //#FIX check if either Thread is running and if it is just do nothing
                /*
                if(folder_thread_done && file_thread_done) // We are ok to go noone is uploading
                */
                /*  if(folder_thread_done)
                  {
                      if(folderUploadThread)
                      {
                          folderUploadThread = NULL;
                      }
                      folder_thread_done=false;
                  }
                  if(!folderUploadThread)
                      folderUploadThread=new FolderUploadThread(directories,curl,current_session,folder_thread_done,downloadprogress);
                  if ( folderUploadThread->Run() != wxTHREAD_NO_ERROR )
                  {
                      wxLogError("Can't create the thread!");
                      delete folderUploadThread;
                      folderUploadThread = NULL;
                  }
                  else
                  {
                //                    wxMessageBox("Folder Thread Started");
                  }*/
                if(folder_thread_done && file_thread_done)
                {
                    folderUploadThread=new FolderUploadThread(directories,curl,current_session,&folder_thread_done,downloadprogress);
                    if ( folderUploadThread->Run() != wxTHREAD_NO_ERROR )
                    {
                        wxLogError("Can't create the thread!");
                        delete folderUploadThread;
                        folderUploadThread = NULL;
                    }
                }


            }
        }
    }
    else
    {
        wxMessageBox("Curl Failed!");
    }
}




// Overridables
wxMenu *MyTaskBarIcon::CreatePopupMenu()
{
    wxMenu *menu = new wxMenu;
    menu->AppendCheckItem(PU_ENABLE_SYNC, _("&Enable Folder Syncronization"));
    menu->AppendSeparator();
    menu->Append(PU_SYNC_FOLDER, _("&Folder Syncronization Manager"));
    menu->Append(PU_SHOW, _("&Open Application"));
    wxMenu *submenu=new wxMenu;
    submenu->Append(PU_LANGUAGE_FINNISH,_("&Finnish"));
    submenu->Append(PU_LANGUAGE_ENGLISH,_("&English"));
    menu->Append(PU_LANGUAGE,_("&Language"),submenu);

    /* OSX has built-in quit menu for the dock menu, but not for the status item */
#ifdef __WXOSX__
    if ( OSXIsStatusItem() )
#endif
    {
        menu->AppendSeparator();
        menu->Append(PU_LOGOUT,    _("Log Out"));
        menu->Append(PU_EXIT,    _("Exit"));
    }
    return menu;
}

void MyTaskBarIcon::OnLeftButtonDClick(wxCommandEvent&)
{
    parent->Show();
}

void MyTaskBarIcon::OnShow(wxCommandEvent&)
{
    parent->Show();
}

static wxString GetFSWEventChangeTypeName(int changeType)
{
    switch (changeType)
    {
    case wxFSW_EVENT_CREATE:
        //GetLastCreated File

        return "CREATE";
    case wxFSW_EVENT_DELETE:
        return "DELETE";
    case wxFSW_EVENT_RENAME:
        return "RENAME";
    case wxFSW_EVENT_MODIFY:
        return "MODIFY";
    case wxFSW_EVENT_ACCESS:
        return "ACCESS";
    case wxFSW_EVENT_ATTRIB:  // Currently this is wxGTK-only
        return "ATTRIBUTE";
#ifdef wxHAS_INOTIFY
    case wxFSW_EVENT_UNMOUNT: // Currently this is wxGTK-only
        return "UNMOUNT";
#endif
    case wxFSW_EVENT_WARNING:
        return "WARNING";
    case wxFSW_EVENT_ERROR:
        return "ERROR";
    }

    return "INVALID_TYPE";
}



void MyTaskBarIcon::OnFileSystemEvent(wxFileSystemWatcherEvent &event)
{
    //Add an entry and manage later -- Folder+Num_Files to check
    if(GetFSWEventChangeTypeName(event.GetChangeType())=="CREATE")
    {
        //wxLogMessage("File System Event!");
        wxString directory=event.GetPath().GetFullPath();
        //LastFileCreated(cloud_path.c_str(),directory);
        folderstosync.insert(directory.ToStdWstring());

        cout<<"Got Here"<<endl;

        wxString entry = wxString::Format(
                             GetFSWEventChangeTypeName(event.GetChangeType())+" "+
                             event.GetPath().GetFullPath()+" "+
                             event.GetNewPath().GetFullPath()+" "+event.ToString());
        //wxMessageBox(entry);


        wxString ConcatPaths;
        for(int i=0; i<files.size(); i++)
        {
            ConcatPaths+=" "+files[i];
        }


        //wxMessageBox(ConcatPaths);

        // wxLogMessage(entry);
//        wxMessageBox( wxString::Format(wxT("%i"),file_thread_done));
        //#FIX Check if Either Thread is running and If it is do nothing else create a new thread and let it run its course


        //wxMessageBox("Thread Done:"+wxString::Format(wxT("%i"),file_thread_done));
        /*
                if(file_thread_done )
                {
                    if(fileSyncThread)
                    {
                        fileSyncThread = NULL;
                    }
                    file_thread_done=false;
                }*/



    }

}

wstring MyTaskBarIcon::LastFileCreated(const char *cloud_path,wstring directory)
{

    time_t now;
    time (&now);
    const wchar_t *dir_name=directory.c_str();
    wchar_t path[PATH_MAXIMUM];
    wstring latest;
    //STUPID IMPLEMENTATION
    //for each file encrypt and upload it//


    _WDIR * d;

    /* Open the directory specified by "dir_name". */

    d = _wopendir (dir_name);

    /* Check it was opened. */
    if (! d)
    {
        fprintf (stderr, "Cannot open directory '%ls': %ls\n",
                 dir_name, strerror (errno));
        exit (EXIT_FAILURE);
    }
    double min_diff=1000000,seconds,latest_create_time;

    while (1)
    {
        struct _wdirent * entry;
        const wchar_t * d_name;

        /* "Readdir" gets subsequent entries from "d". */
        entry = _wreaddir (d);
        if (! entry)
        {
            /* There are no more entries in this directory, so break
            out of the while loop. */
            break;
        }
        d_name = entry->d_name;

        // Check its not A Folder and Upload
        //Windows might require stat to check if its a folder
        swprintf (path,4096,L"%ls/%ls\0", dir_name, d_name);
        printf("path:%ls\n",path);
        struct _stat status;
        _wstat(path,&status);
        printf("%d\n",status.st_mode);
        if( (status.st_mode & _S_IFDIR) == 0)
        {
            //Generate iv,pass filesize as adata,**MAYBE try to identify file type **
            if (wcscmp (d_name, L"..") != 0 &&
                    wcscmp (d_name, L".") != 0)
            {

                printf("file:%ls\n",path);

                //vpccm_crypt *vpc=vpccm_crypt_init_with_key(user_key,strlen(user_key),iv,strlen(iv),adata,strlen(adata),tag_length);

                //cloud_upload_file(curl,cloud_path,path,session_id,"MY_IV","DEAD_DATA","??",user_key,1);
                printf("CLOUD_PATH:%ls\n",cloud_path);
                printf("LOCAL_PATH:%ls\n",path);
                wchar_t targetpath[PATH_MAXIMUM];

                swprintf(targetpath,4096,L"%ls/%ls",cloud_path,dir_name);
                printf("TARGET PATH:%ls\n",targetpath);
                char mb_targetpath[4096],*mb_path=(char*)malloc(4096);
                size_t size=wcstombs(mb_targetpath,targetpath,4096);
                size=wcstombs(mb_path,path,4096);



                printf("mutlibyte mb_targetpath:%s mb_path:%s\n",wchar_to_utf8(targetpath),wchar_to_utf8(path));

                //ADD TO A LIST INSTEAD OF UPLOADING [Code a quick and dirty dyn_array] and a names struct
                double seconds=difftime(now,status.st_ctime);
                if(seconds<min_diff)
                {
                    min_diff=seconds;
                    latest=path;
                    latest_create_time=status.st_ctime;
                }

                //dyn_array_add(filenames,f);
                //cloud_upload_file_named(curl,wchar_to_utf8(targetpath),mb_path,wchar_to_utf8(path),session_id,"MY_IV","DEAD_DATA","??",user_key,1);
            }
        }






        //cloud_upload_file(curl,targetpath,path,current_session->session_id,"MY_IV","DEAD_DATA","??",current_session->user_key,1);


        //Its a folder recursively upload it
    }
    /* After going through all the entries, close the directory. */
    if (_wclosedir (d))
    {
        fprintf (stderr, "Could not close '%s': %s\n",
                 dir_name, strerror (errno));
        exit (EXIT_FAILURE);
    }

    //wxMessageBox("Latest File "+wxString(latest));

    //files.push_back(latest);
    //Also make a pass and add all files with the same timestamp




    /* Open the directory specified by "dir_name". */

    d = _wopendir (dir_name);

    /* Check it was opened. */
    if (! d)
    {
        fprintf (stderr, "Cannot open directory '%ls': %ls\n",
                 dir_name, strerror (errno));
        exit (EXIT_FAILURE);
    }


    while (1)
    {
        struct _wdirent * entry;
        const wchar_t * d_name;

        /* "Readdir" gets subsequent entries from "d". */
        entry = _wreaddir (d);
        if (! entry)
        {
            /* There are no more entries in this directory, so break
            out of the while loop. */
            break;
        }
        d_name = entry->d_name;

        // Check its not A Folder and Upload
        //Windows might require stat to check if its a folder
        swprintf (path,4096,L"%ls/%ls\0", dir_name, d_name);
        printf("path:%ls\n",path);
        struct _stat status;
        _wstat(path,&status);
        printf("%d\n",status.st_mode);
        if( (status.st_mode & _S_IFDIR) == 0)
        {
            //Generate iv,pass filesize as adata,**MAYBE try to identify file type **
            if (wcscmp (d_name, L"..") != 0 &&
                    wcscmp (d_name, L".") != 0)
            {

                printf("file:%ls\n",path);

                //vpccm_crypt *vpc=vpccm_crypt_init_with_key(user_key,strlen(user_key),iv,strlen(iv),adata,strlen(adata),tag_length);

                //cloud_upload_file(curl,cloud_path,path,session_id,"MY_IV","DEAD_DATA","??",user_key,1);
                printf("CLOUD_PATH:%ls\n",cloud_path);
                printf("LOCAL_PATH:%ls\n",path);
                wchar_t targetpath[PATH_MAXIMUM];

                swprintf(targetpath,4096,L"%ls/%ls",cloud_path,dir_name);
                printf("TARGET PATH:%ls\n",targetpath);
                char mb_targetpath[4096],*mb_path=(char*)malloc(4096);
                size_t size=wcstombs(mb_targetpath,targetpath,4096);
                size=wcstombs(mb_path,path,4096);



                printf("mutlibyte mb_targetpath:%s mb_path:%s\n",wchar_to_utf8(targetpath),wchar_to_utf8(path));

                //ADD TO A LIST INSTEAD OF UPLOADING [Code a quick and dirty dyn_array] and a names struct
                double seconds=difftime(now,status.st_ctime);
                //wxMessageBox(wxString::Format(wxT("%f"),latest_create_time-status.st_ctime));
                if(latest_create_time-status.st_ctime==0)//Same Event
                {
                    //wxMessageBox(wxString("Adding:")+wxString(path));
                    files.push_back(path);
                }
                //cloud_upload_file(curl,targetpath,path,current_session->session_id,"MY_IV","DEAD_DATA","??",current_session->user_key,1);
            }
        }


        //Its a folder recursively upload it
    }


    if (_wclosedir (d))
    {
        fprintf (stderr, "Could not close '%s': %s\n",
                 dir_name, strerror (errno));
        exit (EXIT_FAILURE);
    }


    return latest;
}
void MyTaskBarIcon::addToWatcher(wstring directory)
{
    //wxLogMessage("Watcher ADD");
//    wxMessageBox("Adding "+wxString(directory)+" to the watcher");
    this->m_watcher->Add(wxString(directory));
    const wchar_t *dir_name=directory.c_str();


    //Its a folder recursively upload it

    wchar_t path[PATH_MAXIMUM];
    //STUPID IMPLEMENTATION
    //for each file encrypt and upload it//

    _WDIR * d;

    /* Open the directory specified by "dir_name". */

    d = _wopendir (dir_name);

    /* Check it was opened. */
    if (! d)
    {
        fprintf (stderr, "Cannot open directory '%s': %s\n",
                 dir_name, strerror (errno));
        exit (EXIT_FAILURE);
    }
    while (1)
    {
        struct _wdirent * entry;
        const wchar_t * d_name;

        /* "Readdir" gets subsequent entries from "d". */
        entry = _wreaddir (d);
        if (! entry)
        {
            /* There are no more entries in this directory, so break
            out of the while loop. */
            break;
        }
        d_name = entry->d_name;

        // Check its not A Folder and Upload
        //Windows might require stat to check if its a folder
        _snwprintf (path,4096,L"%ls/%ls\0", dir_name, d_name);
        printf("path:%ls\n",path);
        struct _stat status;
        _wstat(path,&status);
        //Its a folder recursively upload it
        if((status.st_mode & S_IFDIR) != 0)
        {
            /* Check that the directory is not "d" or d's parent. */
            if (wcscmp (d_name, L"..") != 0 &&
                    wcscmp (d_name, L".") != 0)
            {
                int path_length;

                path_length = swprintf (path,4096,
                                        L"%s\\%s", dir_name, d_name);
                if (path_length >= PATH_MAXIMUM)
                {
                    fprintf (stderr, "Path length has got too long.\n");
                    exit (EXIT_FAILURE);
                }
                /* Recursively Add subfolders */
                addToWatcher(path);

            }
        }
    }
    /* After going through all the entries, close the directory. */
    if (_wclosedir (d))
    {
        fprintf (stderr, "Could not close '%s': %s\n",
                 dir_name, strerror (errno));
        exit (EXIT_FAILURE);
    }
}

void MyTaskBarIcon::OnTimer(wxTimerEvent& event)
{
    if(curl)
    {
        if(current_session!=NULL)
        {
            //wxMessageBox("Pinging !");
            cloud_ping(curl,current_session->session_id);



            std::set<wstring>::iterator it;


            if(file_thread_done && folder_thread_done)
            {

                string cloud_path=current_session->user_email;
                std::replace( cloud_path.begin(), cloud_path.end(), '\\', '/');
                cloud_path+="/sync";


                for(it=folderstosync.begin(); it!=folderstosync.end(); ++it)
                {
                    LastFileCreated(cloud_path.c_str(),*it);
                    folderstosync.erase(*it);
                }
                //wxMessageBox(wxString("NumFiles to Sync:")+wxString::Format(wxT("%i"),files.size()));
                if(files.size()>0 && file_thread_done && folder_thread_done)
                {

                    fileSyncThread=new FileSyncThread(files,curl,current_session,&file_thread_done,downloadprogress);

                    if ( fileSyncThread->Run() != wxTHREAD_NO_ERROR )
                    {
                        wxLogError("Can't create the thread!");
                        wxMessageBox("Can't create the thread!");
                        delete fileSyncThread;
                        fileSyncThread = NULL;
                    }
                    else
                    {
//            wxMessageBox("FileSync Thread Started");
                    }
                }

            }
        }
    }

}

void MyTaskBarIcon::RefreshTimer(wxTimerEvent& event)
{
    //wxMessageBox("Relogging");
    if(current_session!=NULL)
    {
        ((SimpleFrame*)parent)->Login(current_session->user_email,current_session->password);
    }
}

