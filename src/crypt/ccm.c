#include "ccm.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

void aes_encrypt(vpccm *vp,uint8_t *bytes,uint8_t* cipher_bytes);
void generate_counter_block(vpccm *vp,uint8_t *buffer);
uint8_t* get_tag(vpccm *vp);
void PrintHex(uint8_t* bytes,int length);

void init_vpccm(vpccm *vp)
{
    vp->exit_next=-1;
    vp->cc=(uint8_t*)malloc(vp->block_size*vp->block_count);
    vp->Y=(uint8_t*)malloc(vp->block_size);
    vp->S0=(uint8_t*)malloc(vp->block_size);
    vp->ctr=(uint8_t*)malloc(vp->block_size);
    vp->ctr_cipher=(uint8_t*)malloc(vp->block_size);

    generate_counter_block(vp,vp->ctr);
    aes_encrypt(vp,vp->ctr,vp->S0);

    for(int i=0; i<vp->block_size; i++)
    {
        vp->Y[i]=0;
    }

}


void free_vpccm(vpccm *vp)
{
    free(vp->Y);
    free(vp->S0);
    free(vp->ctr);
    free(vp->ctr_cipher);
    free(vp->cc);
    free(vp);

}


vpccm*  vpccm_init_with_key(uint8_t *key,int key_size,uint8_t *iv,int iv_size,uint8_t *adata,int adata_size,int taglength,long long file_size)
{


    vpccm *vp=(vpccm*)malloc(sizeof(vpccm));
    vp->block_size=AES128BLOCKSIZE;
    vp->block_count=BLOCKCOUNT;
    vp->tag_length=taglength;
    vp->key=key;
    vp->iv=iv;
    vp->adata=adata;
    vp->key_size=key_size;
    vp->iv_size=iv_size;
    vp->adata_size=adata_size;

    vp->file_size=file_size;
    vp->bytes_left=file_size;
    init_vpccm(vp);
    return vp;
}





long format_assoc_data(vpccm *vp,uint8_t* buffer)
{

    int payload_length=1;
    int value=vp->adata_size;

    if(vp->adata_size==0)
    {
        *(buffer)=0;
    }
    else if(vp->adata_size<=0xFEFF)
    {
        for(int i=1; i>=0; i--)
        {
            buffer[i]=value & 0xFF;
            value=value >> 8;
        }
        payload_length=2;

    }
    else
    {
        fprintf(stderr,"Invalid adatalength should be <=65279");

    }

    int padding_length=(vp->block_size-vp->adata_size%vp->block_size)-payload_length;


    for(int i=0; i<vp->adata_size; i++)
    {

        buffer[i+payload_length]=vp->adata[i];

    }

    for(int i=0; i<padding_length; i++)
    {
        buffer[payload_length+vp->adata_size+i]=0;
    }


    if(padding_length<0)
    {
        padding_length=0;
    }


    return payload_length+vp->adata_size+padding_length;
}




void format_header_with_payload_length(vpccm *vp,long long payload_length,uint8_t *buffer)
{

    int qLen=15-vp->iv_size;
    int fl=0;

    fl |= (vp->adata_size>0)?0x40:0;
    fl |= ((((vp->tag_length-2)/2) & 0x07) << 3);
    fl |=((qLen -1) & 0x07);

    *(buffer)=fl;


    for(int i=0; i<qLen; i++)
    {
        // *(buffer+vp->block_size-i-1)=payload_length & 255;
        buffer[vp->block_size-i-1]=payload_length & 255;
        payload_length >>=8;
    }

    for( int i=0; i<vp->iv_size; i++)
    {
        // *(buffer+1+i)=*(vp.iv+i);
        buffer[i+1]=vp->iv[i];
    }

}


void format_payload(vpccm *vp,uint8_t *plaindatablock,uint8_t *buffer,int length)
{

    int pad=length%vp->block_size;

    for(int i=0; i<length; i++)
    {
        //*(buffer+i)=*(plaindatablock+i);
        buffer[i]=plaindatablock[i];
    }

    if(pad>0)
    {
        for(int i=0; i<vp->block_size-pad; i++)
        {
            //*(buffer+length+i)=0;
            buffer[length+i]=0;
        }

    }

}



long formatting_nap(vpccm *vp,uint8_t *plaindatablock,uint8_t *buffer,int payload_length)
{

    if(vp->exit_next==-1)
    {
        format_header_with_payload_length(vp,vp->file_size,buffer);
        long adata_len=vp->block_size;

        if(vp->adata_size>0)
        {
            adata_len+=format_assoc_data(vp,buffer+vp->block_size);
        }
        format_payload(vp,plaindatablock,buffer+adata_len,payload_length);
        return adata_len+vp->block_size;
    }
    else
    {
        format_payload(vp,plaindatablock,buffer,payload_length);
    }

    return -1;
}




void generate_counter_block(vpccm *vp,uint8_t *buffer)
{
    int qLen=15-vp->iv_size;

    int fl=0x0;

    fl |=(qLen-1) & 0x07;
    *(buffer)=fl;

    for(int i=0; i<vp->iv_size; i++)
    {
        //*(buffer+i+1)=*(vp.iv+i);
        buffer[i+1]=vp->iv[i];
    }

    for(int i=vp->iv_size+1; i<vp->block_size; i++)
    {
        //*(buffer+i)=0;
        buffer[i]=0;
    }


}


void BytesXorBytes(uint8_t* bytes_left,uint8_t* bytes_right,uint8_t *outbuff,int length)
{

    for(int i=0; i<length; i++)
    {
        outbuff[i]=bytes_left[i] ^ bytes_right[i];
    }

}


void aes_encrypt(vpccm *vp,uint8_t *bytes,uint8_t* cipher_bytes)
{

    AES128_ECB_encrypt(bytes,vp->key,cipher_bytes);

}




uint8_t* encrypt_block(vpccm *vp,uint8_t *bytes,int length)
{

    vp->bytes_left-=length;
    //int sub_block_count=ceil(length/(float)vp->block_size);
    int sub_block_count=myceil(length,vp->block_size);
    int padding_count=length%vp->block_size;

    for(int i=0; i<sub_block_count; i++)
    {

        uint8_t *sub_block_bytes=bytes+i*vp->block_size;
        int sub_block_length=(i==sub_block_count-1 && padding_count >0)?padding_count:vp->block_size;
        //int napl_bytes_length=(ceil((vp->adata_size)/(float)vp->block_size)*vp->block_size)+3*vp->block_size;
        int napl_bytes_length=(myceil(vp->adata_size,vp->block_size)*vp->block_size)+3*vp->block_size;

        uint8_t *xorbytes=(uint8_t*)malloc(sub_block_length);
        uint8_t *napl_bytes=(uint8_t*)malloc(napl_bytes_length);
        uint8_t *y_bytes=(uint8_t*)malloc(vp->block_size);
        uint8_t *y_cipher=(uint8_t*)malloc(vp->block_size);

        napl_bytes_length=formatting_nap(vp,sub_block_bytes,napl_bytes,sub_block_length);

        if(vp->exit_next==-1)
        {
            vp->exit_next=0;
            for(int i=0; i<napl_bytes_length; i+=vp->block_size)
            {
                BytesXorBytes(napl_bytes+i,vp->Y,y_bytes,vp->block_size);
                aes_encrypt(vp,y_bytes,y_cipher);
                //array_cp(vp->Y,y_cipher,vp->block_size);
                memcpy(vp->Y,y_cipher,vp->block_size);

            }
        }
        else
        {
            BytesXorBytes(napl_bytes,vp->Y,y_bytes,vp->block_size);
            aes_encrypt(vp,y_bytes,y_cipher);
            //array_cp(vp->Y,y_cipher,vp->block_size);
            memcpy(vp->Y,y_cipher,vp->block_size);
        }

        int n=0;
        for(int j=15; j>0; j--)
        {
            //n=*(vp.ctr+j);
            n=vp->ctr[j];
            //*(vp.ctr+j)=(n+1) & 255;
            vp->ctr[j]=(n+1) & 255;

            if(vp->ctr[j]!='\0')
            {
                break;
            }

        }

        aes_encrypt(vp,vp->ctr,vp->ctr_cipher);
        BytesXorBytes(sub_block_bytes,vp->ctr_cipher,xorbytes,sub_block_length);


        for(int j=0; j<sub_block_length; j++)
        {
            //*(vp.cc+i*vp.block_size+j)=*(xorbytes+j);
            vp->cc[i*vp->block_size+j]=xorbytes[j];
        }
        if(sub_block_length<vp->block_size || (vp->bytes_left ==0 && sub_block_length ==vp->block_size))
        {
            vp->exit_next=1;
        }

        free(xorbytes);
        free(napl_bytes);
        free(y_bytes);
        free(y_cipher);

    }
    return vp->cc;
}



uint8_t* decrypt_block(vpccm *vp,uint8_t *buffer,int length,int *exit_next)
{
    vp->bytes_left-=length;
    //int sub_block_count=ceil(length/(float)vp->block_size);
    int sub_block_count=myceil(length,vp->block_size);
    int padding_count=length % vp->block_size;


    for(int i=0; i<sub_block_count; i++)
    {
        uint8_t *sub_block_bytes=buffer+i*vp->block_size;
        int sub_block_length=(i==sub_block_count-1 && padding_count>0)?padding_count:vp->block_size;
        uint8_t *xorbytes=(uint8_t*)malloc(sub_block_length);

        int n=0;
        for(int j=15; j>0; j--)
        {
            //n=*(vp.ctr+j);
            n=vp->ctr[j];
            //*(vp.ctr+j)=(n+1) & 255;
            vp->ctr[j]=(n+1) & 255;

            if(vp->ctr[j]!='\0')
            {
                break;
            }
        }
        aes_encrypt(vp,vp->ctr,vp->ctr_cipher);
        BytesXorBytes(sub_block_bytes,vp->ctr_cipher,xorbytes,sub_block_length);
        for(int j=0; j<sub_block_length; j++)
        {
            //*(vp->cc+i*vp.block_size+j)=*(xorbytes+j);
            vp->cc[i*vp->block_size+j]=xorbytes[j];
        }
        free(xorbytes);

        if(sub_block_length<vp->block_size || (vp->bytes_left ==0  && sub_block_length == vp->block_size))
        {
            vp->exit_next=1;
        }

    }
    if(exit_next)
    {
        *exit_next=vp->exit_next;
    }


    return vp->cc;
}

void verify_tag_with_block(vpccm *vp,uint8_t* buffer,unsigned int length)
{
    vp->bytes_left-=length;
    //int sub_block_count=ceil(length/(float)vp->block_size);
    int sub_block_count=myceil(length,vp->block_size);
    int padding_count=length%vp->block_size;

    for(int i=0; i<sub_block_count; i++)
    {
        uint8_t* sub_block_bytes=buffer+i*vp->block_size;

        int sub_block_length=(i==sub_block_count-1 && padding_count>0)?padding_count: vp->block_size;
        //int napl_bytes_length=(ceil((vp->adata_size)/(float)vp->block_size)*vp->block_size)+3*vp->block_size;
        int napl_bytes_length=(myceil(vp->adata_size,vp->block_size)*vp->block_size)+3*vp->block_size;
        uint8_t *xorbytes=(uint8_t*)malloc(sub_block_length);
        uint8_t *napl_bytes=(uint8_t*)malloc(napl_bytes_length);
        uint8_t *y_bytes=(uint8_t*)malloc(vp->block_size);
        uint8_t *y_cipher=(uint8_t*)malloc(vp->block_size);


        int n=0;

        for( int j=15; j>0; j--)
        {
            //n=*(vp.ctr+j);
            n=vp->ctr[j];
            //*(vp.ctr+j)=(n+1) & 255;
            vp->ctr[j]=(n+1) & 255;

            if((vp->ctr[j]) != '\0')
            {
                break;
            }

        }

        aes_encrypt(vp,vp->ctr,vp->ctr_cipher);
        BytesXorBytes(sub_block_bytes,vp->ctr_cipher,xorbytes,sub_block_length);
        napl_bytes_length=formatting_nap(vp,xorbytes,napl_bytes,sub_block_length);

        if(vp->exit_next==-1)
        {

            vp->exit_next=0;

            for(int i=0; i<napl_bytes_length; i+=vp->block_size)
            {
                BytesXorBytes(napl_bytes+i,vp->Y,y_bytes,vp->block_size);
                aes_encrypt(vp,y_bytes,y_cipher);
                memcpy(vp->Y,y_cipher,vp->block_size);

            }

        }
        else
        {
            BytesXorBytes(napl_bytes,vp->Y,y_bytes,vp->block_size);
            aes_encrypt(vp,y_bytes,y_cipher);
            memcpy(vp->Y,y_cipher,vp->block_size);
        }

        if(sub_block_length<vp->block_size || (vp->bytes_left == 0 && sub_block_length == vp->block_size))
        {
            vp->exit_next=1;
        }



        free(xorbytes);
        free(napl_bytes);
        free(y_bytes);
        free(y_cipher);

    }


}





uint8_t* get_tag(vpccm *vp)
{

    uint8_t *tag_buffer=(uint8_t*)malloc(vp->block_size);
    BytesXorBytes(vp->Y,vp->S0,tag_buffer,vp->block_size);
    uint8_t *tag=(uint8_t*)malloc(vp->tag_length);
    memcpy(tag,tag_buffer,vp->tag_length);
    free(tag_buffer);
    return tag;
}


int verify_tag_with_data(vpccm *vp,uint8_t* data,int data_length)
{


    generate_counter_block(vp,vp->ctr);
    aes_encrypt(vp,vp->ctr,vp->S0);



    uint8_t *T=(uint8_t*)malloc(vp->tag_length);
    uint8_t* tag=(uint8_t*)malloc(vp->tag_length);
    memcpy(tag,data+data_length-vp->tag_length,vp->tag_length);

    BytesXorBytes(tag,vp->S0,T,vp->tag_length);


    int data_part_length=data_length-vp->tag_length;
    uint8_t *data_part=(uint8_t*)malloc(data_part_length);//NSDATA
    memcpy(data_part,data,data_part_length);


    for(int i=0; i<vp->block_size; i++)
    {
        vp->Y[i]=0;

    }

    vp->exit_next=-1;


    unsigned int buffer_size=vp->block_count*AES128BLOCKSIZE;
    vp->file_size-=vp->tag_length;
    //int loop_count=ceil(data_part_length/(float)buffer_size);
    int loop_count=myceil(data_part_length,buffer_size);
    uint8_t *bytes=(uint8_t*)data_part;


    for(int i=0; i<loop_count; i++)
    {

        if(i!=loop_count-1)
        {
            verify_tag_with_block(vp,bytes+i*buffer_size,buffer_size);
        }
        else
        {
            int len=data_part_length%buffer_size;
            verify_tag_with_block(vp,bytes+i*buffer_size,len);
        }

    }

    uint8_t* data_tag=(uint8_t*)malloc(vp->tag_length);//NSDATA
    memcpy(data_tag,T,vp->tag_length);
    free(T);
    uint8_t* calculated_tag=(uint8_t*)malloc(vp->tag_length);//NSDATA
    memcpy(calculated_tag,vp->Y,vp->tag_length);



    if(memcmp(data_tag,calculated_tag,vp->tag_length)==0)
    {

        return 1;
    }


    return 0;

}


int verify_tag_with_file_URL(vpccm *vp,char *input_file)
{

    generate_counter_block(vp,vp->ctr);
    aes_encrypt(vp,vp->ctr,vp->S0);
    uint8_t *T=(uint8_t*)malloc(vp->tag_length);
    FILE* fp=fopen(input_file,"r+");
    fseek(fp,vp->file_size-vp->tag_length,SEEK_SET );//SEEK TO file_size-tag_length
    long tag_pos=ftell(fp);
    uint8_t* tag_buffer=(uint8_t*)malloc(sizeof(vp->tag_length));//FREAD TAG DATA;
    int read=fread(tag_buffer,sizeof(uint8_t),vp->tag_length,fp);
    BytesXorBytes(tag_buffer,vp->S0,T,vp->tag_length);
    rewind(fp);
    fclose(fp);
    // PrintHex(tag_buffer,16);
    for(int i=0; i<vp->block_size; i++)
    {
        vp->Y[i]=0;
    }

    vp->exit_next=-1;
    unsigned int buffer_size=vp->block_count*AES128BLOCKSIZE;
    uint8_t buffer[buffer_size];
    FILE *infp=fopen(input_file,"r+");
    vp->file_size-=vp->tag_length;
    int bytesread=0;
    while(!feof(infp))
    {
        long bytesread=fread(buffer,sizeof(uint8_t),buffer_size,infp);

        if(vp->bytes_left-bytesread<=vp->block_size)
        {

            bytesread-=vp->tag_length-(vp->bytes_left-bytesread);

            if(bytesread==0)
            {
                break;
            }


        }
        verify_tag_with_block(vp,buffer,bytesread);

        if(vp->exit_next==1)
        {
            break;
        }


    }
    fclose(infp);
    uint8_t* file_tag=(uint8_t*)malloc(vp->tag_length);//NSDATA*
    memcpy(file_tag,T,vp->tag_length);
    uint8_t* calculated_tag=(uint8_t*)malloc(vp->tag_length);
    memcpy(calculated_tag,vp->Y,vp->tag_length);
    free(T);


    if(memcmp(file_tag,calculated_tag,vp->tag_length)==0)
    {
        return 1;
    }

    return 0;

}





void PrintHex(uint8_t* bytes,int length)
{
    for(int i=0; i<length; i++)
        printf("0x%x\n",bytes[i]);

}

int myceil (int one, int two)
{
    if ( (one/(float)two) > (float)(one/two) )
    {
        return (one/two)+1;
    }

    return (one/two);
}
/*
int main(int argc, char *argv[])
{


  uint8_t *key="dakey", *iv="daiv", *adata="deadata";
  uint8_t *data="The quick brown fox jumps over the lazy dog";
  int taglength=16;
  long long file_size=512;
  printf("Initializing vpccm\n");
  vpccm *vp=vpccm_init_with_key(key,strlen(key),iv,strlen(iv),adata,strlen(adata),taglength,file_size);
  uint8_t *encrypted_block=malloc(strlen(data)*sizeof(uint8_t));
  uint8_t *decrypted_block=malloc(strlen(data)*sizeof(uint8_t));
  printf("BLOCK ENCRYPTION/DECRYPTION TESTS\n");
  printf("***Encrypting Block:\"%s\"***\n",data);
  memcpy(encrypted_block,encrypt_block(vp,data,strlen(data)),strlen(data));
  int en=1;
  vpccm *nvp=vpccm_init_with_key(key,5,iv,4,adata,8,taglength,file_size);
  memcpy(decrypted_block,decrypt_block(nvp,encrypted_block,strlen(data),&en),strlen(data));
  printf("***Decrypted  Block:\"");
  for(int i=0;i<strlen(data);i++)
    {
      printf("%c",decrypted_block[i]);
    }
  printf("\"***\n");
  printf("##comparison of initial with the decrypted block %d##\n",memcmp(data,decrypted_block,strlen(data)));
  printf("TAG FUNCTIONS TESTS\n");
  uint8_t *tag=get_tag(vp);
  printf("***Testing verify_tag_with_data:%d***\n",verify_tag_with_data(vp,data,strlen(data)));
  printf("***Testing verify_tag_with_fileURL:%d***\n",verify_tag_with_file_URL(vp,"testfile"));
  printf("FREEING AND DEALLOCATING\n");
  free_vpccm(vp);
  free_vpccm(nvp);
  free(encrypted_block);
  free(decrypted_block);

  return 0;
}
*/
