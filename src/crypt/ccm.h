//PUBLIC INTERFACE FUNCTIONS

//MAYBE DEFINE A STRUCT?

#ifndef __VPCCM__
#define __VPCCM__

#define AES128BLOCKSIZE 16
#define BLOCKCOUNT 1024
#include <stdint.h>
#include "aes.h"


typedef
struct vpccm
{
    int tag_length,block_size,block_count,exit_next,key_size,iv_size,adata_size;
    long long bytes_left,file_size;
    uint8_t *key,*iv,*ctr,*adata,*Y,*S0,*ctr_cipher,*cc;

} vpccm;

vpccm*  vpccm_init_with_key(uint8_t *key,int key_size,uint8_t *iv,int iv_size,uint8_t *adata,int adata_size,int taglength,long long file_size);
uint8_t* encrypt_block(vpccm *vp,uint8_t *bytes,int length);
uint8_t* decrypt_block(vpccm *vp,uint8_t *buffer,int length,int *exit_next);

uint8_t* get_tag(vpccm *vp);

int verify_tag_with_data(vpccm *vp,uint8_t* data,int data_length);
int verify_tag_with_file_URL(vpccm *vp,char *input_file);
int myceil (int one, int two);

void init_vpccm(vpccm *vp);
void free_vpccm(vpccm *vp);
#endif
