#include "ccm.h"



typedef struct vpccm_crypt
{
    int tag_length,buffer_size,block_size;
    long long bytes_left;
    uint8_t *key,*iv,*adata;
    int key_size,iv_size,adata_size;


} vpccm_crypt;




vpccm_crypt* vpccm_crypt_init_with_key(uint8_t *key,int key_size,uint8_t *iv,int iv_size,uint8_t *adata,int adata_size,int tag_length);

uint8_t*  encrypt_stream_with_URL(vpccm_crypt *vpc,char *url,long *encrypted_stream_length);
uint8_t*  decrypt_stream_with_URL(vpccm_crypt *vpc,char *url,long *decrypted_stream_length);

uint8_t*  encrypt_stream_with_file(vpccm_crypt *vpc,char *url,long *encrypted_stream_length);
uint8_t*  decrypt_stream_with_fileL(vpccm_crypt *vpc,char *url,long *decrypted_stream_length);

void encrypt_file_to_file_with_sourceURL(vpccm_crypt *vpc,char *sourceURL,char *destURL);
void decrypt_file_to_file_with_sourceURL(vpccm_crypt *vpc,char *sourceURL,char *destURL);


void encrypt_file_to_file(vpccm_crypt *vpc,char *source,char *dest);
void decrypt_file_to_file(vpccm_crypt *vpc,char *source,char *dest);


uint8_t*  encrypt_data_with_data(vpccm_crypt *vpc,uint8_t  *data,int data_length,long *encrypted_length);
uint8_t*  decrypt_data_with_data(vpccm_crypt *vpc,uint8_t *data,int data_length,long *decrypted_length);

