#include "ccmcrypt.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>


vpccm_crypt* vpccm_crypt_init_with_key(uint8_t *key,int key_size,uint8_t *iv,int iv_size,uint8_t *adata,int adata_size,int tag_length)
{

    vpccm_crypt *vpc=(vpccm_crypt*)malloc(sizeof(vpccm_crypt));
    vpc->key=key;
    vpc->key_size=key_size;
    vpc->iv=iv;
    vpc->iv_size=iv_size;
    vpc->adata=adata;
    vpc->adata_size=adata_size;
    vpc->tag_length=tag_length;
    vpc->block_size=AES128BLOCKSIZE;
    vpc->buffer_size=BLOCKCOUNT*vpc->block_size;
    return vpc;
}



//MODIFY THIS TO WORK WITH URLS INSTEAD OF FILES CURL LOAD INTO MEMORY AND HANDLE IT LIKE DATA?
uint8_t*  encrypt_stream_with_URL(vpccm_crypt *vpc,char *url,long *encrypted_stream_length)
{
    FILE* fp=fopen(url,"r+");
    fseek(fp,0,SEEK_END);
    long file_size=ftell(fp);
    rewind(fp);
    vpc->bytes_left=file_size;
    vpccm* vp=vpccm_init_with_key(vpc->key,vpc->key_size,vpc->iv,vpc->iv_size,vpc->adata,vpc->adata_size,vpc->tag_length,file_size);

    uint8_t *encoded_stream=(uint8_t*)malloc(file_size+vpc->tag_length);
    uint8_t buffer[vpc->buffer_size];
    uint8_t *encrypted=NULL;
    long length=0;
    while(!feof(fp))
    {
        long bytesread=fread(buffer,sizeof(uint8_t),vpc->buffer_size,fp);
        if(bytesread>0)
        {
            encrypted=encrypt_block(vp,buffer,bytesread);
            memcpy(encoded_stream+length,encrypted,bytesread);
            length+=bytesread;
        }
    }
    uint8_t *tag=get_tag(vp);
    memcpy(encoded_stream+length,tag,vpc->tag_length);
    fclose(fp);
    *encrypted_stream_length=length+vpc->tag_length;
    return encoded_stream;
}

void encrypt_file_to_file_with_sourceURL(vpccm_crypt *vpc,char *sourceURL,char *destURL)
{

    FILE* ofp=fopen(destURL,"w+");
    long out_length;
    uint8_t* encrypted_srteam=encrypt_stream_with_URL(vpc,sourceURL,&out_length);
    fwrite(encrypted_srteam,sizeof(uint8_t),out_length,ofp);
    fclose(ofp);
}



//MODIFY THIS TO WORK WITH URLS INSTEAD OF FILES CURL LOAD INTO MEMORY AND HANDLE IT LIKE DATA?
uint8_t*  decrypt_stream_with_URL(vpccm_crypt *vpc,char *url,long *decrypted_stream_length)
{

    FILE* fp=fopen(url,"r+");
    fseek(fp,0,SEEK_END);
    long file_size=ftell(fp);
    rewind(fp);
    printf("File Size: %ld\n",file_size);
    vpc->bytes_left=file_size;

    vpccm* vp=vpccm_init_with_key(vpc->key,vpc->key_size,vpc->iv,vpc->iv_size,vpc->adata,vpc->adata_size,vpc->tag_length,file_size);
    if(verify_tag_with_file_URL(vp,url))
    {
        printf("Tag Verified!!\n");
        /*UGLY HACK SEE IF I'VE DONE SOMETHING VERY STUPID*/
        vp->file_size=file_size-vpc->tag_length;
        vp->bytes_left=file_size-vpc->tag_length;
        init_vpccm(vp);

        uint8_t *decrypted_stream=(uint8_t*)malloc(file_size-vpc->tag_length);
        uint8_t buffer[vpc->buffer_size];
        int length=0;
        while(!feof(fp))
        {

            long bytesread=fread(buffer,sizeof(uint8_t),vpc->buffer_size,fp);
            if(vpc->bytes_left-bytesread<=vpc->block_size)
            {
                bytesread-=vpc->tag_length-(vpc->bytes_left-bytesread);
                if(bytesread==0)
                {
                    break;
                }
            }

            int exit_next=0;
            uint8_t  *decrypted=decrypt_block(vp,buffer,bytesread,&exit_next);
            memcpy(decrypted_stream+length,decrypted,bytesread);
            length+=bytesread;
            vpc->bytes_left-=bytesread;
            int is_last_block=(exit_next==1);

            if(is_last_block)
            {
                break;
            }

        }
        *decrypted_stream_length=length;
        return decrypted_stream;
    }
    else
    {
        fprintf(stderr,"Invalid Tag, ErrorNumber:1\n");
        *decrypted_stream_length=-1;
        return NULL;
    }


}




void decrypt_file_to_file_with_sourceURL(vpccm_crypt *vpc,char *sourceURL,char *destURL)
{

    FILE *ofp=fopen(destURL,"w+");

    long out_length=0;
    uint8_t* decrypted_stream=decrypt_stream_with_URL(vpc,sourceURL,&out_length);
    printf("out_length %ld\n",out_length);
    fwrite(decrypted_stream,sizeof(uint8_t),out_length,ofp);

    fclose(ofp);

}



uint8_t*  encrypt_data_with_data(vpccm_crypt *vpc,uint8_t  *data,int data_length,long *encrypted_length)
{
    int loop_count=ceil(data_length/(float)vpc->buffer_size);

    vpccm *vp=vpccm_init_with_key(vpc->key,vpc->key_size,vpc->iv,vpc->iv_size,vpc->adata,vpc->adata_size,vpc->tag_length,data_length);

    uint8_t* temp_cipher=(uint8_t*)malloc(loop_count*vpc->buffer_size);
    uint8_t *bytes=data;
    int length=0;

    int len=0;
    for(int i=0; i<loop_count; i++)
    {
        uint8_t* encrypted=NULL;
        if(i!=loop_count-1)
        {
            encrypted,encrypt_block(vp,bytes+i*vpc->buffer_size,vpc->buffer_size);
            len=vpc->buffer_size;

        }
        else
        {
            len=data_length%vpc->buffer_size;
            encrypted=encrypt_block(vp,bytes+i*vpc->buffer_size,len);

        }

        //cipher append to temp buffer
        memcpy(temp_cipher+length,encrypted,len);
        length+=len;
    }
    uint8_t *tag=get_tag(vp);
    uint8_t* cipher=(uint8_t*)malloc(length+vpc->tag_length);//arguement
    memcpy(cipher,temp_cipher,length);
    memcpy(cipher+length,tag,vpc->tag_length);
    free(temp_cipher);
    free(tag);
    *encrypted_length=length+vpc->tag_length;
    return cipher;
}


uint8_t*  decrypt_data_with_data(vpccm_crypt *vpc,uint8_t *data,int data_length,long *decrypted_length)
{
    int length=0;
    if(data_length<=vpc->tag_length)
    {
        fprintf(stderr,"Cipher text is too short Errornum:2\n");
        return 0;

    }
    vpc->bytes_left=data_length-vpc->tag_length;

    vpccm *vp=vpccm_init_with_key(vpc->key,vpc->key_size,vpc->iv,vpc->iv_size,vpc->adata,vpc->adata_size,vpc->tag_length,data_length);
    if(verify_tag_with_data(vp,data,data_length))
    {
        /*UGLY HACK SEE IF I'VE DONE SOMETHING VERY STUPID*/
        vp->file_size=data_length-vpc->tag_length;
        vp->bytes_left=data_length-vpc->tag_length;
        init_vpccm(vp);
        //EVEN UGLIER HACK
        //vp=vpccm_init_with_key(vpc->key,vpc->key_size,vpc->iv,vpc->iv_size,vpc->adata,vpc->adata_size,vpc->tag_length,data_length-vpc->tag_length);
        int loop_count=ceil((data_length-vpc->tag_length)/(float)vpc->buffer_size);
        uint8_t *plain=(uint8_t*)malloc(loop_count*vpc->buffer_size);

        uint8_t *bytes=(uint8_t*)data;

        int len=0;
        int en=-1;
        for( int i=0; i<loop_count; i++)
        {
            uint8_t *decrypted=NULL;
            if(i!=loop_count-1)
            {
                decrypted=decrypt_block(vp,bytes+i*vpc->buffer_size,vpc->buffer_size,&en);
                len=vpc->buffer_size;
            }
            else
            {

                len=(data_length-vpc->tag_length)%vpc->buffer_size;
                decrypted=decrypt_block(vp,bytes+i*vpc->buffer_size,len,&en);
            }


            memcpy(plain+length,decrypted,len);
            length+=len;

        }
        *decrypted_length=length;
        uint8_t *plaintext=(uint8_t*)malloc(length);
        memcpy(plaintext,plain,length);
        free(plain);
        free_vpccm(vp);
        return plaintext;
    }
    else
    {
        printf("Failed to verify flag\n");
        fprintf(stderr,"Invalid TAG errorNumber:1\n");
        free_vpccm(vp);
        return NULL;
    }


}

/*
int main(int argc,char *argv[])
{



  uint8_t *key="dakey", *iv="daiv", *adata="deadatabloom";
  printf("DATA ENCRYPTION TEST:\n");
  uint8_t *data="The quick brown fox jumps over the lazy dog";
  int taglength=16;
  long long file_size=512;
  vpccm_crypt *vpc=vpccm_crypt_init_with_key(key,strlen(key),iv,strlen(iv),adata,strlen(adata),taglength);
  long encrypted_size,decrypted_size;
  uint8_t* encrypted_data=encrypt_data_with_data(vpc,data,strlen(data),&encrypted_size);
  uint8_t* decrypted_data=decrypt_data_with_data(vpc,encrypted_data,encrypted_size,&decrypted_size);
  printf("****Decrypted Data:\"");
  for(int i=0;i<decrypted_size;i++)
    {
      printf("%c",decrypted_data[i]);
    }
  printf("\"****\n");

  printf("\nFILE ENCRYPTION TEST:\n");
  vpccm_crypt *nvpc=vpccm_crypt_init_with_key(key,strlen(key),iv,strlen(iv),adata,strlen(adata),taglength);
  encrypt_file_to_file_with_sourceURL(nvpc,"testfile","cryptedfile");
  printf("\nFILE DECRYPTION TEST\n");
  decrypt_file_to_file_with_sourceURL(nvpc,"cryptedfile","decryptedfile");
  free(vpc);
  free(nvpc);

  return 0;
}
*/
