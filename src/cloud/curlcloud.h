#include <curl/curl.h>
#include <wchar.h>
#include <locale.h>
#include "../dynarray/dyn_array.h"

#ifndef __CURLCLOUD__
#define __CURLCLOUD__

#define PATH_MAXIMUM 4096




typedef
struct session_info
{
    char *user_email;
    char *password;
    char *session_id;
    char *user_key;
    char *url;
} session_info;

typedef struct filenames
{
    char *cloud_path;
    char *filename;
    char *convertedname;
} filenames;



//https://demo.whitecloud.mobi/uploadsmobile

int cloud_session_valid(session_info *session);
session_info* cloud_login(CURL *curl,char *user_email,char *password,int version);//DONE??

int cloud_ping(CURL *curl,char *session_id);//CHECK FOR PONG JUST FOR LAUGHS

int cloud_upload_file(CURL *curl,char *cloud_dir,char *filename,char *session_id, char *iv, char *adata,char *ctype,char *user_key,int flags);

int cloud_upload_file_named(CURL *curl,char *cloud_dir,char *filename,char *convertedname,char *session_id, char *iv, char *adata,char *ctype,char *user_key,int flags);

int cloud_mkdir(CURL *curl,char *dirname,char *parent,char *session_id);

int cloud_mkpath(CURL *curl,char *path,char *session_id);
int cloud_logout(CURL *curl,char *session_id);
int cloud_browse(CURL *curl,char *session_id);
int cloud_get_file(CURL *curl,char *session_id,char *file_path,char *user_key);
int cloud_upload_folder(CURL *curl,char *cloud_path,char *dir_name,char *session_id,char *user_key);
int cloud_upload_unicode_folder(CURL *curl,wchar_t *cloud_path,wchar_t *dir_name,char *session_id,char *user_key);
void cloud_unicode_folder_to_filenames(dyn_array *array,CURL *curl,wchar_t *cloud_path,wchar_t*dir_name,char *session_id,char *user_key);
#endif // __CURLCLOUD__

