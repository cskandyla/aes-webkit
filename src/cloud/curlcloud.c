#include <curl/curl.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <limits.h>
#include <errno.h>
#include <sys/types.h>
#include "../jsmn/jsmn.h"
#include "../crypt/base64.h"
#include "../crypt/ccmcrypt.h"
#include "curlcloud.h"
#include <iconv.h>
#ifdef _WIN32
#include <io.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/stat.h>
#elif defined __linux__
#endif
#define UNICODE 1
#define _UNICODE 1

//Server WTF?
char* wchar_to_utf8(wchar_t *in)
{
    printf("in:%ls\n",in);
    char *out=(char*)malloc(wcslen(in)*6);

    int sizeRequired = WideCharToMultiByte( CP_UTF8, 0, in, -1,
                                            out, 6*wcslen(in),  NULL, NULL);
    printf( "Bytes required for UTF8 encoding (excluding NUL terminator): %u\n",
            sizeRequired-1);
    printf("outlen:%d %d\n ",strlen(out),sizeRequired);
    return out;
}


//char *
char *strndup (const char *s, size_t n)
{
    char *result;
    size_t len = strlen (s);

    if (n < len)
        len = n;

    result = (char *) malloc (len + 1);
    if (!result)
        return 0;

    result[len] = '\0';
    return (char *) memcpy (result, s, len);
}




//Utility matching function for json strings
int jsoneq(const char *json, jsmntok_t *tok, const char *s)
{
    if (tok->type == JSMN_STRING && (int) strlen(s) == tok->end - tok->start &&
            strncmp(json + tok->start, s, tok->end - tok->start) == 0)
    {
        return 0;
    }
    return -1;
}





//UGLY HACK TO STORE POST RESPONSES TO A CHAR*
size_t write_to_string(void *ptr, size_t size, size_t count, void *stream)
{
    //printf("strlen:%d , size*count: %d\n",strlen(ptr),size*count);
    //printf("%s\n",ptr);
    //*(char**)stream=(char*)malloc(size*count+1);
    //memcpy(*(char**)stream,ptr,size*count);
    strcat((char*)stream,(const char*)ptr);

    //chara[size*count]='\0';
    //*(char**)stream=strdup(ptr);

    return size*count;
}

session_info*  cloud_login(CURL *curl,char *user_email,char *password,int version)
{

    CURLcode res;
    session_info *current_session=(session_info*)malloc(sizeof(session_info));
    current_session->user_email=NULL;
    current_session->user_key=NULL;
    current_session->session_id=NULL;
    current_session->password=(char*)malloc(strlen(password));
     strcpy(current_session->password,password);

    printf("Current Session:\n");
    printf("password length:%d\n",strlen(password));

    if(curl)
    {
        char *response=(char*)malloc(4096*sizeof(char));
        memset(response,0,4096);
        char postinfo[1024];
        sprintf(postinfo,"status=login&device=Bloom&username=%s&password=%s&app_version=%d",user_email,password,version);
        curl_easy_setopt(curl, CURLOPT_URL, "https://pilvilinna.elisa.fi/cloudia_api/core/login");
        //curl_easy_setopt(curl, CURLOPT_URL, "https://dev.whitecloud.mobi/cloudia_api/core/login");
        //curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postinfo);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, response);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
        //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
        res = curl_easy_perform(curl);
        //strcat(response,"\0");
        printf("%s",response);

        /* Check for errors */
        if(res != CURLE_OK)
            fprintf(stderr, "curl_easy_perform() failed: %s\n",
                    curl_easy_strerror(res));
        else
        {
            if(response!=NULL)
            {

                jsmn_parser p;
                jsmntok_t t[128];
                jsmn_init(&p);
                int r=jsmn_parse(&p,response,strlen(response),t,sizeof(t)/sizeof(t[0]));
                printf("SESSION:\n");
                for(int i=0; i<r; i++)
                {
                    if (jsoneq(response, &t[i], "username") == 0)
                    {
                        printf("username: %.*s\n", t[i+1].end-t[i+1].start,response + t[i+1].start);
                        current_session->user_email=strndup(response+t[i+1].start,t[i+1].end-t[i+1].start);
                        i++;
                    }
                    else if (jsoneq(response, &t[i], "session_id") == 0)
                    {
                        printf("sessionid: %.*s\n", t[i+1].end-t[i+1].start,response + t[i+1].start);
                        current_session->session_id=strndup(response+t[i+1].start,t[i+1].end-t[i+1].start);
                        i++;
                    }
                    else if (jsoneq(response, &t[i], "key") == 0)
                    {
                        printf("key: %.*s\n", t[i+1].end-t[i+1].start,response + t[i+1].start);
                        current_session->user_key=strndup(response+t[i+1].start,t[i+1].end-t[i+1].start);
                        i++;
                    }

                }
                free(response);
            }

        }

    }
    if(current_session->user_email==NULL || current_session->user_key==NULL || current_session->session_id==NULL)
    {
        free(current_session->password);
        free(current_session);
        return NULL;
    }
    return current_session;
}

int cloud_session_valid(session_info *session)
{
    if(session!= NULL && session->user_email!=NULL && session->password!=NULL && session->session_id!=NULL && session->user_key!=NULL )
        return 0;
    return -1;
}

int cloud_ping(CURL *curl,char *session_id)
{
    CURLcode res;

    if(curl)
    {
        char response[4096];
        char postinfo[1024];
        sprintf(postinfo,"session_id=%s",session_id);
        curl_easy_setopt(curl, CURLOPT_URL, "https://pilvilinna.elisa.fi/login/cloudia_api/core/ping");
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postinfo);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
        //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
        res = curl_easy_perform(curl);


        /* Check for errors */
        if(res != CURLE_OK)
        {
            fprintf(stderr, "curl_easy_perform() failed: %s\n",
                    curl_easy_strerror(res));



            return -1;
        }
        else
        {
            printf("%s\n",response);

            return 0;
        }



    }
}






int cloud_upload_file(CURL *curl,char *cloud_dir,char *filename,char *session_id, char *iv, char *adata,char *ctype,char *user_key,int flags)
{


    //SETUP POST
    struct curl_httppost *formpost=NULL;
    struct curl_httppost *lastptr=NULL;
    struct curl_slist *headerlist=NULL;

    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "iv",
                 CURLFORM_COPYCONTENTS, iv,
                 CURLFORM_END);

    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "adata",
                 CURLFORM_COPYCONTENTS, adata,
                 CURLFORM_END);


    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "file",
                 CURLFORM_FILE,filename ,
                 CURLFORM_FILENAME,filename,
                 CURLFORM_END);
    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "session_id",
                 CURLFORM_COPYCONTENTS, session_id,
                 CURLFORM_END);


    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "dir",
                 CURLFORM_COPYCONTENTS,cloud_dir ,
                 CURLFORM_END);


    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "ctype",
                 CURLFORM_COPYCONTENTS, ctype,
                 CURLFORM_END);

    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "key",
                 CURLFORM_COPYCONTENTS, user_key,
                 CURLFORM_END);

    char flags_s[2];
    sprintf(flags_s,"%d\0",flags);
    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "flags",
                 CURLFORM_COPYCONTENTS, flags_s,
                 CURLFORM_END);



    headerlist = curl_slist_append(headerlist, "Content-Disposition: form-data; name=\"files[]\"; filename=\"C:dafile.txt\"");


    CURLcode res;
    if(curl)
    {
        curl_easy_setopt(curl, CURLOPT_URL, "https://pilvilinna.elisa.fi/uploadsmobile");
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
        curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);
        char response[4096];
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);


        res = curl_easy_perform(curl);
        /* Check for errors */
        if(res != CURLE_OK)
        {
            fprintf(stderr, "curl_easy_perform() failed: %s\n",
                    curl_easy_strerror(res));
            return 1;
        }

        else
        {
            printf("%s\n",response);
            return 0;
        }

    }
}

int cloud_mkdir(CURL *curl,char *dirname,char *parent,char *session_id)
{
    CURLcode res;
    if(curl)
    {
        char postinfo[512];
        sprintf(postinfo,"session_id=%s&dir=%s&dir=%s",session_id,parent,dirname);
        curl_easy_setopt(curl, CURLOPT_URL, "https://pilvilinna.elisa.fi/cloudia_api/core/mkdir");
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postinfo);
        char response[4096];
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);
        res = curl_easy_perform(curl);
        /* Check for errors */
        if(res != CURLE_OK)
            fprintf(stderr, "curl_easy_perform() failed: %s\n",
                    curl_easy_strerror(res));
        else
        {
            printf("%s\n",response);
        }


    }


}
int cloud_mkpath(CURL *curl,char *path,char *session_id)
{
    //browse?session_id=fdfgdsf

    CURLcode res;
    if(curl)
    {
        char postinfo[4096];
        sprintf(postinfo,"session_id=%s&dir=%s",session_id,path);
        curl_easy_setopt(curl, CURLOPT_URL, "https://pilvilinna.elisa.fi/cloudia_api/core/mkpath");
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postinfo);
        char response[4096];
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);
        res = curl_easy_perform(curl);
        /* Check for errors */
        if(res != CURLE_OK)
            fprintf(stderr, "curl_easy_perform() failed: %s\n",
                    curl_easy_strerror(res));
        else
        {
            printf("%s\n",response);
        }
    }
}



int cloud_browse(CURL *curl,char *session_id)
{

    CURLcode res;
    if(curl)
    {
        char postinfo[512];
        sprintf(postinfo,"session_id=%s",session_id);
        //curl_easy_setopt(curl, CURLOPT_URL, "https://pilvilinna.elisa.fi/cloudia_api/core/browse");
        //curl_easy_setopt(curl, CURLOPT_URL, "https://dev.whitecloud.mobi/cloudia_api/core/browse");
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postinfo);
        char response[4096];
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);
        res = curl_easy_perform(curl);
        /* Check for errors */
        if(res != CURLE_OK)
            fprintf(stderr, "curl_easy_perform() failed: %s\n",
                    curl_easy_strerror(res));
        else
        {
            printf("%s\n",response);
        }
    }


}


int cloud_get_file(CURL *curl,char *session_id,char *file_path,char *user_key)
{
    int tag_length=16;
    CURLcode res;
    if(curl)
    {
        char postinfo[512];
        sprintf(postinfo,"session_id=%s&file=%s",session_id,file_path);
        curl_easy_setopt(curl, CURLOPT_URL, "https://pilvilinna.elisa.fi/cloudia_api/core/get_file");
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postinfo);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        char response[4096];
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);

        res = curl_easy_perform(curl);
        /* Check for errors */
        if(res != CURLE_OK)
            fprintf(stderr, "curl_easy_perform() failed: %s\n",
                    curl_easy_strerror(res));
        else
        {
            //FILE SHOULD BE ENCODED IN THE RESPONSE
            printf("%s\n",response);

            //PARSE JSON TO GET THE IV AND ADATA
            char *iv="MTQ0NjIxODU2NQ==";
            //DECODE IV
            char *dec_iv=(char*)malloc(Base64decode_len(iv));
            Base64decode(dec_iv,iv);
            printf("DECODDED IV:%s\n",dec_iv);
            char *adata="1446218565";
            printf("adata:%s\n",adata);
            //vpccm_crypt *vpc=(vpccm_crypt*)vpccm_crypt_init_with_key((uint8_t*)user_key,strlen(user_key),(uint8_t*)dec_iv,strlen(dec_iv),(uint8_t*)adata,strlen(adata),5);
            long dec_length;
            // char *decrypted=(char*)decrypt_data_with_data(vpc,(uint8_t*)response,strlen(response),&dec_length);
        }

    }


}

int cloud_logout(CURL *curl,char *session_id)
{

    CURLcode res;
    if(curl)
    {
        char postinfo[512];
        sprintf(postinfo,"session_id=%s",session_id);
        curl_easy_setopt(curl, CURLOPT_URL, "https://pilvilinna.elisa.fi/cloudia_api/core/logout");
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postinfo);
        char response[4096];
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);
        res = curl_easy_perform(curl);
        if(res != CURLE_OK)
            fprintf(stderr, "curl_easy_perform() failed: %s\n",
                    curl_easy_strerror(res));
        else
        {
            printf("%s\n",response);
        }
    }

    return 0;
}





int cloud_upload_folder(CURL *curl,char *cloud_path,char *dir_name,char *session_id,char *user_key)
{

    char path[PATH_MAXIMUM];
    //STUPID IMPLEMENTATION
    //for each file encrypt and upload it//

    DIR * d;

    /* Open the directory specified by "dir_name". */

    d = opendir (dir_name);

    /* Check it was opened. */
    if (! d)
    {
        fprintf (stderr, "Cannot open directory '%s': %s\n",
                 dir_name, strerror (errno));
        exit (EXIT_FAILURE);
    }
    while (1)
    {
        struct dirent * entry;
        const char * d_name;

        /* "Readdir" gets subsequent entries from "d". */
        entry = readdir (d);
        if (! entry)
        {
            /* There are no more entries in this directory, so break
            out of the while loop. */
            break;
        }
        d_name = entry->d_name;

        // Check its not A Folder and Upload
        //Windows might require stat to check if its a folder
        sprintf (path,"%s/%s\0", dir_name, d_name);
        printf("path:%s\n",path);
        struct stat status;
        stat(path,&status);
        printf("%d\n",status.st_mode);
        if( (status.st_mode & S_IFDIR) == 0)
        {
            //Generate iv,pass filesize as adata,**MAYBE try to identify file type **
            if (strcmp (d_name, "..") != 0 &&
                    strcmp (d_name, ".") != 0)
            {

                printf("file:%s\n",path);

                //vpccm_crypt *vpc=vpccm_crypt_init_with_key(user_key,strlen(user_key),iv,strlen(iv),adata,strlen(adata),tag_length);

                //cloud_upload_file(curl,cloud_path,path,session_id,"MY_IV","DEAD_DATA","??",user_key,1);
                printf("CLOUD_PATH:%s\n",cloud_path);
                printf("LOCAL_PATH:%s\n",path);
                char targetpath[PATH_MAXIMUM];
#ifdef _WIN32
                sprintf(targetpath,"%s/%s",cloud_path,dir_name);
#elif defined __linux__
                sprintf(targetpath,"%s%s",cloud_path,dir_name);
#endif // _WIN32

                printf("TARGET PATH:%s\n",targetpath);
                cloud_upload_file(curl,targetpath,path,session_id,"MY_IV","DEAD_DATA","??",user_key,1);
            }
        }


        //Its a folder recursively upload it
        if((status.st_mode & S_IFDIR) != 0)
        {

            /* Check that the directory is not "d" or d's parent. */

            if (strcmp (d_name, "..") != 0 &&
                    strcmp (d_name, ".") != 0)
            {
                int path_length;
                path_length = snprintf (path, PATH_MAXIMUM,
                                        "%s/%s", dir_name, d_name);
                //printf ("%s\n", path);
                if (path_length >= PATH_MAXIMUM)
                {
                    fprintf (stderr, "Path length has got too long.\n");
                    exit (EXIT_FAILURE);
                }
                /* Recursively upload subfolders */
                char new_cloud_path[4096];
#ifdef _WIN32
                sprintf(new_cloud_path,"%s/%s",cloud_path,path);
#elif defined __linux__
                sprintf(new_cloud_path,"%s%s",cloud_path,path);
#endif // _WIN32


                printf("folder:%s\n",new_cloud_path);
                cloud_mkpath(curl,new_cloud_path,session_id);
                cloud_upload_folder (curl,cloud_path,path,session_id,user_key);
            }
        }
    }
    /* After going through all the entries, close the directory. */
    if (closedir (d))
    {
        fprintf (stderr, "Could not close '%s': %s\n",
                 dir_name, strerror (errno));
        exit (EXIT_FAILURE);
    }


}

int cloud_folder_sync(CURL *curl,char *folder_name)
{
    //WHAT EXACTLY?
    //GET DATES AND IF NEWER UPLOAD?

}


int cloud_upload_unicode_folder(CURL *curl,wchar_t *cloud_path,wchar_t *dir_name,char *session_id,char *user_key)
{

    printf("Unicode Directory:%ls\n",dir_name);
    wchar_t path[PATH_MAXIMUM];
    //STUPID IMPLEMENTATION
    //for each file encrypt and upload it//

    _WDIR * d;

    /* Open the directory specified by "dir_name". */

    d = _wopendir (dir_name);

    /* Check it was opened. */
    if (! d)
    {
        fprintf (stderr, "Cannot open directory '%ls': %ls\n",
                 dir_name, strerror (errno));
        exit (EXIT_FAILURE);
    }
    while (1)
    {
        struct _wdirent * entry;
        const wchar_t * d_name;

        /* "Readdir" gets subsequent entries from "d". */
        entry = _wreaddir (d);
        if (! entry)
        {
            /* There are no more entries in this directory, so break
            out of the while loop. */
            break;
        }
        d_name = entry->d_name;

        // Check its not A Folder and Upload
        //Windows might require stat to check if its a folder
        swprintf (path,L"%ls/%ls\0", dir_name, d_name);
        printf("path:%ls\n",path);
        struct _stat status;
        _wstat(path,&status);
        printf("%d\n",status.st_mode);
        if( (status.st_mode & _S_IFDIR) == 0)
        {
            //Generate iv,pass filesize as adata,**MAYBE try to identify file type **
            if (wcscmp (d_name, L"..") != 0 &&
                    wcscmp (d_name, L".") != 0)
            {

                printf("file:%ls\n",path);

                //vpccm_crypt *vpc=vpccm_crypt_init_with_key(user_key,strlen(user_key),iv,strlen(iv),adata,strlen(adata),tag_length);

                //cloud_upload_file(curl,cloud_path,path,session_id,"MY_IV","DEAD_DATA","??",user_key,1);
                printf("CLOUD_PATH:%ls\n",cloud_path);
                printf("LOCAL_PATH:%ls\n",path);
                wchar_t targetpath[PATH_MAXIMUM];

                swprintf(targetpath,L"%ls/%ls",cloud_path,dir_name);
                printf("TARGET PATH:%ls\n",targetpath);
                char mb_targetpath[4096],mb_path[4096];
                size_t size=wcstombs(mb_targetpath,targetpath,4096);
                size=wcstombs(mb_path,path,4096);



                printf("mutlibyte mb_targetpath:%s mb_path:%s\n",wchar_to_utf8(targetpath),wchar_to_utf8(path));

                //ADD TO A LIST INSTEAD OF UPLOADING [Code a quick and dirty dyn_array] and a names struct
                cloud_upload_file_named(curl,wchar_to_utf8(targetpath),mb_path,wchar_to_utf8(path),session_id,"MY_IV","DEAD_DATA","??",user_key,1);
            }
        }


        //Its a folder recursively upload it
        if((status.st_mode & _S_IFDIR) != 0)
        {

            /* Check that the directory is not "d" or d's parent. */

            if (wcscmp (d_name, L"..") != 0 &&
                    wcscmp (d_name, L".") != 0)
            {
                int path_length;
                path_length = swprintf (path,
                                        L"%ls/%ls", dir_name, d_name);
                printf ("%ls\n", path);
                if (path_length >= PATH_MAXIMUM)
                {
                    fprintf (stderr, "Path length has got too long.\n");
                    exit (EXIT_FAILURE);
                }
                /* Recursively upload subfolders */
                wchar_t new_cloud_path[4096];

                swprintf(new_cloud_path,L"%ls/%ls",cloud_path,path);



                printf("folder:%ls\n",new_cloud_path);
                char mb_new_cloud_path[4096];
                wcstombs(mb_new_cloud_path,new_cloud_path,4096);
                //create
                cloud_mkpath(curl,wchar_to_utf8(new_cloud_path),session_id);

                printf("recursing:%ls\n",path);
                cloud_upload_unicode_folder (curl,cloud_path,path,session_id,user_key);
            }
        }
    }
    /* After going through all the entries, close the directory. */
    if (_wclosedir (d))
    {
        fprintf (stderr, "Could not close '%s': %s\n",
                 dir_name, strerror (errno));
        exit (EXIT_FAILURE);
    }


}



int cloud_upload_unicode_folder_array(CURL *curl,wchar_t *cloud_path,wchar_t *dir_name,char *session_id,char *user_key)
{
    //dyn_array *filez=malloc(sizeof(dyn_array));

    printf("Unicode Directory:%ls\n",dir_name);
    wchar_t path[PATH_MAXIMUM];
    //STUPID IMPLEMENTATION
    //for each file encrypt and upload it//

    _WDIR * d;

    /* Open the directory specified by "dir_name". */

    d = _wopendir (dir_name);

    /* Check it was opened. */
    if (! d)
    {
        fprintf (stderr, "Cannot open directory '%ls': %ls\n",
                 dir_name, strerror (errno));
        exit (EXIT_FAILURE);
    }
    while (1)
    {
        struct _wdirent * entry;
        const wchar_t * d_name;

        /* "Readdir" gets subsequent entries from "d". */
        entry = _wreaddir (d);
        if (! entry)
        {
            /* There are no more entries in this directory, so break
            out of the while loop. */
            break;
        }
        d_name = entry->d_name;

        // Check its not A Folder and Upload
        //Windows might require stat to check if its a folder
        swprintf (path,L"%ls/%ls\0", dir_name, d_name);
        printf("path:%ls\n",path);
        struct _stat status;
        _wstat(path,&status);
        printf("%d\n",status.st_mode);
        if( (status.st_mode & _S_IFDIR) == 0)
        {
            //Generate iv,pass filesize as adata,**MAYBE try to identify file type **
            if (wcscmp (d_name, L"..") != 0 &&
                    wcscmp (d_name, L".") != 0)
            {

                printf("file:%ls\n",path);

                //vpccm_crypt *vpc=vpccm_crypt_init_with_key(user_key,strlen(user_key),iv,strlen(iv),adata,strlen(adata),tag_length);

                //cloud_upload_file(curl,cloud_path,path,session_id,"MY_IV","DEAD_DATA","??",user_key,1);
                printf("CLOUD_PATH:%ls\n",cloud_path);
                printf("LOCAL_PATH:%ls\n",path);
                wchar_t targetpath[PATH_MAXIMUM];

                swprintf(targetpath,L"%ls/%ls",cloud_path,dir_name);
                printf("TARGET PATH:%ls\n",targetpath);
                char mb_targetpath[4096],mb_path[4096];
                size_t size=wcstombs(mb_targetpath,targetpath,4096);
                size=wcstombs(mb_path,path,4096);



                printf("mutlibyte mb_targetpath:%s mb_path:%s\n",wchar_to_utf8(targetpath),wchar_to_utf8(path));

                //ADD TO A LIST INSTEAD OF UPLOADING [Code a quick and dirty dyn_array] and a names struct
                filenames *fnames=(filenames*)malloc(sizeof(filenames));
                fnames->cloud_path=wchar_to_utf8(targetpath);
                fnames->convertedname=wchar_to_utf8(path);
                fnames->filename=mb_path;
                //dyn_array_add(filenames,f);
                //cloud_upload_file_named(curl,wchar_to_utf8(targetpath),mb_path,wchar_to_utf8(path),session_id,"MY_IV","DEAD_DATA","??",user_key,1);
            }
        }


        //Its a folder recursively upload it
        if((status.st_mode & _S_IFDIR) != 0)
        {

            /* Check that the directory is not "d" or d's parent. */

            if (wcscmp (d_name, L"..") != 0 &&
                    wcscmp (d_name, L".") != 0)
            {
                int path_length;
                path_length = swprintf (path,
                                        L"%ls/%ls", dir_name, d_name);
                printf ("%ls\n", path);
                if (path_length >= PATH_MAXIMUM)
                {
                    fprintf (stderr, "Path length has got too long.\n");
                    exit (EXIT_FAILURE);
                }
                /* Recursively upload subfolders */
                wchar_t new_cloud_path[4096];

                swprintf(new_cloud_path,L"%ls/%ls",cloud_path,path);



                printf("folder:%ls\n",new_cloud_path);
                char mb_new_cloud_path[4096];
                wcstombs(mb_new_cloud_path,new_cloud_path,4096);
                //create
                cloud_mkpath(curl,wchar_to_utf8(new_cloud_path),session_id);

                printf("recursing:%ls\n",path);
                cloud_upload_unicode_folder (curl,cloud_path,path,session_id,user_key);
            }
        }
    }
    /* After going through all the entries, close the directory. */
    if (_wclosedir (d))
    {
        fprintf (stderr, "Could not close '%s': %s\n",
                 dir_name, strerror (errno));
        exit (EXIT_FAILURE);
    }


}





int cloud_upload_file_named(CURL *curl,char *cloud_dir,char *filename,char *convertedname,char *session_id, char *iv, char *adata,char *ctype,char *user_key,int flags)
{
    //SETUP POST
    struct curl_httppost *formpost=NULL;
    struct curl_httppost *lastptr=NULL;
    struct curl_slist *headerlist=NULL;

    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "iv",
                 CURLFORM_COPYCONTENTS, iv,
                 CURLFORM_END);

    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "adata",
                 CURLFORM_COPYCONTENTS, adata,
                 CURLFORM_END);


    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "file",
                 CURLFORM_FILE,filename ,
                 CURLFORM_FILENAME,convertedname,
                 CURLFORM_END);
    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "session_id",
                 CURLFORM_COPYCONTENTS, session_id,
                 CURLFORM_END);


    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "dir",
                 CURLFORM_COPYCONTENTS,cloud_dir ,
                 CURLFORM_END);


    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "ctype",
                 CURLFORM_COPYCONTENTS, ctype,
                 CURLFORM_END);

    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "key",
                 CURLFORM_COPYCONTENTS, user_key,
                 CURLFORM_END);

    char flags_s[2];
    sprintf(flags_s,"%d\0",flags);
    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "flags",
                 CURLFORM_COPYCONTENTS, flags_s,
                 CURLFORM_END);



    headerlist = curl_slist_append(headerlist, "Content-Disposition: form-data; name=\"files[]\"; filename=\"C:dafile.txt\"");


    CURLcode res;
    if(curl)
    {
        curl_easy_setopt(curl, CURLOPT_URL, "https://pilvilinna.elisa.fi/uploadsmobile");
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
        curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);
        char response[4096];
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);
        res = curl_easy_perform(curl);
        /* Check for errors */
        if(res != CURLE_OK)
            fprintf(stderr, "curl_easy_perform() failed: %s\n",
                    curl_easy_strerror(res));
        else
        {
            printf("%s\n",response);
        }

    }
}


void cloud_unicode_folder_to_filenames(dyn_array *array,CURL *curl,wchar_t *cloud_path,wchar_t*dir_name,char *session_id,char *user_key)
{
    //dyn_array *filez=malloc(sizeof(dyn_array));

    printf("Unicode Directory:%ls\n",dir_name);
    wchar_t path[PATH_MAXIMUM];
    //STUPID IMPLEMENTATION
    //for each file encrypt and upload it//

    _WDIR * d;

    /* Open the directory specified by "dir_name". */

    d = _wopendir (dir_name);

    /* Check it was opened. */
    if (! d)
    {
        fprintf (stderr, "Cannot open directory '%ls': %ls\n",
                 dir_name, strerror (errno));
        exit (EXIT_FAILURE);
    }
    while (1)
    {
        struct _wdirent * entry;
        const wchar_t * d_name;

        /* "Readdir" gets subsequent entries from "d". */
        entry = _wreaddir (d);
        if (! entry)
        {
            /* There are no more entries in this directory, so break
            out of the while loop. */
            break;
        }
        d_name = entry->d_name;

        // Check its not A Folder and Upload
        //Windows might require stat to check if its a folder
        swprintf (path,L"%ls/%ls\0", dir_name, d_name);
        printf("path:%ls\n",path);
        struct _stat status;
        _wstat(path,&status);
        printf("%d\n",status.st_mode);
        if( (status.st_mode & _S_IFDIR) == 0)
        {
            //Generate iv,pass filesize as adata,**MAYBE try to identify file type **
            if (wcscmp (d_name, L"..") != 0 &&
                    wcscmp (d_name, L".") != 0)
            {

                printf("file:%ls\n",path);

                //vpccm_crypt *vpc=vpccm_crypt_init_with_key(user_key,strlen(user_key),iv,strlen(iv),adata,strlen(adata),tag_length);

                //cloud_upload_file(curl,cloud_path,path,session_id,"MY_IV","DEAD_DATA","??",user_key,1);
                printf("CLOUD_PATH:%ls\n",cloud_path);
                printf("LOCAL_PATH:%ls\n",path);
                wchar_t targetpath[PATH_MAXIMUM];

                swprintf(targetpath,L"%ls/%ls",cloud_path,dir_name);
                printf("TARGET PATH:%ls\n",targetpath);
                char mb_targetpath[4096],*mb_path=(char*)malloc(4096);
                size_t size=wcstombs(mb_targetpath,targetpath,4096);
                size=wcstombs(mb_path,path,4096);



                printf("mutlibyte mb_targetpath:%s mb_path:%s\n",wchar_to_utf8(targetpath),wchar_to_utf8(path));

                //ADD TO A LIST INSTEAD OF UPLOADING [Code a quick and dirty dyn_array] and a names struct
                filenames *fnames=(filenames*)malloc(sizeof(filenames));
                fnames->cloud_path=wchar_to_utf8(targetpath);
                fnames->convertedname=wchar_to_utf8(path);
                fnames->filename=mb_path;
                dyn_array_add(array,fnames);

                //dyn_array_add(filenames,f);
                //cloud_upload_file_named(curl,wchar_to_utf8(targetpath),mb_path,wchar_to_utf8(path),session_id,"MY_IV","DEAD_DATA","??",user_key,1);
            }
        }


        //Its a folder recursively upload it
        if((status.st_mode & _S_IFDIR) != 0)
        {

            /* Check that the directory is not "d" or d's parent. */

            if (wcscmp (d_name, L"..") != 0 &&
                    wcscmp (d_name, L".") != 0)
            {
                int path_length;
                path_length = swprintf (path,
                                        L"%ls/%ls", dir_name, d_name);
                printf ("%ls\n", path);
                if (path_length >= PATH_MAXIMUM)
                {
                    fprintf (stderr, "Path length has got too long.\n");
                    exit (EXIT_FAILURE);
                }
                /* Recursively upload subfolders */
                wchar_t new_cloud_path[4096];

                swprintf(new_cloud_path,L"%ls/%ls",cloud_path,path);



                printf("folder:%ls\n",new_cloud_path);
                char mb_new_cloud_path[4096];
                wcstombs(mb_new_cloud_path,new_cloud_path,4096);
                //create
                cloud_mkpath(curl,wchar_to_utf8(new_cloud_path),session_id);

                printf("recursing:%ls\n",path);
                cloud_unicode_folder_to_filenames(array,curl,cloud_path,path,session_id,user_key);
                //cloud_upload_unicode_folder (curl,cloud_path,path,session_id,user_key);
            }
        }
    }
    /* After going through all the entries, close the directory. */
    if (_wclosedir (d))
    {
        fprintf (stderr, "Could not close '%s': %s\n",
                 dir_name, strerror (errno));
        exit (EXIT_FAILURE);
    }


}
