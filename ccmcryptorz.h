#include "ccm.h"
#include <curl/curl.h>
//#include "../tpl/tpl.h"
//#include "aes.h"
//#include <openssl/aes.h>

#define BLOCKSIZE	16
#define IVLENGTH	12
#define CHUNK		16384

typedef struct vpccm_crypt
{
    int tag_length,buffer_size,block_size;
    long long bytes_left;
    uint8_t *key,*iv,*adata;
    int key_size,iv_size,adata_size;


} vpccm_crypt;

typedef struct vpccm_crypt_chunked
{
    int tag_length,buffer_size,block_size,block_count,exit_next,key_size,iv_size,adata_size;
    long bytes_left, file_size, offset, chunk_length;
    uint8_t *key,*iv,*adata,*ctr,*Y,*S0,*ctr_cipher,*cc;
    char *url;
} vpccm_crypt_chunked;



vpccm_crypt* vpccm_crypt_init_with_key(uint8_t *key,int key_size,uint8_t *iv,int iv_size,uint8_t *adata,int adata_size,int tag_length);

uint8_t*  encrypt_stream_with_URL(vpccm_crypt *vpc,char *url,long *encrypted_stream_length);
uint8_t*  decrypt_stream_with_URL(vpccm_crypt *vpc,char *url,long *decrypted_stream_length);

uint8_t*  encrypt_stream_with_file(vpccm_crypt *vpc,char *url,long *encrypted_stream_length);
uint8_t*  decrypt_stream_with_fileL(vpccm_crypt *vpc,char *url,long *decrypted_stream_length);

void encrypt_file_to_file_with_sourceURL(vpccm_crypt *vpc,char *sourceURL,char *destURL);
void decrypt_file_to_file_with_sourceURL(vpccm_crypt *vpc,char *sourceURL,char *destURL);

int save_state(char * state_file, vpccm_crypt_chunked *vpc);
int load_state(char * state_file, vpccm_crypt_chunked *vpc);

void encrypt_file_to_file(vpccm_crypt *vpc,char *source,char *dest);
void decrypt_file_to_file(vpccm_crypt *vpc,char *source,char *dest);


uint8_t*  encrypt_data_with_data(vpccm_crypt *vpc,uint8_t  *data,int data_length,long *encrypted_length);
uint8_t*  decrypt_data_with_data(vpccm_crypt *vpc,uint8_t *data,int data_length,long *decrypted_length);

int ccm_upload_chunked(CURL *curl,vpccm_crypt *vpc,char *file_name,char *state_file,char *cloud_dir,char *session_id,char *ctype);

int myceil (int one, int two);


