;NSIS Ultra Modern User Interface
;Background Image Example Script
;Written by SuperPat

;--------------------------------
;General

  ;Name and file
  Name "Elisa Pilvilinna Plus"
  OutFile "ElisaPilvilinnaPlusInstaller.exe"

  ;Default installation folder
  InstallDir "$PROGRAMFILES\Elisa Pilvilinna Plus"
  
  ;Get installation folder from registry if available
  InstallDirRegKey HKCU "Software\Elisa Pilvilinna Plus" ""

  ;Request application privileges for Windows Vista
  RequestExecutionLevel user


;--------------------------------
;Include UltraModernUI

  !include "UMUI.nsh"


;--------------------------------
;--------------------------------
;Variables

  Var STARTMENU_FOLDER

;--------------------------------
;Interface Settings
!insertmacro MUI_DEFAULT UMUI_TEXT_COLOR 000000
!insertmacro MUI_DEFAULT UMUI_LINK_COLOR 000000
BrandingText " "
; A unique background image

!define UMUI_UNIQUEBGIMAGE
!define UMUI_UNUNIQUEBGIMAGE

!define UMUI_PAGEBGIMAGE
!define UMUI_UNPAGEBGIMAGE
!define UMUI_PAGEBGIMAGE_BMP "installer800x560.bmp"

	

	!define UMUI_USE_INSTALLOPTIONSEX

  !define UMUI_PARAMS_REGISTRY_ROOT HKCU
  !define UMUI_PARAMS_REGISTRY_KEY "Software\ElisaPilvilinna"
  
  !define UMUI_INSTALLDIR_REGISTRY_VALUENAME "InstallDir"		;Replace the InstallDirRegKey instruction and automatically save the $INSTDIR variable

  !define UMUI_SHELLVARCONTEXT_REGISTRY_VALUENAME "UMUI ShellVarContext" 
  !define UMUI_DEFAULT_SHELLVARCONTEXT all
	!define MUI_ABORTWARNING
	!define MUI_UNABORTWARNING

	!define UMUI_USE_ALTERNATE_PAGE
	!define UMUI_USE_UNALTERNATE_PAGE
	
	!define MUI_LICENSEPAGE_RADIOBUTTONS
;--------------------------------
;Pages

  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE "License.txt"
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
  
  
  
  		!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"
		!define MUI_STARTMENUPAGE_DEFAULTFOLDER "Elisa Pilvlinna"
		!define UMUI_ALTERNATIVESTARTMENUPAGE_SETSHELLVARCONTEXT
		!define UMUI_ALTERNATIVESTARTMENUPAGE_USE_TREEVIEW
  !insertmacro UMUI_PAGE_ALTERNATIVESTARTMENU Application $STARTMENU_FOLDER

!insertmacro UMUI_PAGE_CONFIRM

  !insertmacro MUI_PAGE_INSTFILES
  
  
;	!define MUI_FINISHPAGE_RUN "blabla.exe"
;	!define MUI_FINISHPAGE_RUN_TEXT "run blabla"
	
	
  !insertmacro MUI_PAGE_FINISH

  !insertmacro UMUI_PAGE_ABORT


  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  
  !insertmacro MUI_UNPAGE_FINISH


  !insertmacro UMUI_UNPAGE_ABORT
  
;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "English"
  !insertmacro MUI_LANGUAGE "Finnish"

;--------------------------------
;Installer Sections

Section "Elisa Pilvilinna Plus" Pilvi

  SetOutPath "$INSTDIR"
  
  ;ADD YOUR OWN FILES HERE...
  ;DLLS
  file "d3dcompiler_43.dll"
  file "d3dcompiler_46.dll"
  file "ffmpegsumo.dll"
  file "icudt.dll"
  file "libcef.dll"
  file "libcharset1.dll"
  file "libcurl.dll"
  file "libeay32.dll"
  file "libEGL.dll"
  file "libGLESv2.dll"
  file "libiconv2.dll"
  file "libiconv-2.dll"
  file "libidn-11.dll"
  file "libintl3.dll"
  file "librtmp.dll"
  file "libssh2.dll"
  file "ssleay32.dll"
  file "wxmsw30u_gcc_cb.dll"
  file "wxmsw30u_gl_gcc_cb.dll"
  file "zlib1.dll"
  
  ;GCC DLLS
  file "libgcc_s_dw2-1.dll"
  file "libstdc++-6.dll"
  
  ;pngs
  file "folder32.png"
  file "arrow_right-fa.png"
  file "arrow_right-fa-256.png"
  file "arrow_right-fa-128.png"
  file "arrow_right-fa-64.png"
  file "arrow_right-fa-32.png"
  file "arrow_right-fa-64-orange.png"
  file "arrow_right-fa-64-blue.png"
  file "arrow_right-fa-32-blue.png"
  file "Pilvilinna.png"
  file "Pilvilinna_Small.png"
  ;
  file "wxCloudService.exe"
  ;paks
  file "cef.pak"
  file "devtools_resources.pak"
  ;folders
  file /r "locales"
  ;lib
  file "libcef.lib"
  
  
  
  
  file "HowTo.htm"
  Rename "$INSTDIR\wxCloudService.exe" $INSTDIR\"ElisaPilvilinna.exe"
  
  ;Store installation folder
  WriteRegStr HKCU "Software\Elisa Pilvilinna Plus" "" $INSTDIR
  
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"
  
  ;!insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    
    ;Create shortcuts
    CreateDirectory "$SMPROGRAMS\$STARTMENU_FOLDER"
    CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Uninstall.lnk" "$INSTDIR\Uninstall.exe"
	CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\ElisaPilvilinna.lnk" "$INSTDIR\ElisaPilvilinna.exe"
	
  
  ;!insertmacro MUI_STARTMENU_WRITE_END

SectionEnd

;--------------------------------
;Descriptions

  ;Language strings
  LangString DESC_Pilvi ${LANG_ENGLISH} "Elisa Pilvilinna Plus Desktop Application For Windows"

  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${Pilvi} $(DESC_Pilvi)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END
 
;--------------------------------
;Uninstaller Section

Section "Uninstall"

  ;ADD YOUR OWN FILES HERE...

  Delete "$INSTDIR\Uninstall.exe"
  Delete "$INSTDIR\d3dcompiler_43.dll"
  Delete "$INSTDIR\d3dcompiler_46.dll"
  Delete "$INSTDIR\ffmpegsumo.dll"
  Delete "$INSTDIR\icudt.dll"
  Delete "$INSTDIR\libcef.dll"
  Delete "$INSTDIR\libcharset1.dll"
  Delete "$INSTDIR\libcurl.dll"
  Delete "$INSTDIR\libeay32.dll"
  Delete "$INSTDIR\libEGL.dll"
  Delete "$INSTDIR\libGLESv2.dll"
  Delete "$INSTDIR\libiconv2.dll"
  Delete "$INSTDIR\libiconv-2.dll"
  Delete "$INSTDIR\libidn-11.dll"
  Delete "$INSTDIR\libintl3.dll"
  Delete "$INSTDIR\librtmp.dll"
  Delete "$INSTDIR\libssh2.dll"
  Delete "$INSTDIR\ssleay32.dll"
  Delete "$INSTDIR\wxmsw30u_gcc_cb.dll"
  Delete "$INSTDIR\wxmsw30u_gl_gcc_cb.dll"
  Delete "$INSTDIR\zlib1.dll"
  
  ;GCC DLLS
  Delete "$INSTDIR\libgcc_s_dw2-1.dll"
  Delete "$INSTDIR\libstdc++-6.dll"
  
  ;pngs
  Delete "$INSTDIR\folder32.png"
  Delete "$INSTDIR\arrow_right-fa.png"
  Delete "$INSTDIR\arrow_right-fa-256.png"
  Delete "$INSTDIR\arrow_right-fa-128.png"
  Delete "$INSTDIR\arrow_right-fa-64.png"
  Delete "$INSTDIR\arrow_right-fa-32.png"
  Delete "$INSTDIR\arrow_right-fa-64-orange.png"
  Delete "$INSTDIR\arrow_right-fa-64-blue.png"
  Delete "$INSTDIR\arrow_right-fa-32-blue.png"
  Delete "$INSTDIR\Pilvilinna.png"
  Delete "$INSTDIR\Pilvilinna_Small.png"
  ;
  Delete "$INSTDIR\wxCloudService.exe"
  ;paks
  Delete "$INSTDIR\cef.pak"
  Delete "$INSTDIR\devtools_resources.pak"
  ;folders
  RMDir /r "$INSTDIR\locales"
  ;lib
  Delete "$INSTDIR\libcef.lib"
  
  ;ugly hack for now  
  Delete "$INSTDIR\HowTo.htm"
  Delete "$INSTDIR\ElisaPilvilinna.exe"
 
  RMDir "$INSTDIR"
  
  Delete "$SMPROGRAMS\$STARTMENU_FOLDER\Uninstall.lnk" 
  Delete "$SMPROGRAMS\$STARTMENU_FOLDER\ElisaPilvilinna.lnk" 
  RMDir "$SMPROGRAMS\$STARTMENU_FOLDER"

  DeleteRegKey /ifempty HKCU "Software\Elisa Pilvilinna Plus"

SectionEnd  