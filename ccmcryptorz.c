#include "ccmcrypt.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <openssl/aes.h>
#include <curl/curl.h>

int myceil (int one, int two) {
  if ( (one/(float)two) > (float)(one/two) ) {
    return (one/two)+1;
  }

  return (one/two);
}

vpccm_crypt* vpccm_crypt_init_with_key(uint8_t *key,int key_size,uint8_t *iv,int iv_size,uint8_t *adata,int adata_size,int tag_length)
{

  vpccm_crypt *vpc=(vpccm_crypt*)malloc(sizeof(vpccm_crypt));
  vpc->key=key;
  vpc->key_size=key_size;
  vpc->iv=iv;
  vpc->iv_size=iv_size;
  vpc->adata=adata;
  vpc->adata_size=adata_size;
  vpc->tag_length=tag_length;
  vpc->block_size=AES128BLOCKSIZE;
  vpc->buffer_size=BLOCKCOUNT*vpc->block_size;
  return vpc;
}

void generate_chunk_counter_block(vpccm_crypt_chunked *vp,uint8_t *buffer) {
  int i;
  int qLen=15-vp->iv_size;

  int fl=0x0;

  fl |=(qLen-1) & 0x07;
  *(buffer)=fl;

  for(i=0; i<vp->iv_size; i++) {
    buffer[i+1]=vp->iv[i];
  }

  for(i=vp->iv_size+1; i<vp->block_size; i++) {
    buffer[i]=0;
  }


}

void aes_chunked_encrypt(vpccm_crypt_chunked *vp, uint8_t *bytes, uint8_t* cipher_bytes) {
  /* tiny AES */
  /*
    AES128_ECB_encrypt(bytes,vp->key,cipher_bytes);
  */

  /* openssl */

  AES_KEY wctx;
  AES_set_encrypt_key(vp->key, 128,&wctx);

  AES_encrypt(bytes, cipher_bytes, &wctx);

  /* AESv2 */
  /*
    aes_encrypt(bytes, cipher_bytes, vp->key);
  */
}


vpccm_crypt_chunked* vpccm_crypt_init_with_key_and_file_data(uint8_t *key,int key_size,uint8_t *iv,int iv_size,uint8_t *adata,int adata_size,int tag_length, long file_size, char *url) {

  vpccm_crypt_chunked *vpc=(vpccm_crypt_chunked*)malloc(sizeof(vpccm_crypt_chunked));
  int i;

  char state_file[ strlen(url)+6 ];
  sprintf(state_file,"%s.state",url);

  vpc->key=key;
  vpc->key_size=key_size;
  vpc->iv=iv;
  vpc->iv_size=iv_size;
  vpc->adata=adata;
  vpc->adata_size=adata_size;
  vpc->tag_length=tag_length;
  vpc->block_size=AES128BLOCKSIZE;
  vpc->block_count=BLOCKCOUNT;
  vpc->buffer_size=BLOCKCOUNT*AES128BLOCKSIZE;
  vpc->file_size = file_size;
  vpc->bytes_left = file_size;
  vpc->offset = 0;
  vpc->exit_next = -1;
  vpc->url = url;

  vpc->cc=(uint8_t*)malloc(vpc->block_size*vpc->block_count);
  vpc->Y=(uint8_t*)malloc(vpc->block_size);
  vpc->S0=(uint8_t*)malloc(vpc->block_size);
  vpc->ctr=(uint8_t*)malloc(vpc->block_size);
  vpc->ctr_cipher=(uint8_t*)malloc(vpc->block_size);

  if (load_state(state_file, vpc) == 0) {
    printf("state loaded with offset %ld, returning state\n", vpc->offset);
    return vpc;
  }

  generate_chunk_counter_block(vpc,vpc->ctr);
  aes_chunked_encrypt(vpc,vpc->ctr,vpc->S0);

  for(i=0; i<vpc->block_size; i++) {
    vpc->Y[i]=0;
  }

  return vpc;
}

void free_ccm_engine(vpccm_crypt_chunked *vp) {
  free(vp->Y);
  free(vp->S0);
  free(vp->ctr);
  free(vp->ctr_cipher);
  free(vp->cc);
  free(vp);
}

//MODIFY THIS TO WORK WITH URLS INSTEAD OF FILES CURL LOAD INTO MEMORY AND HANDLE IT LIKE DATA?
/*
  uint8_t*  encrypt_stream_with_URL(vpccm_crypt *vpc,char *url,long *encrypted_stream_length)
  {
  FILE* fp=fopen(url,"r+");
  fseek(fp,0,SEEK_END);
  long file_size=ftell(fp);
  rewind(fp);

  vpc->bytes_left=file_size;

  vpccm* vp=vpccm_init_with_key(vpc->key,vpc->key_size,vpc->iv,vpc->iv_size,vpc->adata,vpc->adata_size,vpc->tag_length,file_size);

  uint8_t *encoded_stream=(uint8_t*)malloc(file_size+vpc->tag_length);
  uint8_t buffer[vpc->buffer_size];
  uint8_t *encrypted=NULL;
  long length=0;

  while(!feof(fp))
  {
  long bytesread=fread(buffer,sizeof(uint8_t),vpc->buffer_size,fp);
  if(bytesread>0)
  {
  encrypted=encrypt_block(vp,buffer,bytesread);
  memcpy(encoded_stream+length,encrypted,bytesread);
  length+=bytesread;
  }

  }

  uint8_t *tag=get_tag(vp);
  memcpy(encoded_stream+length,tag,vpc->tag_length);
  fclose(fp);
  *encrypted_stream_length=length+vpc->tag_length;

  return encoded_stream;
  }

  void encrypt_file_to_file_with_sourceURL(vpccm_crypt *vpc,char *sourceURL,char *destURL)
  {

  FILE* ofp=fopen(destURL,"w+");
  long out_length;
  uint8_t* encrypted_srteam=encrypt_stream_with_URL(vpc,sourceURL,&out_length);
  fwrite(encrypted_srteam,sizeof(uint8_t),out_length,ofp);
  fclose(ofp);
  }
*/

/*
//MODIFY THIS TO WORK WITH URLS INSTEAD OF FILES CURL LOAD INTO MEMORY AND HANDLE IT LIKE DATA?
uint8_t*  decrypt_stream_with_URL(vpccm_crypt *vpc,char *url,long *decrypted_stream_length)
{

FILE* fp=fopen(url,"r+");
fseek(fp,0,SEEK_END);
long file_size=ftell(fp);
rewind(fp);
printf("File Size: %ld\n",file_size);
vpc->bytes_left=file_size;

vpccm* vp=vpccm_init_with_key(vpc->key,vpc->key_size,vpc->iv,vpc->iv_size,vpc->adata,vpc->adata_size,vpc->tag_length,file_size);
if(verify_tag_with_file_URL(vp,url))
{
printf("Tag Verified!!\n");
//UGLY HACK SEE IF I'VE DONE SOMETHING VERY STUPID
vp->file_size=file_size-vpc->tag_length;
vp->bytes_left=file_size-vpc->tag_length;
init_vpccm(vp);

uint8_t *decrypted_stream=(uint8_t*)malloc(file_size-vpc->tag_length);
uint8_t buffer[vpc->buffer_size];
int length=0;
while(!feof(fp))
{

long bytesread=fread(buffer,sizeof(uint8_t),vpc->buffer_size,fp);
if(vpc->bytes_left-bytesread<=vpc->block_size)
{
bytesread-=vpc->tag_length-(vpc->bytes_left-bytesread);
if(bytesread==0)
{
break;
}
}

int exit_next=0;
uint8_t  *decrypted=decrypt_block(vp,buffer,bytesread,&exit_next);
memcpy(decrypted_stream+length,decrypted,bytesread);
length+=bytesread;
vpc->bytes_left-=bytesread;
int is_last_block=(exit_next==1);

if(is_last_block)
{
break;
}

}
*decrypted_stream_length=length;
return decrypted_stream;
}
else
{
fprintf(stderr,"Invalid Tag, ErrorNumber:1\n");
*decrypted_stream_length=-1;
return NULL;
}


}
*/


/*
  void decrypt_file_to_file_with_sourceURL(vpccm_crypt *vpc,char *sourceURL,char *destURL)
  {

  FILE *ofp=fopen(destURL,"w+");

  long out_length=0;
  uint8_t* decrypted_stream=decrypt_stream_with_URL(vpc,sourceURL,&out_length);
  printf("out_length %ld\n",out_length);
  fwrite(decrypted_stream,sizeof(uint8_t),out_length,ofp);

  fclose(ofp);

  }
*/

/*
  uint8_t*  encrypt_data_with_data(vpccm_crypt *vpc,uint8_t  *data,int data_length,long *encrypted_length)
  {
  //int loop_count=ceil(data_length/(float)vpc->buffer_size);
  int loop_count=myceil(data_length,vpc->buffer_size);
  int i;

  vpccm *vp=vpccm_init_with_key(vpc->key,vpc->key_size,vpc->iv,vpc->iv_size,vpc->adata,vpc->adata_size,vpc->tag_length,data_length);

  uint8_t* temp_cipher=(uint8_t*)malloc(loop_count*vpc->buffer_size);
  uint8_t *bytes=data;
  int length=0;

  int len=0;
  for(i=0; i<loop_count; i++) {
  uint8_t* encrypted=NULL;
  if(i!=loop_count-1) {
  encrypted,encrypt_block(vp,bytes+i*vpc->buffer_size,vpc->buffer_size);
  len=vpc->buffer_size;

  }
  else {
  len=data_length%vpc->buffer_size;
  encrypted=encrypt_block(vp,bytes+i*vpc->buffer_size,len);

  }

  //cipher append to temp buffer
  memcpy(temp_cipher+length,encrypted,len);
  length+=len;
  }
  uint8_t *tag=get_tag(vp);
  uint8_t* cipher=(uint8_t*)malloc(length+vpc->tag_length);//arguement
  memcpy(cipher,temp_cipher,length);
  memcpy(cipher+length,tag,vpc->tag_length);
  free(temp_cipher);
  free(tag);
  *encrypted_length=length+vpc->tag_length;
  return cipher;
  }
*/

/*
  uint8_t*  decrypt_data_with_data(vpccm_crypt *vpc,uint8_t *data,int data_length,long *decrypted_length)
  {
  int length=0;
  int i;
  if(data_length<=vpc->tag_length) {
  fprintf(stderr,"Cipher text is too short Errornum:2\n");
  return 0;

  }
  vpc->bytes_left=data_length-vpc->tag_length;

  vpccm *vp=vpccm_init_with_key(vpc->key,vpc->key_size,vpc->iv,vpc->iv_size,vpc->adata,vpc->adata_size,vpc->tag_length,data_length);
  if(verify_tag_with_data(vp,data,data_length)) {
  //UGLY HACK SEE IF I'VE DONE SOMETHING VERY STUPID
  vp->file_size=data_length-vpc->tag_length;
  vp->bytes_left=data_length-vpc->tag_length;
  init_vpccm(vp);
  //EVEN UGLIER HACK
  //vp=vpccm_init_with_key(vpc->key,vpc->key_size,vpc->iv,vpc->iv_size,vpc->adata,vpc->adata_size,vpc->tag_length,data_length-vpc->tag_length);
  //int loop_count=ceil((data_length-vpc->tag_length)/(float)vpc->buffer_size);
  int loop_count=myceil( (data_length-vpc->tag_length), vpc->buffer_size );
  uint8_t *plain=(uint8_t*)malloc(loop_count*vpc->buffer_size);

  uint8_t *bytes=(uint8_t*)data;

  int len=0;
  int en=-1;
  for( i=0; i<loop_count; i++) {
  uint8_t *decrypted=NULL;
  if(i!=loop_count-1) {
  decrypted=decrypt_block(vp,bytes+i*vpc->buffer_size,vpc->buffer_size,&en);
  len=vpc->buffer_size;
  }
  else {

  len=(data_length-vpc->tag_length)%vpc->buffer_size;
  decrypted=decrypt_block(vp,bytes+i*vpc->buffer_size,len,&en);
  }


  memcpy(plain+length,decrypted,len);
  length+=len;

  }
  *decrypted_length=length;
  uint8_t *plaintext=(uint8_t*)malloc(length);
  memcpy(plaintext,plain,length);
  free(plain);
  free_vpccm(vp);
  return plaintext;
  }
  else {
  printf("Failed to verify flag\n");
  fprintf(stderr,"Invalid TAG errorNumber:1\n");
  free_vpccm(vp);
  return NULL;
  }


  }
*/

void cBytesXorBytes(uint8_t* bytes_left,uint8_t* bytes_right,uint8_t *outbuff,int length) {

  int i;
  for(i=0; i<length; i++) {
    outbuff[i]=bytes_left[i] ^ bytes_right[i];
  }
}

void format_chunked_payload(vpccm_crypt_chunked *vp,uint8_t *plaindatablock,uint8_t *buffer,int length) {

  int pad=length%vp->block_size;

  int i;
  for(i=0; i<length; i++) {
    buffer[i]=plaindatablock[i];
  }

  if(pad>0) {
    for(i=0; i<vp->block_size-pad; i++) {
      buffer[length+i]=0;
    }
  }
}

long format_chunked_assoc_data(vpccm_crypt_chunked *vp,uint8_t* buffer) {
  int payload_length=1;
  int value=vp->adata_size;
  int i;

  if(vp->adata_size==0) {
    *(buffer)=0;
  }
  else if(vp->adata_size<=0xFEFF) {
    for(i=1; i>=0; i--) {
      buffer[i]=value & 0xFF;
      value=value >> 8;
    }
    payload_length=2;

  }
  else {
    fprintf(stderr,"Invalid adatalength should be <=65279");
  }

  int padding_length=(vp->block_size-vp->adata_size%vp->block_size)-payload_length;

  for(i=0; i<vp->adata_size; i++) {
    buffer[i+payload_length]=vp->adata[i];
  }

  for(i=0; i<padding_length; i++) {
    buffer[payload_length+vp->adata_size+i]=0;
  }

  if(padding_length<0) {
    padding_length=0;
  }

  return payload_length+vp->adata_size+padding_length;
}

void format_chunked_header_with_payload_length(vpccm_crypt_chunked *vp,long long payload_length,uint8_t *buffer) {

  int qLen=15-vp->iv_size;
  int fl=0;
  int i;

  fl |= (vp->adata_size>0)?0x40:0;
  fl |= ((((vp->tag_length-2)/2) & 0x07) << 3);
  fl |=((qLen -1) & 0x07);

  *(buffer)=fl;


  for(i=0; i<qLen; i++) {
    // *(buffer+vp->block_size-i-1)=payload_length & 255;
    buffer[vp->block_size-i-1]=payload_length & 255;
    payload_length >>=8;
  }

  for(i=0; i<vp->iv_size; i++) {
    // *(buffer+1+i)=*(vp.iv+i);
    buffer[i+1]=vp->iv[i];
  }

}

long formatting_chunked_nap(vpccm_crypt_chunked *vp,uint8_t *plaindatablock,uint8_t *buffer,int payload_length) {

  if(vp->exit_next==-1)
    {
      format_chunked_header_with_payload_length(vp,vp->file_size,buffer);
      long adata_len=vp->block_size;

      if(vp->adata_size>0)
        {
	  adata_len+=format_chunked_assoc_data(vp,buffer+vp->block_size);
        }
      format_chunked_payload(vp,plaindatablock,buffer+adata_len,payload_length);
      return adata_len+vp->block_size;
    }
  else
    {
      format_chunked_payload(vp,plaindatablock,buffer,payload_length);
    }

  return -1;
}

uint8_t* encrypt_chunked_block(vpccm_crypt_chunked *vp,uint8_t *bytes,int length)
{

  vp->bytes_left-=length;
  //int sub_block_count=ceil(length/(float)vp->block_size);
  int sub_block_count=myceil(length,vp->block_size);
  //printf("%d vs %d (%d / %d)",sub_block_count,sub_block_count1,length,vp->block_size);

  int padding_count=length%vp->block_size;
  int i,j,k,n;

  for(i=0; i<sub_block_count; i++) {

    uint8_t *sub_block_bytes=bytes+i*vp->block_size;
    int sub_block_length=(i==sub_block_count-1 && padding_count >0)?padding_count:vp->block_size;
    //int napl_bytes_length=(ceil((vp->adata_size)/(float)vp->block_size)*vp->block_size)+3*vp->block_size;
    int napl_bytes_length=( myceil(vp->adata_size,vp->block_size)*vp->block_size )+3*vp->block_size;

    uint8_t *xorbytes=(uint8_t*)malloc(sub_block_length);
    uint8_t *napl_bytes=(uint8_t*)malloc(napl_bytes_length);
    uint8_t *y_bytes=(uint8_t*)malloc(vp->block_size);
    uint8_t *y_cipher=(uint8_t*)malloc(vp->block_size);

    napl_bytes_length=formatting_chunked_nap(vp,sub_block_bytes,napl_bytes,sub_block_length);

    if(vp->exit_next==-1) {
      vp->exit_next=0;
      for(k=0; k<napl_bytes_length; k+=vp->block_size) {
	cBytesXorBytes(napl_bytes+k,vp->Y,y_bytes,vp->block_size);
	aes_chunked_encrypt(vp,y_bytes,y_cipher);
	//array_cp(vp->Y,y_cipher,vp->block_size);
	memcpy(vp->Y,y_cipher,vp->block_size);

      }
    }
    else {
      cBytesXorBytes(napl_bytes,vp->Y,y_bytes,vp->block_size);
      aes_chunked_encrypt(vp,y_bytes,y_cipher);
      //array_cp(vp->Y,y_cipher,vp->block_size);
      memcpy(vp->Y,y_cipher,vp->block_size);
    }

    for(j=15; j>0; j--) {
      //n=*(vp.ctr+j);
      n=vp->ctr[j];
      //*(vp.ctr+j)=(n+1) & 255;
      vp->ctr[j]=(n+1) & 255;

      if(vp->ctr[j]!='\0') {
	break;
      }

    }

    aes_chunked_encrypt(vp,vp->ctr,vp->ctr_cipher);
    cBytesXorBytes(sub_block_bytes,vp->ctr_cipher,xorbytes,sub_block_length);


    for(j=0; j<sub_block_length; j++) {
      //*(vp.cc+i*vp.block_size+j)=*(xorbytes+j);
      vp->cc[i*vp->block_size+j]=xorbytes[j];
    }
    if(sub_block_length<vp->block_size || (vp->bytes_left ==0 && sub_block_length ==vp->block_size)) {
      vp->exit_next=1;
    }

    free(xorbytes);
    free(napl_bytes);
    free(y_bytes);
    free(y_cipher);

  }
  return vp->cc;
}

uint8_t* get_chunked_tag(vpccm_crypt_chunked *vp) {
  uint8_t *tag_buffer=(uint8_t*)malloc(vp->block_size);
  cBytesXorBytes(vp->Y,vp->S0,tag_buffer,vp->block_size);
  uint8_t *tag=(uint8_t*)malloc(vp->tag_length);
  memcpy(tag,tag_buffer,vp->tag_length);
  free(tag_buffer);
  return tag;
}

/*
 * massacred code to POST files in chunks
 */
uint8_t* encrypt_chunked(vpccm_crypt_chunked *ccm_chunked, uint8_t *chunk_buffer, uint8_t *read_buffer)
{
  FILE* fp=fopen(ccm_chunked->url,"r");

  fseek(fp, ccm_chunked->offset, SEEK_SET);

  //uint8_t buffer[CHUNK];
  //chunk_buffer = NULL;

  long bytes_this_run = 0;
  int i;
  while(!feof(fp) && bytes_this_run < (CHUNK-ccm_chunked->tag_length) ) {
    //size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream);

    long bytesread = fread( read_buffer, sizeof(uint8_t), (CHUNK-ccm_chunked->tag_length), fp);

    if(bytesread>0) {
      chunk_buffer=encrypt_chunked_block(ccm_chunked, read_buffer, bytesread);
      /*
	printf("read %ld bytes\n", bytesread);
	printf("buffer is\n");
	for (i=0; i<CHUNK; i++) {
	if (!(i%80))
	printf("\n");
	printf("%02x",chunk_buffer[i]);
	}
	printf("\n");
      */

      //memcpy(chunk_buffer, chunk_buffer, bytesread);

      ccm_chunked->offset+=bytesread;
      bytes_this_run+=bytesread;
      ccm_chunked->chunk_length = bytesread;
    }

  }

  if (ccm_chunked->exit_next != 1) {
    fclose(fp);
    return chunk_buffer;
  }

  uint8_t *tag=get_chunked_tag(ccm_chunked);

  memcpy(chunk_buffer+bytes_this_run,tag,ccm_chunked->tag_length);
  ccm_chunked->chunk_length += ccm_chunked->tag_length;

  fclose(fp);

  return chunk_buffer;
}

int load_state(char *state_file, vpccm_crypt_chunked *ccm_engine) {
  FILE *fp;
  size_t bytes_read;

  fp = fopen(state_file, "r");
  if (!fp)
    return -2;

  bytes_read = fread(&ccm_engine->tag_length, sizeof(int), 1, fp);
  if (bytes_read < 1)
    return -1;
  bytes_read = fread(&ccm_engine->buffer_size, sizeof(int), 1, fp);
  if (bytes_read < 1)
    return -1;
  bytes_read = fread(&ccm_engine->block_size, sizeof(int), 1, fp);
  if (bytes_read < 1)
    return -1;
  bytes_read = fread(&ccm_engine->block_count, sizeof(int), 1, fp);
  if (bytes_read < 1)
    return -1;
  bytes_read = fread(&ccm_engine->exit_next, sizeof(int), 1, fp);
  if (bytes_read < 1)
    return -1;
  bytes_read = fread(&ccm_engine->key_size, sizeof(int), 1, fp);
  if (bytes_read < 1)
    return -1;
  bytes_read = fread(&ccm_engine->iv_size, sizeof(int), 1, fp);
  if (bytes_read < 1)
    return -1;
  bytes_read = fread(&ccm_engine->adata_size, sizeof(int), 1, fp);
  if (bytes_read < 1)
    return -1;

  bytes_read = fread(&ccm_engine->bytes_left, sizeof(long), 1, fp);
  if (bytes_read < 1)
    return -1;
  bytes_read = fread(&ccm_engine->file_size, sizeof(long), 1, fp);
  if (bytes_read < 1)
    return -1;
  bytes_read = fread(&ccm_engine->offset, sizeof(long), 1, fp);
  if (bytes_read < 1)
    return -1;
  bytes_read = fread(&ccm_engine->chunk_length, sizeof(long), 1, fp);
  if (bytes_read < 1)
    return -1;

  bytes_read = fread(ccm_engine->cc, sizeof(uint8_t), (ccm_engine->block_size*ccm_engine->block_count), fp);
  if (bytes_read < 1)
    return -1;
  bytes_read = fread(ccm_engine->Y, sizeof(uint8_t), ccm_engine->block_size, fp);
  if (bytes_read < 1)
    return -1;
  bytes_read = fread(ccm_engine->S0, sizeof(uint8_t), ccm_engine->block_size, fp);
  if (bytes_read < 1)
    return -1;
  bytes_read = fread(ccm_engine->ctr, sizeof(uint8_t), ccm_engine->block_size, fp);
  if (bytes_read < 1)
    return -1;
  bytes_read = fread(ccm_engine->ctr_cipher, sizeof(uint8_t), ccm_engine->block_size, fp);
  if (bytes_read < 1)
    return -1;

  fclose(fp);
  return 0;
}

int save_state(char *state_file, vpccm_crypt_chunked *ccm_engine) {
  FILE *fp;
  size_t bytes_written;

  fp = fopen(state_file, "w");
  if (!fp)
    return -2;

  bytes_written = fwrite(&ccm_engine->tag_length, sizeof(int), 1, fp);
  if (bytes_written < 1)
    return -1;
  bytes_written = fwrite(&ccm_engine->buffer_size, sizeof(int), 1, fp);
  if (bytes_written < 1)
    return -1;
  bytes_written = fwrite(&ccm_engine->block_size, sizeof(int), 1, fp);
  if (bytes_written < 1)
    return -1;
  bytes_written = fwrite(&ccm_engine->block_count, sizeof(int), 1, fp);
  if (bytes_written < 1)
    return -1;
  bytes_written = fwrite(&ccm_engine->exit_next, sizeof(int), 1, fp);
  if (bytes_written < 1)
    return -1;
  bytes_written = fwrite(&ccm_engine->key_size, sizeof(int), 1, fp);
  if (bytes_written < 1)
    return -1;
  bytes_written = fwrite(&ccm_engine->iv_size, sizeof(int), 1, fp);
  if (bytes_written < 1)
    return -1;
  bytes_written = fwrite(&ccm_engine->adata_size, sizeof(int), 1, fp);
  if (bytes_written < 1)
    return -1;

  bytes_written = fwrite(&ccm_engine->bytes_left, sizeof(long), 1, fp);
  if (bytes_written < 1)
    return -1;
  bytes_written = fwrite(&ccm_engine->file_size, sizeof(long), 1, fp);
  if (bytes_written < 1)
    return -1;
  bytes_written = fwrite(&ccm_engine->offset, sizeof(long), 1, fp);
  if (bytes_written < 1)
    return -1;
  bytes_written = fwrite(&ccm_engine->chunk_length, sizeof(long), 1, fp);
  if (bytes_written < 1)
    return -1;

  bytes_written = fwrite(ccm_engine->cc, sizeof(uint8_t), (ccm_engine->block_size*ccm_engine->block_count), fp);
  if (bytes_written < 1)
    return -1;
  bytes_written = fwrite(ccm_engine->Y, sizeof(uint8_t), ccm_engine->block_size, fp);
  if (bytes_written < 1)
    return -1;
  bytes_written = fwrite(ccm_engine->S0, sizeof(uint8_t), ccm_engine->block_size, fp);
  if (bytes_written < 1)
    return -1;
  bytes_written = fwrite(ccm_engine->ctr, sizeof(uint8_t), ccm_engine->block_size, fp);
  if (bytes_written < 1)
    return -1;
  bytes_written = fwrite(ccm_engine->ctr_cipher, sizeof(uint8_t), ccm_engine->block_size, fp);
  if (bytes_written < 1)
    return -1;

  fclose(fp);
  return 0;
}



//UGLY HACK TO STORE POST RESPONSES TO A CHAR*
size_t write_to_string(void *ptr, size_t size, size_t count, void *stream)
{
  *(char**)stream=(char*)malloc(size*count+1);
  memcpy(*(char**)stream,ptr,size*count);
  char *chara=*(char**)stream;
  chara[size*count]='\0';
  //*(char**)stream=strdup(ptr);
  return size*count;
}


int ccm_upload_chunked(CURL *curl,vpccm_crypt *vpc,char *file_name,char *state_file,char *cloud_dir,char *session_id,char *ctype)
{

  CURLcode res;
  if(curl)
    {
      FILE *state_fp=fopen(file_name,"rb");


      fseek(state_fp,SEEK_END,0);
      long file_size=ftell(state_fp);
      rewind(state_fp);
      printf("file size %ld",file_size);
      if(state_fp)
	{

	  vpccm_crypt_chunked *vpc_c=vpccm_crypt_init_with_key_and_file_data(vpc->key,vpc->key_size,vpc->iv,vpc->iv_size,vpc->adata,vpc->adata_size,vpc->tag_length,file_size,file_name);

	  /*struct curl_httppost *formpost=NULL;
	      struct curl_httppost *lastptr=NULL;
	      struct curl_slist *headerlist=NULL;
	      headerlist = curl_slist_append(headerlist, "Expect:");



	      char url[2048];
	      char *response;
	      sprintf(url,"https://dev.whitecloud.mobi/uploadsmobile?dir=%s&flags=0&sfile=%s&iv=%s&ctype=%s&adata=%s",cloud_dir,session_id,vpc_c->iv,ctype,vpc_c->adata);
	      curl_easy_setopt(curl, CURLOPT_URL, url);
	      char postfield[1024];
	      sprintf(postfield,"name=%s",file_name);
	      curl_easy_setopt(curl,CURLOPT_POSTFIELDS,postfield);
	      curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string);
	      curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);
	      curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
	      curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);*/


	      uint8_t *read_buffer=malloc(CHUNK);
	  while(vpc_c->bytes_left)
	    {

	      //printf("fetching next chunk at %ld ..\n", ccm_engine->offset);
	      //encrypted = encrypt_chunked(ccm_engine, encrypted);
	      //encrypted = encrypt_chunked(ccm_engine, encrypted, read_buffer);
	      vpc_c->cc = encrypt_chunked(vpc_c, vpc_c->cc, read_buffer);

	      printf("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b ****Encrypted Data: (length %ld / %ld)",vpc_c->chunk_length,vpc_c->bytes_left);

	      //fwrite(ccm_engine->cc, sizeof(uint8_t), ccm_engine->chunk_length, fpw);


	      //SETUP POST

	      //res = curl_easy_perform(curl);


	      if (save_state(state_file, vpc_c) != 0)
		fprintf(stderr,"save state failure ?");








	    }








	  return 0;
	}
    }

  return -1;


}







int main(int argc,char *argv[])
{

  CURL *curl;
  curl_global_init(CURL_GLOBAL_ALL);
  curl = curl_easy_init();

  char *file_name="lala";
  char *state_file="lala.state";
  char *session_id="faa4867aa739b434bcb032e22937a989";
  char *ctype="STUFF";
  char *key="DLAjAogg+RGrpRmDL/zEQw==";
  char *iv="DAIV";
  char *adata="DEADDATA";
  char *cloud_dir="";
  int tag_length=16;
  vpccm_crypt* vpc=vpccm_crypt_init_with_key(key,strlen(key),iv,strlen(iv),adata,strlen(adata),tag_length);


  ccm_upload_chunked(curl,vpc,file_name,state_file,cloud_dir,session_id,ctype);

  if(curl)
    {
      curl_easy_cleanup(curl);
    }
  curl_global_cleanup();


  return 0;
}


